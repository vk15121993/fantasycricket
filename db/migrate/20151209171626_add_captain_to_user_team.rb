class AddCaptainToUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :captain_id, :integer
  end
end
