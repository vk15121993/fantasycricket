class AddMatchToUserTeams < ActiveRecord::Migration
  def change
    add_column :user_teams, :match_id, :integer
    add_column :matches, :start_time, :datetime
    add_column :matches, :end_time, :datetime
  end
end
