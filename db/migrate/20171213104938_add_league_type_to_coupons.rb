class AddLeagueTypeToCoupons < ActiveRecord::Migration
  def change
    add_column :coupons, :league_type, :string
  end
end
