class AddIsTiedToLeagues < ActiveRecord::Migration
  def change
    rename_column :leagues, :is_passed, :is_tied
  end
end
