class AddWinnerRanksToLeague < ActiveRecord::Migration
  def change
    add_column :leagues, :winner_ranks, :text, array:true, default: []
    add_column :leagues, :loser_ranks, :text, array:true, default: []
    add_column :leagues, :tied_team_ranks, :text, array:true, default: []
  end
end
