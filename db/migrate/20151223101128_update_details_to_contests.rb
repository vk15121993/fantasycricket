class UpdateDetailsToContests < ActiveRecord::Migration
  def change
    add_column :contests, :team_type, :string
    remove_column :contests, :answer_size, :integer
    remove_column :contests, :answer_type, :string
  end
end
