class CreateCoupon < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :code
      t.float :discount_value
      t.string :discount_type
      t.float :maximum_discount_value
      t.timestamp :start_date
      t.timestamp :end_date
      t.integer :maximum_redeem_count
      t.integer :current_redeem_count
      t.string :user_validation_eval_string
      t.string :league_validation_eval_string
      t.belongs_to :tournament, index: true
      t.belongs_to :match, index: true

      t.timestamps null: false
    end
  end
end
