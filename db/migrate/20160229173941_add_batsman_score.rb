class AddBatsmanScore < ActiveRecord::Migration
  def change
    add_column :player_scores, :bat_singles, :integer, default: 0
    add_column :player_scores, :bat_duck, :integer, default: 0
    add_column :player_scores, :bat_hundreds, :integer, default: 0
    add_column :player_scores, :bat_fifties, :integer, default: 0
  end
end
