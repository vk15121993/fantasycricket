class ChangeTotalScoreToFloat < ActiveRecord::Migration
  def up
    change_column :user_teams, :totalscore, :float
  end

  def down
    change_column :user_teams, :totalscore, 'integer USING CAST(totalscore AS integer)'
  end
end
