class CreateWalletTransactions < ActiveRecord::Migration
  def change
    create_table :wallet_transactions do |t|
      t.belongs_to :user
      t.belongs_to :wallet
      t.belongs_to :banked_credit
      t.float :amount
      t.string :transaction_type
      t.string :category
      t.json :current_wallet_amount

      t.timestamps null: false
    end
  end
end
