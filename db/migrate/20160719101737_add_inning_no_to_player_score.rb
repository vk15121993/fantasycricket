class AddInningNoToPlayerScore < ActiveRecord::Migration
  def change
    add_column :player_scores, :inning_no, :integer, default: 1
  end
end
