class AddFieldsToUserTransactions < ActiveRecord::Migration
  def change
    add_column :user_transactions, :transaction_category, :string , default: ""
    add_column :user_transactions, :total_amount, :integer , default: 0
    add_column :user_transactions, :balance_amount, :integer , default: 0
  end
end
