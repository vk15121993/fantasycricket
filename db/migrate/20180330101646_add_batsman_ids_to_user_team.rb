class AddBatsmanIdsToUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :batsman_ids, :string, array: true, default: []
    add_column :user_teams, :bowler_ids, :string, array: true, default: []
    add_column :user_teams, :allrounder_ids, :string, array: true, default: []
    add_column :user_teams, :wicketkeeper_ids, :string, array: true, default: []
  end
end
