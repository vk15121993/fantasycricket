class AddRegistrationInfoToUser < ActiveRecord::Migration
  def change
    add_column :users, :registration_info, :hstore
  end
end
