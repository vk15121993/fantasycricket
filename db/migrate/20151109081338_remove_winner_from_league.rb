class RemoveWinnerFromLeague < ActiveRecord::Migration
  def change
    remove_column :leagues, :winner_id, :integer
  end
end
