class AddTransactionTypeToBankedCredits < ActiveRecord::Migration
  def change
    add_column :banked_credits, :transaction_type, :string  , :default => ""
  end
end
