class AddTransactionDataUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :transaction_data, :hstore
  end
end
