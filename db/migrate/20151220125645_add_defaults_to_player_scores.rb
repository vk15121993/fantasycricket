class AddDefaultsToPlayerScores < ActiveRecord::Migration
  def change
    change_column :player_scores, :bat_runs_scored, :integer, default: 0
    change_column :player_scores, :bat_fours, :integer, default: 0
    change_column :player_scores, :bat_sixes, :integer, default: 0
    change_column :player_scores, :bowl_maidens, :integer, default: 0
    change_column :player_scores, :bowl_wickets, :integer, default: 0
    change_column :player_scores, :bowl_wides, :integer, default: 0
    change_column :player_scores, :bowl_noballs, :integer, default: 0
  end
end
