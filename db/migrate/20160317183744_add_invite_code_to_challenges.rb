class AddInviteCodeToChallenges < ActiveRecord::Migration
  def change
    add_column :challenges, :invite_code, :string, index: true
  end
end
