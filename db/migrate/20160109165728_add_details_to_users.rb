class AddDetailsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :phone, :string
    add_column :users, :date_of_birth, :date
    add_column :users, :state, :string
    add_column :users, :country, :string, default: "IN"
  end
end
