class CreateMetaInfo < ActiveRecord::Migration
  def change
    create_table :meta_infos do |t|
      t.string :name
      t.integer :identifier
      t.json :info

      t.timestamps null: false
    end
  end
end
