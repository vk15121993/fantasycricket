class CreateWallet < ActiveRecord::Migration
  def change
    create_table :wallets do |t|
      t.float :main_balance, default: 0.0
      t.float :referral_balance, default: 0.0
      t.belongs_to :user, index: true

      t.timestamps null: false
    end
  end
end
