class AddHasTiedToUserTeam < ActiveRecord::Migration
  def change
    add_column :user_teams, :has_tied, :boolean, default: false
  end
end
