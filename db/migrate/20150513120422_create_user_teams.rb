class CreateUserTeams < ActiveRecord::Migration
  def change
    create_table :user_teams do |t|
      t.string    :name
      t.integer   :user_id
      t.integer   :totalscore
      t.integer   :captain_id
      t.boolean   :validated       
      t.integer   :keeper_id 
      t.boolean   :never_validated 

      t.timestamps
    end
    add_column :participant_teams, :captain_id, :integer
    add_column :participant_teams, :keeper_id,  :integer
  end
end
