class AddStateToUserTransactions < ActiveRecord::Migration
  def change
    add_column :user_transactions, :state, :string, default: 'created'
  end
end
