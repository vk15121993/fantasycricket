class AddPerUserCountToCoupon < ActiveRecord::Migration
  def change
    add_column :coupons, :per_user_count, :integer, default: 1
  end
end
