class CreateReferralCredits < ActiveRecord::Migration
  def change
    create_table :referral_credits do |t|
      t.float :amount_used, default: 0
      t.float :amount_remaining, default: 0
      t.boolean :expired, default: false
      t.belongs_to :wallet, index: true
      t.timestamp :expired_at

      t.timestamps null: false
    end
  end
end
