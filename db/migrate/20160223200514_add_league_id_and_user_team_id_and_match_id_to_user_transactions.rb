class AddLeagueIdAndUserTeamIdAndMatchIdToUserTransactions < ActiveRecord::Migration
  def change
    add_column :user_transactions, :league_id, :integer
    add_column :user_transactions, :user_team_id, :integer
    add_column :user_transactions, :match_id, :integer
  end
end
