class CreateContests < ActiveRecord::Migration
  def change
    create_table :contests do |t|
      t.json    :questions_hash
      t.integer :answer_size
      t.string  :answer_type
      t.string  :title

      t.timestamps
    end
  end
end
