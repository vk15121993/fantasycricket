class AddLeagueTypeToLeagues < ActiveRecord::Migration
  def change
    add_column :leagues, :league_type, :string
  end
end
