class ChangesHstoreToJsonLeagues < ActiveRecord::Migration
  def change
    remove_column :leagues, :questions_hash
    remove_column :leagues, :truth_hash
    remove_column :leagues, :rank_hash

    add_column :leagues, :questions_hash,  :json
    add_column :leagues, :truth_hash,  :json
    add_column :leagues, :rank_hash,   :json

  end
end
