class CleanupUserTeam < ActiveRecord::Migration
  def change
    remove_column :user_teams, :name, :string
    remove_column :user_teams, :captain_id
    remove_column :user_teams, :keeper_id
    remove_column :user_teams, :never_validated
  end
end
