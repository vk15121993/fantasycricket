class CreateLeagues < ActiveRecord::Migration
  def change
    create_table :leagues do |t|
      t.string :name
      t.text :description
      t.integer :match_id
      t.integer :limit          , default: 0
      t.integer :joined         , default: 0
      t.boolean :is_live        , default: false
      t.integer :prize_money    , default: 0
      t.boolean :is_multiple_allowed, default: false
      t.boolean :is_fake            , default: false
      t.integer :fake_size          , default: 0

      t.timestamps
    end
  end
end
