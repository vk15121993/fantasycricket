class ModifyLeagueToChallenge < ActiveRecord::Migration
  def change
    add_column    :users,   :username,    :integer
    add_column    :users,   :credits,     :integer
    add_column    :leagues, :winner_id,   :integer
    add_column    :leagues, :contest_id,  :integer

    remove_column :leagues, :description
    remove_column :leagues, :rules
    remove_column :leagues, :answer_type
    remove_column :leagues, :answer_size
    remove_column :leagues, :questions_hash
    remove_column :leagues, :truth_hash
    remove_column :leagues, :rank_hash
  end
end
