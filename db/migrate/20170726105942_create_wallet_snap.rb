class CreateWalletSnap < ActiveRecord::Migration
  def change
    create_table :wallet_snaps do |t|
      t.belongs_to :wallet, index: true
      t.belongs_to :user, index: true
      t.float :main_balance
      t.float :referral_balance

      t.timestamps null: false
    end
  end
end
