class SetEmailToNull < ActiveRecord::Migration
  def change
  	change_column :users, :email, :string, :null => true , :default => ""
  	change_column :users, :phone , :string, :null => false 
  	add_index :users, [:phone]
  	remove_index :users, [:lower_email_index] 
  end
end
