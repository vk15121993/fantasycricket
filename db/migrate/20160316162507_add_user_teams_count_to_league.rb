class AddUserTeamsCountToLeague < ActiveRecord::Migration
  def change
    add_column :leagues, :user_teams_count, :integer
  end
end
