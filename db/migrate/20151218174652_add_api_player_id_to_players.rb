class AddApiPlayerIdToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :api_player_id, :string
  end
end
