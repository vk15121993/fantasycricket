class ChangeColumnDefaultLeagueCounter < ActiveRecord::Migration
  def change
    change_column_default(:leagues, :user_teams_count, 0)
  end
end
