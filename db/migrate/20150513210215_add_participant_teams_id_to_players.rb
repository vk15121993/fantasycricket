class AddParticipantTeamsIdToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :participant_team_id, :integer
  end
end
