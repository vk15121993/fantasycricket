class AddUserTransactionToWalletTransaction < ActiveRecord::Migration
  def change
    add_reference :wallet_transactions, :user_transaction, index: true
  end
end
