class AddAnswerColumnsToUt < ActiveRecord::Migration
  def change
    remove_column :user_teams, :answer_hash, :json
    add_column :user_teams, :answer1, :string, array: true, default: []
    add_column :user_teams, :answer2, :string, array: true, default: []
  end
end
