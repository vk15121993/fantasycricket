class AddMsToMatch < ActiveRecord::Migration
  def change
    add_column :matches, :ms, :string, default: "Match is yet to start"
  end
end
