# Modify size option to increase threads
# http://manuel.manuelles.nl/blog/2012/11/13/sidekiq-on-heroku-with-redistogo-nano/
# Current sizes of 1 and 2 are for RedisToGo Nano with a limit of 10 connections
if defined? Sidekiq

  require 'sidekiq'
  require 'sidekiq/web'

  Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]
  Sidekiq::Web.set :sessions, Rails.application.config.session_options
  Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
    [user, password] == ["sk_admin", "sk_adam1n"]
  end



#   redis_url = ENV['REDIS_URL']

#   Sidekiq.configure_server do |config|
#     config.redis = {
#       # namespace: 'workers'
#       url: redis_url
#     }

#     # polling config - 5 sec
#     # config.average_scheduled_poll_interval = 5

    schedule_file = "config/schedule.yml"
    if File.exists?(schedule_file) && Sidekiq.server?
      Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_file)
    end

#   end

#   Sidekiq.configure_client do |config|
#     config.redis = {
#       url: redis_url,
#       size: 2
#     }
#   end

  Sidekiq.configure_client do |config|
    config.redis = { url: ENV['REDIS_URL'] }
  end

  Sidekiq.configure_server do |config|
    config.redis = { url: ENV['REDIS_URL'] }
  end

  class Sidekiq::Extensions::DelayedMailer
    sidekiq_options queue: :mailer, retry: 3
  end
end


if Rails.env.production?


  Sidekiq.configure_server do |config|
    # config.redis = { url: ENV['REDIS_URL'] }

    Rails.application.config.after_initialize do
      Rails.logger.info("DB Connection Pool size for Sidekiq Server before disconnect is: #{ActiveRecord::Base.connection.pool.instance_variable_get('@size')}")
      ActiveRecord::Base.connection_pool.disconnect!

      ActiveSupport.on_load(:active_record) do
        config = Rails.application.config.database_configuration[Rails.env]
        config['reaping_frequency'] = ENV['DATABASE_REAP_FREQ'] || 10 # seconds
        # config['pool'] = ENV['WORKER_DB_POOL_SIZE'] || Sidekiq.options[:concurrency]
        config['pool'] = 16
        ActiveRecord::Base.establish_connection(config)

        Rails.logger.info("DB Connection Pool size for Sidekiq Server is now: #{ActiveRecord::Base.connection.pool.instance_variable_get('@size')}")
      end
    end
  end

end
