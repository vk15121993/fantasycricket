require 'net/http'

module OuterAPI
  include HTTParty

  def self.getTransactionStatus(transaction_id)
    # https://www.instamojo.com/api/1.1/payments/MOJO5619000U69687334/ 
    # --header "X-Api-Key: e276c38c6aa79cfbf86e21ccf44572d4" 
    # --header "X-Auth-Token: 71d5b318e31e9df9647bb65cd2d88530"

    url = "https://www.instamojo.com/api/1.1/payments/#{transaction_id}/"

    auth_header = { "X-Api-Key" => "e276c38c6aa79cfbf86e21ccf44572d4", "X-Auth-Token" => "71d5b318e31e9df9647bb65cd2d88530" }
    response = HTTParty.get(url, headers: auth_header)
    return response
  end
  
end
