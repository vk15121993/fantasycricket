# FoneVerify Api Module
require 'net/http'

module FoneVerifyAPI
 include HTTParty

# Response
	 # {"verificationId"=>"2995",
	 # "mobileNumber"=>"8765587934",
	 # "responseCode"=>"200",
	 # "timeout"=>"90",
	 # "smsCLI"=>"VERIFY",
	 # "updateTime"=>"30",
	 # "callFlowName"=>"SMS/SMS"}
def self.send_verification_code(phone)
	url = 'http://apifv.foneverify.com/verification/v2.0/flow/init'
	response = HTTParty.post(url,
	          :body => "countryCode=91&phoneNumber=#{phone}",
	          :headers => { 'Content-Type' => 'application/x-www-form-urlencoded',
	          'appKey' => Rails.application.config.settings.env['FONE_VERIFY_KEY'],
	          'customerId' => Rails.application.config.settings.env['FONE_VERIFY_CUST_ID']}
	        )
	response
end

# Response
# 	# retry
#     {"verificationId"=>609,
#    "verificationStatus"=>"TRYING_FALLBACK_SMS_DELIVERED",
#    "responseCode"=>"705",
#    "timeout"=>"90",
#    "smsCli"=>"VERIFY",
#    "updateTime"=>"30",
#    "transactionId"=>"1482325575306"}
#   else
# # wrong_otp
#   {"verificationId"=>609,
#    "mobileNumber"=>"8802053264",
#    "verificationStatus"=>"VERIFICATION_COMPLETED",
#    "responseCode"=>"200",
#    "transactionId"=>"1482325575306"}


def self.verify_number(verification_id,otp)
	url = 'http://apifv.foneverify.com/verification/v2.0/flow/update'
	query_string = "?verificationId=#{verification_id}&code=#{otp}"
	response = HTTParty.get(url+query_string,
	          :headers => { 'Content-Type' => 'application/x-www-form-urlencoded',
	           'appKey' => Rails.application.config.settings.env['FONE_VERIFY_KEY'],
	          'customerId' => Rails.application.config.settings.env['FONE_VERIFY_CUST_ID']}
	        )
	response
end


end
