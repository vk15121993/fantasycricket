# bundle exec rake team:create_nickname
namespace :team do
  desc "make team nicknames start"
  task create_nickname: :environment do
    ParticipantTeam.all.each do |team|
	  team.short_name = (team.name.split.count == 1) ?  (team.name[0..2].upcase) :  (team.name.split.map(&:first).join.upcase)
      team.save!
      puts "Team #{team.name} short_name: #{team.short_name}"
    end
  end
end
