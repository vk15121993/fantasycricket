namespace :league_processing do
  desc "League Processing Starts at #{ Time.current }."
  task contests: :environment do
    Match.starts_within(30).each do |match|
      match.process_leagues
    end
    puts "League Processing stopped at #{ Time.current }."
  end
end
