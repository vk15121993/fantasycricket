desc 'Reset all data for demo servers'
task :reset_all_data => :environment do
  User.destroy_all
end

namespace :db do
  desc "Fill database with sample data"


  task populate: :environment do
    ###
    ### Users
    ###
    # t = Tournament.create( name: 'IPL 2015')

    # PLAYERS = {  
    #   "Chennai Super Kings" =>  [ "MS Dhoni", "Ashish Nehra", "Baba Aparajith", "Brendon McCullum", "Dwayne Bravo", "Dwayne Smith", "Faf du Plessis", "Ishwar Pandey", "Matt Henry", "Mithun Manhas", "Mohit Sharma", "Pawan Negi", "R Ashwin", "Ravindra Jadeja", "Samuel Badree", "Suresh Raina", "Ronit More", "Michael Hussey", "Rahul Sharma", "Kyle Abbott", "Irfan Pathan", "Pratyush Singh", "Ankush Bains", "Eklavya Dwivedi", "Andrew Tye" ],
    #   "Delhi Daredevils" =>  [ "Jean-Paul Duminy", "Kedar Jadhav", "Manoj Tiwary", "Mohammed Shami", "Nathan Coulter-Nile", "Quinton De Kock", "Saurabh Tiwary", "Shahbaz Nadeem", "Mayank Agarwal", "Imran Tahir", "Jayant Yadav", "Angelo Mathews", "Yuvraj Singh", "Amit Mishra", "Jaydev Unadkat", "Gurinder Sandhu", "Shreyas Iyer", "CM Gautam", "Dominic Muthuswamy", "Albie Morkel", "Travis Head", "Marcus Stoinis", "Kona Srikar Bharat", "KK Jiyaz", "Zaheer Khan" ], 
    #   "Kings XI Punjab" =>  [ "Axar Patel", "Anureet Singh", "Beuran Hendricks", "David Miller", "George Bailey", "Glenn Maxwell", "Gurkeerat Singh Mann", "Karanveer Singh", "Manan Vora", "Mitchell Johnson", "Parvinder Awana", "Rishi Dhawan", "Sandeep Sharma", "Shardul Thakur", "Shaun Marsh", "Shivam Sharma", "Thisara Perera", "Virender Sehwag", "Wriddhiman Saha", "Murali Vijay", "Nikhil Naik", "Yogesh Golwalkar" ],
    #   "Kolkata Knight Riders" =>  [ "Gautam Gambhir", "Andre Russell", "Chris Lynn", "Kuldeep Yadav", "Manish Pandey", "Suryakumar Yadav", "Morne Morkel", "Patrick Cummins", "Piyush Chawla", "Robin Uthappa", "Ryan ten Doeschate", "Shakib Al Hasan", "Sunil Narine", "Umesh Yadav", "Veer Pratap Singh", "Yusuf Pathan", "James Neesham", "Brad Hogg", "Aditya Garhwal", "Sumit Narwal", "KC Cariappa", "Vaibhav Rawal", "Sheldon Jackson" ],
    #   "Mumbai Indians" =>  [ "Rohit Sharma", "Aditya Tare", "Ambati Rayudu", "Corey Anderson", "Harbhajan Singh", "Jasprit Bumrah", "Josh Hazlewood", "Keiron Pollard", "Lasith Malinga", "Marchant de Lange", "Pawan Suyal", "Shreyas Gopal", "Lendl Simmons", "Unmukt Chand", "R Vinay Kumar", "Parthiv Patel", "Aaron Finch", "Pragyan Ojha", "Mitchell McClenaghan", "Akshay Wakhare", "Aiden Blizzard", "Hardik Pandya", "Siddhesh Lad", "J Suchith", "Nitish Rana", "Abhimanyu Mithun" ],
    #   "Rajasthan Royals" =>  [ "Shane Watson", "Abhishek Nayar", "Ajinkya Rahane", "Ankit Nagendra Sharma", "Ben Cutting", "Deepak Hooda", "Dhawal Kulkarni", "Dishant Yagnik", "James Faulkner", "Kane Richardson", "Karun Nair", "Pravin Tambe", "Rahul Tewatia", "Rajat Bhatia", "Sanju Samson", "Steven Smith", "Stuart Binny", "Tim Southee", "Vikramjeet Malik", "Chris Morris", "Juan Theron", "Barinder Singh Saran", "Dinesh Salunkhe", "Sagar Trivedi", "Pardeep Sahu" ],
    #   "Royal Challengers Bangalore" =>  [ "Virat Kohli", "AB de Villers", "Chris Gayle", "Mitchell Starc", "Nic Maddinson", "Varun Aaron", "Yuzvendra Singh Chahal", "Rilee Rossouw", "Vijay Zol", "Yogesh Takawale", "Abu Nechim Ahmed", "Harshal Patel", "Ashoke Dinda", "Sandeep Warrier", "Manvinder Bisla", "Iqbal Abdullah", "Dinesh Karthik", "Subramaniam Badrinath", "Darren Sammy", "Sean Abbott", "Adam Milne", "David Wiese", "Jalaj Saxena", "Sarfaraz Naushad Khan", "Shishir Bhavane" ],
    #   "Sunrisers Hyderabad" =>  [ "Shikhar Dhawan", "Ashish Reddy", "Bhuvneshwar Kumar", "Chama Milind", "Dale Steyn", "David Warner", "Ishant Sharma", "Karn Sharma", "KL Rahul", "Moises Henriques", "Naman Ojha", "Parveez Rasool", "Ricky Bhui", "Kevin Pietersen", "Eoin Morgan", "Kane Williamson", "Ravi Bopara", "Laxmi Ratan Shukla", "Praveen Kumar", "Trent Boult", "Hanuma Vihari", "Prasanth Padmanabhan", "Siddarth Kaul" ]
    # }
    # t1 = Tournament.find_by_name('India tour of Bangladesh, 2015')
    PLAYERS1 = {  
      'Bangladesh' =>  [ "Taijul Islam", "Mohammad Shahid", " Rubel Hossain", " Jubair Hossain", "Litton Das", "Shuvagata Hom", "Soumya Sarkar", "Shakib Al Hasan", "Nasir Hossain", "Mominul Haque", "Imrul Kayes", "Tamim Iqbal", "Mushfiqur Rahim", "Abul Hasan" ],
      'India' => ["Virat Kohli",  "Ishant Sharma",  "Varun Aaron",  "Umesh Yadav",  "Bhuvneshwar Kumar",  "Karn Sharma",  "Harbhajan Singh", "Ravichandran Ashwin", "Wriddhiman Saha",  "Rohit Sharma", "Ajinkya Rahane", "Cheteshwar Pujara",  "Shikhar Dhawan", "Murali Vijay"]
    }

    Player.delete_all
    PLAYERS1.each do |k,v|
      create_teams_players(k,v)
    end

  end

  def create_teams_players(k,v)
    p = ParticipantTeam.find_by_name(k)

    puts "ParticipantTeam found: "
    puts p.to_yaml

    v.each do |player_name|
      pp = Player.create!(
        name: player_name,
        player_category: PLAYER_CATEGORIES[rand(PLAYER_CATEGORIES.length)],
        participant_team_id: p.id,
        player_xp: rand(100))
      
      puts " Team Player created: "
      puts pp.to_yaml

    end
    puts "------------------------------------------------------------------------"
  end
end
