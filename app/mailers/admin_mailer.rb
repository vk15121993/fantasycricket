class AdminMailer < ActionMailer::Base
  default from: Rails.application.config.settings.mail.from
  layout 'emails/email2'

  AdminMailer.raise_delivery_errors do
    AdminMailer.mail_delivery_unseccessfull
  end

  def mail_delivery_unseccessfull
    p "======================MAIL DELIVERY FAILED========================="
  end

  def notify_admin_about_failed_payment(user_transaction)
    @user_transaction = user_transaction
    @match = user_transaction.match
    mail to: "play@fantoss.com", subject: "#{@match.team_names}: Payment failed."
  end

  def notify_admin_about_successful_payment(user_transaction)
    @user_transaction = user_transaction
    @match = user_transaction.match
    mail to: "play@fantoss.com", subject: "#{@match.team_names}: Payment made successfully."
  end

  def notify_admin_about_winners(league)
    @match = league.match
    @user = user_team.user
    mail to: "play@fantoss.com", subject: "#{@match.team_names} winners. Paid contest."
  end

  def notify_admin_about_sms_credits(data)
    @data = data
    mail to: [Rails.application.config.settings.mail.from, "play@fantoss.com"], subject: "SMS Notifications Credits Statements"
  end

end
