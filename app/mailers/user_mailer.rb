

class UserMailer < ActionMailer::Base
  default from: Rails.application.config.settings.mail.from
  # layout 'emails/email2'
  layout 'application_mailer'

  UserMailer.raise_delivery_errors do
    UserMailer.mail_delivery_unseccessfull
  end

  def mail_delivery_unseccessfull
    P "======================MAIL DELIVERY FAILED========================="
  end

  def welcome_email(user)
    return false unless load_user(user).present?
    mail(to: @user.email, subject: I18n.t('emails.welcome.subject')) if @user.email.present?
  end

  def notify_league_processing(user)
    return false unless load_user(user).present?
    mail to: @user.email, subject: I18n.t('emails.notify_league_processing.subject')
  end

  def general_email(user, data = {})
    return false unless load_user(user).present?

    ## Below code is to set the instance variables dynamically
    #  E.g.
    #     data = { intro_text: 'Some introduction', more_details: 'Some other details' }
    #  Above code would declare the following:
    #     @intro_text = 'Some introduction'
    #     @more_details = 'Some other details'

    ## This way the HAML template can be added with more different instance variables
    #   without breaking th existing functionality.

    data.each do |key, value|
      self.instance_variable_set "@#{ key }", value
    end
    mail to: @user.email, subject: (data[:subject] || 'fantoss.com')
  end

  protected

  def load_user(user)
    @user = user.is_a?(User) ? user : User.find(user)
  end
end
