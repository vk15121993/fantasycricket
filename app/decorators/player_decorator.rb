class PlayerDecorator < Draper::Decorator
  delegate :id, :name, :participant_team, :player_category, :price, :image, :odi_career, :ipl_career, :t20_career, :match, :player_scores, :test_career

  def thumb
    # image.url
  end

  def name_with_details
    "#{ name } (#{ participant_team.name })"
    # "#{ name } (#{ participant_team.name }) - #{ player_category.try(:humanize) }"
  end

  def category_with_details
    "#{ player_category.try(:humanize) } (#{ participant_team.name })"
  end

  def current_match_score(m)
    # need to edit this part
    pp = player_scores.where(match_id: m.id)
    ps = pp.select(
      "SUM(bat_singles) as bat_singles, " +
      "SUM(bat_fours) as bat_fours, " +
      "SUM(bat_sixes) as bat_sixes, " +
      "SUM(bat_fifties) as bat_fifties, " +
      "SUM(bat_hundreds) as bat_hundreds, " +
      "SUM(bat_double_hundreds) as bat_double_hundreds, " +
      "SUM(bat_duck) as bat_duck, " +
      "SUM(bat_score) as bat_score, " +
      "SUM(bowl_wickets) as bowl_wickets, " +
      "SUM(bowl_maidens) as bowl_maidens, " +
      "SUM(bowl_score) as bowl_score, " +
      "SUM(bat_score + bowl_score) as total"
    ).to_a.first if pp.present?
    # pp = player_scores.select("SUM(bat_singles) as bat_singles, SUM(bat_fours) as bat_fours, SUM(bat_sixes) as bat_sixes, SUM(bat_fifties) as bat_fifties, SUM(bat_hundreds) as bat_hundreds, SUM(bat_duck) as bat_duck, SUM(bat_score) as bat_score").where(match_id: m.id).to_a.first
    ps = PlayerScore.new(match_id: m.id, player_id: id) unless ps.present?
    ps
  end

  def past_match_scores
    player_scores.order('created_at DESC').limit(5).pluck(:total)
  end

  def stats(mtype)
    # case mtype
    # when 'odi'
    #   return_data(odi_career)
    # when 't20'
    #   return_data(t20_career)
    # when 'ipl'
    #   return_data(ipl_career)
    # when 'test'
    #   return_data(test_career)
    # end
    {}

  end

private

  def return_data(career_type)

    blank_data = {
         "Matches"=>"-",
         "Runs"=>"-",
         "Average"=>"-",
         "StrikeRate"=>"-",
         "Hundreds"=>"-",
         "Fifties"=>"-",
         "Fours"=>"-",
         "Sixes"=>"-"
      }

    return blank_data if career_type.blank? || career_type['Batting'].blank?
    career_type['Batting']

  end


  def odi_stats
    "ODI Avg : #{ odi_career['Batting']['Average'] }, Runs: #{ odi_career['Batting']['Runs'] }" if odi_career['Batting'].present?
  end

  def t20_stats
    "T20 Avg : #{ t20_career['Batting']['Average'] }, Runs: #{ t20_career['Batting']['Runs'] }" if t20_career['Batting'].present?
  end

  def ipl_stats
    "IPL Avg : #{ ipl_career['Batting']['Average'] }, Runs: #{ ipl_career['Batting']['Runs'] }" if ipl_career['Batting'].present?
  end
end
