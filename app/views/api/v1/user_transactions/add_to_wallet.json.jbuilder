if @user_transaction
	json.responseCode 200
	json.responseMessage "redirecting to Paytm.."
  json.link "https://web.fantoss.com/payment/mobile?transaction_id=#{@user_transaction.token}&gateway=1"
  json.mock_link "https://web.fantoss.com/payment/mobile?transaction_id=#{@user_transaction.token}&gateway=1&mode=test"
else
	json.responseCode 404
	json.responseMessage "Bad request"
end
