if @transactions
	json.responseCode 200
	json.responseMessage "transactions fetched."
	json.transactions @transactions
else
	json.responseCode 404
	json.responseMessage "No user transactions found"
end
