if @is_valid
	json.responseCode  200
	json.responseMessage  "Signed in successfully!"
	json.user current_user.as_json(
		only: [:id, :authentication_token, :image_url, :email, :phone, :username, :wallet_balance, :referral_code]
	).merge(new_user: @new_user)
else
	if @retry_flag
		json.responseCode  400
		json.responseMessage  "Request Timeout,we have sent another OTP to your phone.Enter OTP again."
	elsif @wrong_otp
		json.responseCode  401
		json.responseMessage  "Wrong OTP provided,please enter correct OTP again."
	else
		json.responseCode  504
		json.responseMessage "Oops!! Something went wrong."
	end
end

