require "administrate/base_dashboard"

class UserTeamDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    user: Field::BelongsTo,
    rank: Field::Number,
    totalscore: Field::Number,
    match: Field::BelongsTo,
    league: Field::BelongsTo,
    captain_id: Field::Number,
    captain_id1: Field::Number,
    players: Field::HasMany,
    has_won: Field::Boolean,
    token: Field::String,
    transaction_data: Field::String.with_options(searchable: false),
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :match,
    :league,
    :user,
    :rank,
    :totalscore,
  ]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :players,
    :match,
    :league,
    :user,
    :totalscore,
    :has_won,
    :rank,
    :token,
    :transaction_data,
    :captain_id,
  ]

  # Overwrite this method to customize how user teams are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(user_team)
    "#{user_team.user.try(:display_name)}, #{user_team.match.display_name}"
  end
end
