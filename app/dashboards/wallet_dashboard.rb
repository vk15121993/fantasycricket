require "administrate/base_dashboard"

class WalletDashboard < Administrate::BaseDashboard

  ATTRIBUTE_TYPES = {
    id: Field::Number,
    user: Field::HasOne,
    main_balance: Field::Number,
    referral_balance: Field::Number
  }

  COLLECTION_ATTRIBUTES = [
    :id,
    :user,
    :main_balance,
    :referral_balance
  ]

  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

end
