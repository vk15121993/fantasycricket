require "administrate/base_dashboard"

class WalletTransactionDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    wallet: Field::BelongsTo,
    banked_credit: Field::BelongsTo,
    id: Field::Number,
    amount: Field::Number,
    transaction_type: Field::String,
    current_wallet_amount: Field::String,
    category: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :wallet,
    :user,
    :banked_credit,
    :amount,
    :transaction_type,
    :category,
    :current_wallet_amount,
    :created_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :user,
    :wallet,
    :banked_credit,
    :amount,
    :transaction_type,
    :current_wallet_amount,
    :category,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [].freeze

  # Overwrite this method to customize how authentications are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(authentication)
  #   "Authentication ##{authentication.id}"
  # end
end
