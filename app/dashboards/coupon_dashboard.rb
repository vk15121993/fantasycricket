require "administrate/base_dashboard"

class CouponDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    title: Field::String,
    code: Field::String,
    discount_value: Field::Number,
    discount_type: Field::String,
    league_type: Field::String,
    concession_type: Field::String,
    maximum_discount_value: Field::Number,
    coupon_message: Field::String,
    start_date: Field::DateTime,
    end_date: Field::DateTime,
    maximum_redeem_count: Field::Number,
    current_redeem_count: Field::Number,
    per_user_count: Field::Number,
    user_validation_eval_string: Field::String,
    league_validation_eval_string: Field::String,
    match_validation_eval_string: Field::String,
    tournament: Field::BelongsTo,
    match: Field::BelongsTo,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :code,
    :discount_value,
    :discount_type,
    :league_type,
    :concession_type,
    :maximum_discount_value,
    :start_date,
    :end_date,
    :maximum_redeem_count,
    :current_redeem_count,
    :per_user_count,
    :tournament,
    :match,
    :created_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :code,
    :discount_value,
    :discount_type,
    :league_type,
    :concession_type,
    :maximum_discount_value,
    :coupon_message,
    :start_date,
    :end_date,
    :maximum_redeem_count,
    :current_redeem_count,
    :per_user_count,
    :user_validation_eval_string,
    :league_validation_eval_string,
    :match_validation_eval_string,
    :tournament,
    :match,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :code,
    :discount_value,
    :discount_type,
    :concession_type,
    :maximum_discount_value,
    :coupon_message,
    :start_date,
    :end_date,
    :league_type,
    :maximum_redeem_count,
    :per_user_count,
    :user_validation_eval_string,
    :league_validation_eval_string,
    :match_validation_eval_string,
    :tournament,
    :match
  ].freeze

  # Overwrite this method to customize how coupons are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(coupon)
  #   "Coupon ##{coupon.id}"
  # end
end
