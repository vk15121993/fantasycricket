require "administrate/base_dashboard"

class MatchDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    leagues: Field::HasMany,
    player_scores: Field::HasMany,
    players: Field::HasMany,
    user_teams: Field::HasMany,
    users: Field::HasMany,
    tournament: Field::BelongsTo,
    home_team: Field::BelongsTo.with_options(class_name: "ParticipantTeam"),
    away_team: Field::BelongsTo.with_options(class_name: "ParticipantTeam"),
    id: Field::Number,
    home_team_id: Field::Number,
    away_team_id: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    start_time: Field::DateTime,
    end_time: Field::DateTime,
    is_live: Field::Boolean,
    title: Field::String,
    api_match_id: Field::String,
    mtype: Field::String,
    team_names: Field::String,
    ms: Field::String,
    transaction_total: Field::String,
    is_payment_processed: Field::Boolean
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :team_names,
    :start_time,
    :ms,
    :user_teams,
    :player_scores,
    :players,
    :transaction_total,
    :is_payment_processed
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :leagues,
    :is_payment_processed,
    :player_scores,
    :id,
    :players,
    :user_teams,
    :users,
    :tournament,
    :home_team,
    :away_team,
    :home_team_id,
    :away_team_id,
    :created_at,
    :updated_at,
    :start_time,
    :end_time,
    :is_live,
    :title,
    :api_match_id,
    :mtype,
    :team_names,
    :ms,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :leagues,
    :player_scores,
    :players,
    :user_teams,
    :users,
    :tournament,
    :home_team,
    :away_team,
    :home_team_id,
    :away_team_id,
    :start_time,
    :end_time,
    :is_live,
    :title,
    :api_match_id,
    :mtype,
    :team_names,
    :ms,
  ].freeze

  # Overwrite this method to customize how matches are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(match)
  #   match.display_name
  # end

end
