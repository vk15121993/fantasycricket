require "administrate/base_dashboard"

class UserDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    matches: Field::HasMany,
    user_teams: Field::HasMany,
    authentications: Field::HasMany,
    display_name: Field::String.with_options(searchable: false),
    username: Field::String,
    phone: Field::String,
    email: Field::String,
    wallet_data: Field::String.with_options(searchable: false),
    credits: Field::Number,
    first_name: Field::String,
    last_name: Field::String,
    image_url: Field::String,
    date_of_birth: Field::DateTime,
    state: Field::String,
    country: Field::String,
    sign_in_count: Field::Number,
    current_sign_in_at: Field::DateTime,
    last_sign_in_at: Field::DateTime,
    current_sign_in_ip: Field::String,
    last_sign_in_ip: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    is_admin: Field::Boolean,
    is_spam_referrer: Field::Boolean,
    referred_by_id: Field::Number,
    meta_info: Field::String.with_options(searchable: false),
  }

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :display_name,
    :phone,
    :email,
    :wallet_data
  ]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :first_name,
    :last_name,
    :image_url,
    :email,
    :username,
    :phone,
    :is_spam_referrer
  ]

  # Overwrite this method to customize how users are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(user)
    "#{user.display_name}"
  end
end
