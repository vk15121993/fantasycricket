require "administrate/base_dashboard"

class AuthenticationDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    id: Field::Number,
    provider: Field::String,
    proid: Field::String,
    token: Field::String,
    refresh_token: Field::String,
    secret: Field::String,
    expires_at: Field::DateTime,
    username: Field::String,
    image_url: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :user,
    :provider,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user,
    :id,
    :provider,
    :proid,
    :token,
    :refresh_token,
    :secret,
    :expires_at,
    :username,
    :image_url,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user,
    :provider,
    :proid,
    :token,
    :refresh_token,
    :secret,
    :expires_at,
    :username,
    :image_url,
  ].freeze

  # Overwrite this method to customize how authentications are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(authentication)
  #   "Authentication ##{authentication.id}"
  # end
end
