require "administrate/base_dashboard"

class PlayerScoreDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    player: Field::BelongsTo,
    match: Field::BelongsTo,
    id: Field::Number,
    bat_runs_scored: Field::Number,
    bat_balls: Field::Number,
    bat_fours: Field::Number,
    bat_sixes: Field::Number,
    bat_singles: Field::Number,
    bat_duck: Field::Number,
    bat_hundreds: Field::Number,
    bat_fifties: Field::Number,
    bat_status: Field::String,
    bat_score: Field::Number,
    bat_sr: Field::String.with_options(searchable: false),
    bowl_overs: Field::Number,
    bowl_maidens: Field::Number,
    bowl_runs: Field::Number,
    bowl_wickets: Field::Number,
    bowl_wides: Field::Number,
    bowl_noballs: Field::Number,
    bowl_er: Field::String.with_options(searchable: false),
    field_catches: Field::Number,
    field_stumpings: Field::Number,
    field_runouts: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    bowl_score: Field::Number,
    field_score: Field::Number,
    total: Field::Number,
    inning_no: Field::Number,
  }

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :player,
    :match,
    :total,
  ]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :player,
    :match,
    :bat_runs_scored,
    :bat_balls,
    :bat_fours,
    :bat_sixes,
    :bat_singles, 
    :bat_duck, 
    :bat_hundreds, 
    :bat_fifties, 
    :bat_status, 
    :bat_score, 
    :bowl_overs,
    :bowl_maidens,
    :bowl_runs,
    :bowl_wickets,
    :bowl_wides,
    :bowl_noballs,
    :bowl_er,
    :field_catches,
    :field_stumpings,
    :field_runouts,
    :bat_score,
    :bowl_score,
    :field_score,
    :total,
  ]

  # Overwrite this method to customize how player scores are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(player_score)
    "#{player_score.total}"
  end
end
