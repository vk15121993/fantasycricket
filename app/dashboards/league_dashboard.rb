require "administrate/base_dashboard"

class LeagueDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user_teams: Field::HasMany,
    user_transactions: Field::HasMany,
    match: Field::BelongsTo,
    contest: Field::BelongsTo,
    id: Field::Number,
    name: Field::String,
    limit: Field::Number,
    prize_money: Field::Number,
    entry_fee: Field::Number,
    invite_code: Field::String,
    user_teams_count: Field::Number,
    is_private: Field::Boolean,
    is_tied: Field::Boolean,
    is_fake: Field::Boolean,
    created_at: Field::DateTime,
    updated_at: Field::DateTime
  }

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :user_teams,
    :match,
  ]

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = ATTRIBUTE_TYPES.keys

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user_teams,
    :match,
    :contest,
    :name,
    :limit,
    :is_tied,
    :is_private,
    :prize_money,
    :is_multiple_allowed,
    :entry_fee,
  ]

  # Overwrite this method to customize how leagues are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(league)
    "#{league.entry_fee}/#{league.prize_money}/#{league.limit} - #{league.match.team_names}"
  end
end
