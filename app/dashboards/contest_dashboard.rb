require "administrate/base_dashboard"

class ContestDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user_teams: Field::HasMany,
    leagues: Field::HasMany,
    id: Field::Number,
    questions_hash: Field::String.with_options(searchable: false),
    title: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    team_type: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :user_teams,
    :leagues,
    :questions_hash,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :user_teams,
    :leagues,
    :id,
    :questions_hash,
    :title,
    :created_at,
    :updated_at,
    :team_type,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :user_teams,
    :leagues,
    :questions_hash,
    :title,
    :team_type,
  ].freeze

  # Overwrite this method to customize how contests are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(contest)
  #   "Contest ##{contest.id}"
  # end
end
