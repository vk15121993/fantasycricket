require "administrate/base_dashboard"

class MoneyTransactionDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo,
    user_team_id: Field::Number,
    id: Field::Number,
    request_details: Field::String,
    response_details: Field::String,
    last_verification_request_details: Field::String,
    last_verification_response_details: Field::String,
    amount: Field::String,
    status: Field::String,
    provider: Field::String,
    details: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :user_team_id,
    :user,
    :provider,
    :amount,
    :status,
    :created_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :user_team_id,
    :user,
    :request_details,
    :response_details,
    :last_verification_request_details,
    :last_verification_response_details,
    :amount,
    :status,
    :provider,
    :details,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [].freeze

  # Overwrite this method to customize how authentications are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(authentication)
  #   "Authentication ##{authentication.id}"
  # end
end
