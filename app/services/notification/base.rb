class Notification::Base
  attr_accessor :recipient, :subject, :content_plain, :content_html

  # Abstract Methods: Children classes must declare these methods
  def _send(options = {})
    raise 'Abstract method undefined!'
  end

  # Instance Methods
  def initialize(initial_data = {})
    @recipient = initial_data[:recipient]
    @subject = initial_data[:subject]
    @content_plain = initial_data[:content_plain]
    @content_html = initial_data[:content_html]
  end

end
