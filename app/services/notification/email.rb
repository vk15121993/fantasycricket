class Notification::Email < Notification::Base

  def _send(options = {})
    mailer = options[:mailer].constantize
    mail_method = options[:mail_method]
    mail_params = options[:mail_params]

    if mailer.present?
      mailer.send(mail_method, *mail_params).deliver_now

    elsif recipient.try(:email).present?
      UserMailer.delay.general_email(
        recipient,
        {
          subject: subject,
          intro_text: subject,
          more_details: content_html || content_plain
        }
      )
    else
      notify_error
    end
  end

  private

  def notify_error
    logger.error('Mailer/Email not present for sending email notification.')
    logger.error("Options: #{options.inspect}\nRecipient Class: #{ recipient.class }, Recipient Id: #{ recipient.try :id }")
  end
end
