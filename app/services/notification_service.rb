class NotificationService
  attr_accessor :recipient, :channels, :subject, :content_plain, :content_html, :recipient_id

  # CONSTANTS
  CHANNELS = [:email, :sms, :push]

  def initialize(data = {})
    @recipient = data[:recipient]
    @recipient = User.find(data[:recipient_id]) if data[:recipient_id].present?
    @subject = data[:subject]
    @content_plain = data[:content_plain]
    @content_html = data[:content_html]
  end

  def _send(options = {})
    raise 'Recipient absent!' unless recipient

    @channels = options[:channels] || CHANNELS

    @channels.each do |channel|
      "Notification::#{ channel.capitalize }".constantize.new(
        recipient: recipient,
        subject: subject,
        content_plain: content_plain,
        content_html: content_html
      )._send(options)
    end
  end
end
