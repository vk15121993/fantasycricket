class MoneyTransferService
  include HTTParty

  # Constants
  PAYTM_CASHBACK_CRED = Rails.application.config.settings.env['paytm_pay_to_user']
  MIN_TXN_AMOUNT_FOR_TXN_FEES = 501
  TXN_FEE_PRECENT = 16.0

  base_uri PAYTM_CASHBACK_CRED['base_uri']

  PayTM = {
    merchant_id: PAYTM_CASHBACK_CRED['merchant_id'],
    aes_key: PAYTM_CASHBACK_CRED['aes_key'],
    sales_wallet_id: PAYTM_CASHBACK_CRED['sales_wallet_id']
  }

  def self.transfer_for(user_id, options = {})
    @user = User.find_by(id: user_id)

    # unless Rails.env.production?
    #   @user.update(failed_attempts: 0)
    #   return true
    # end

    last_user_transanction = @user.user_transactions.last
    if last_user_transanction && last_user_transanction.created_at > (Time.current - 15.seconds)
      @user.update(failed_attempts: 0)
      return false
    end

    @amount = options[:amount] || @user.wallet.main_balance
    @metadata = options[:metadata]
    @money_transaction_type = options[:money_transaction_type]
    # Txn Fees Calculation
    if @money_transaction_type.blank?
      @paytm_amount = @amount - (@amount * TXN_FEE_PRECENT / 100.0)
      @paytm_amount = @paytm_amount.round(2)
    end

    if options[:details].present?
      options[:details][:paytm_amount] = @paytm_amount
    else
      options[:details] = { paytm_amount: @paytm_amount }
    end

    money_transaction = MoneyTransaction.create!(
      provider: 'PayTM',
      amount: @amount,
      user: @user,
      money_transaction_type: @money_transaction_type || 'WITHDRAW_TO_BANK',
      details: options[:details]
    )
    if money_transaction
      MoneyTransaction.transaction do
        response = post(
          '/wallet-web/salesToUserCredit',
          { body: paytm_request_body.to_json, headers: paytm_request_headers }
        )
        @user.update(failed_attempts: 0)
        update_new_money_transaction_with response, money_transaction
      end
    end
  end

  def self.check_transaction_status_for(money_transaction_id)
    money_transaction = MoneyTransaction.find_by(id: money_transaction_id)
    if money_transaction
      response = post(
        '/wallet-web/checkStatus',
        {
          body: paytm_check_transation_status_body(money_transaction).to_json,
          headers: paytm_check_transation_status_header(money_transaction)
        }
      )
      update_money_transaction_with response, money_transaction
    end
  end


  private

  def self.update_new_money_transaction_with(response, money_transaction)
    parsed_response = JSON.parse(response.parsed_response)
    money_transaction.update!(
      request_details: response.request.options.slice(:base_uri, :headers, :body, :default_params),
      response_details: parsed_response,
      status: parsed_response['status']
    )
  end

  def self.update_money_transaction_with(response, money_transaction)
    parsed_response = JSON.parse(response.parsed_response)
    money_transaction.update(
      last_verification_request_details: response.request.options.slice(:base_uri, :headers, :body, :default_params),
      last_verification_response_details: parsed_response,
      status: parsed_response.try(:[],'response').try(:[],'txnList').try(:first).try(:[],'message')
    )
  end

  def self.paytm_check_transation_status_body(money_transaction)
    {
      request: {
        requestType: 'merchanttxnid',
        txnType: 'salestouser',
        txnId: money_transaction.response_details.try(:[],'orderId'),
        merchantGuid: PayTM[:merchant_id]
      },
      platformName: 'PayTM',
      operationType: 'CHECK_TXN_STATUS'
    }
  end

  def self.paytm_check_transation_status_header(money_transaction)
    paytm_request_headers(paytm_check_transation_status_body(money_transaction))
  end

  def self.paytm_request_body(request_type = nil)
    {
      request: {
        requestType: request_type,
        merchantGuid: PayTM[:merchant_id],
        merchantOrderId: "#{ @user.id }-#{ Time.current.to_i }",
        salesWalletName: nil,
        salesWalletGuid: PayTM[:sales_wallet_id],
        payeeEmailId: @user.email,
        payeePhoneNumber: @user.phone,
        payeeSsoId: '',
        appliedToNewUsers: 'Y',
        amount: @paytm_amount,
        currencyCode: 'INR'
      },
      metadata: @metadata || "FanTOSS withdraw amount. Play again at http://fantoss.com",
      ipAddress: '127.0.0.1',
      platformName: 'PayTM',
      operationType: 'SALES_TO_USER_CREDIT'
    }
  end

  def self.paytm_request_headers(request_body = paytm_request_body)
    {
      'Content-Type' => 'application/json',
      'Accept' => 'application/json',
      'mid' => PayTM[:merchant_id],
      'checksumhash' => generate_hash(PayTM[:aes_key], request_body)
    }
  end

  def self.generate_hash(key, params={})
    @checksum = SecureHash.new_pg_checksum_by_String(params.to_json, key).gsub("\n",'')
  end

end
