class UserCodeGenerator
  extend ActiveSupport::Concern

  def self.generate(name,email,min_length=4,max_length=10)
    length = min_length + rand(max_length - min_length)
    code = name.presence || email
    code = code.delete(" ")
    code += SecureRandom.hex(1) if code.size < 4
    code = code[0..length]
    code + rand(99).to_s
  end
end
