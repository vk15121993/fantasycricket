class ScoresPro::Match < ScoresPro::Base

  def self.populate_match(options = {})
      tournament_name = options[:data]['Information']['parentCompetition']['__content__']
      start_date = options[:data]['start_time'].remove(": ").to_datetime
      series_id = options[:series_id]
      # MATCH DATA
      home_team = ParticipantTeam.find_by_api_team_id options[:data]['Away']['id']
      away_team = ParticipantTeam.find_by_api_team_id options[:data]['Home']['id']

      tournament = Tournament.find_or_create_by(name: tournament_name , api_series_id: series_id, api_provider: 'scorespro' )
      return false unless home_team.present?

      if tournament
        tournament.update(details: options[:data])
        match_obj = Match.where(tournament_id: tournament.id, api_match_id: options[:data]['id'], api_provider: options[:api_provider])
        if match_obj.present?
          match_obj.last.update_attributes( home_team_id: home_team.id,
                            away_team_id: away_team.id,
                            start_time: start_date,
                            tournament_id: tournament.id,
                            title: options[:data]['Information']['round'],
                            api_match_id: options[:data]['id'],
                            mtype: options[:data]['match_type'],
                            team_names: "#{home_team.name}" + "vs  #{away_team.name}",
                            ms: 'Match is yet to start',
                            api_provider: 'scorespro')
        else
          Match.create( home_team_id: home_team.id,
                        away_team_id: away_team.id,
                        start_time: start_date,
                        tournament_id: tournament.id,
                        title: options[:data]['Information']['round'],
                        api_match_id: options[:data]['id'],
                        mtype: options[:data]['match_type'],
                        team_names: "#{home_team.name}" + " vs  #{away_team.name}",
                        ms: 'Match is yet to start',
                        api_provider: 'scorespro')

        end
      end
  end


end
