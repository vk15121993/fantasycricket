class ScoresPro::LiveScoreScoresPro < ScoresPro::Base

  def self.fetch(options = {})
    first_match_date = Match.ongoing.where(id: options[:match_ids]).pluck(:start_time).sort.first rescue Time.current
    first_match_date = first_match_date.utc
    current_date = Time.now
      if ((current_date - first_match_date ) <=> 5) == 1
        first_match_date = current_date - 5.days
      end
    first_match_date = first_match_date.strftime('%F')
    begin
      url = "http://data2.scorespro.com/exporter/?state=clientUpdate" +
        "&usr=#{ApplicationYML['env']['SCORES_PRO']['username']}" +
        "&pwd=#{ApplicationYML['env']['SCORES_PRO']['password']}" +
        "&s=14&type=26&start=#{ first_match_date }&end=#{ Time.now.strftime('%F') }"
      response = HTTParty.get(url)
    rescue Errno::ETIMEDOUT, Net::ReadTimeout
      retry
    end
    parsed_response = response.parsed_response
    p '***************************************'
    p "URL: #{ url }"
    # p parsed_response
    p '***************************************'

    if parsed_response['Export'].try(:[], 'GeneralInformation').try(:[], 'Error')
      p '***************************************'
      p 'Error from ScoresPro API'
      p parsed_response
      p '***************************************'
      return
    end

    match_day_data = parsed_response['Export']['Sport']['Matchday']
    match_data = []
    # Hash object to Array
    match_day_data = Array.wrap(match_day_data).flatten(1) if match_day_data.is_a?(Hash)
      match_day_data.each do |m_day|
        m_day_match = m_day['Match']
        m_day_match = [m_day_match] if m_day_match.is_a?(Hash)
        match_data << m_day_match
      end
    match_data

  end

  def self.sync(options = {})
    match_data = fetch(options)

    # p '*********************************'
    # match_data.each { |el| p el.class }
    # p '*********************************'

    if match_data
      match_data.each do |match_datum|
        match_datum.each do |md|
          # match = tournament['match']
          # p '*********************************'
          # # p match_datum
          # p md
          # p '*********************************'

          @match_obj = ::Match.find_by(
            api_match_id: md['id'],
            api_provider: options[:api_provider] || 'scorespro',
            id: options[:match_ids]
          )

          # Syncing batsman stats - START
          if @match_obj.present?
            # debugger
            innings = md['Innings']
            if innings
              all_innings = innings['Inning']
              all_innings = [all_innings] if all_innings.is_a?(Hash)
              all_innings.each do |inning_info|
                inning_no = inning_info['number']
                batsmanstats = inning_info['Battingline']
                batsmen = batsmanstats.present? ? batsmanstats['Batsman'] : []
                bowlingstats = inning_info['Bowlingline']
                bowlers = bowlingstats.present? ? bowlingstats['Bowler'] : []
                batsmen.each do |batsman|
                  players = ::Player.where(
                    api_player_id: batsman['id'],
                    api_provider: options[:api_provider] || 'scorespro',
                    participant_team_id: [@match_obj.home_team.id, @match_obj.away_team.id]
                  )
                  if players.count > 1
                    pls1 = players.first
                    pls2 = players.second
                    pt1 = @match_obj.home_team
                    pt2 = @match_obj.away_team

                    if pt1.current_squad.include?(pls1.id.to_s) || pt2.current_squad.include?(pls1.id.to_s)
                      player = pls1
                    elsif pt1.current_squad.include?(pls2.id.to_s) || pt2.current_squad.include?(pls2.id.to_s)
                      player = pls2
                    else
                      player = nil
                    end
                  else
                    player = players.first
                  end

                  if player.present?
                    ps = @match_obj.player_scores.where(player_id: player.id, inning_no: inning_no).first_or_initialize
                    ps.inning_no       = inning_no.to_i
                    ps.bat_runs_scored = batsman["score"].to_i
                    ps.bat_balls       = batsman["balls"].to_i
                    ps.bat_fours       = batsman["s4"].to_i
                    ps.bat_sixes       = batsman["s6"].to_i
                    ps.bat_status      = Player::WICKET_TYPES[batsman["wicket_type_id"]]
                    ps.save
                  end
                end
                bowlers.each do |bowler|
                  players = ::Player.where(
                    api_player_id: bowler['id'],
                    api_provider: options[:api_provider] || 'scorespro',
                    participant_team_id: [@match_obj.home_team.id, @match_obj.away_team.id]
                  )
                  if players.count > 1
                    pls1 = players.first
                    pls2 = players.second
                    pt1 = @match_obj.home_team
                    pt2 = @match_obj.away_team

                    if pt1.current_squad.include?(pls1.id.to_s) || pt2.current_squad.include?(pls1.id.to_s)
                      player = pls1
                    elsif pt1.current_squad.include?(pls2.id.to_s) || pt2.current_squad.include?(pls2.id.to_s)
                      player = pls2
                    else
                      player = nil
                    end
                  else
                    player = players.first
                  end

                  if player.present?
                    ps = @match_obj.player_scores.where(player_id: player.id, inning_no: inning_no).first_or_initialize
                    ps.inning_no       = inning_no.to_i
                    ps.bowl_wickets    = bowler["wkt"].to_i
                    ps.bowl_maidens    = bowler["mdn"].to_i
                    ps.save
                  end
                end
              end
            end
            ::UpdateUserTeamScoreWorker.new.perform(@match_obj.id)
            @match_obj.ms = md['status']
            @match_obj.save
            if md['status'] == 'Fin'
              @match_obj.update(end_time: Time.current)
              @match_obj.process_league_rankings
            end
          end
        end
        # Syncing batsman stats - END
      end
    end

  end

end
