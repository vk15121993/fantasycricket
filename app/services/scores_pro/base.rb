class ScoresPro::Base
  include HTTParty


  def self.destination_url(options = {})
    base_url = "http://data2.scorespro.com/exporter/?state=clientUpdate&usr=#{ApplicationYML['env']['SCORES_PRO']['username']}&pwd=#{ApplicationYML['env']['SCORES_PRO']['password']}"
    if options[:sync_squad] #SquadSyncing
      "#{base_url}&type=29&s=14&l=#{options[:league_id]}"
    elsif options[:sync_score] #BallByBall
      "#{base_url}&type=26&start=#{options[:first_match_date]}&end=#{(Date.today).strftime("%F")}"
    elsif options[:index_matches] #IndexingOfMatches
      "#{base_url}&type=5&s=14&start=#{(Date.today).strftime("%F")}&end=#{(Date.today+ 30.days).strftime("%F")}"
    end
  end

end
