class ScoresPro::Tournament < ScoresPro::Base

    def self.upcoming(options = {})

      if options[:api_provider] == 'scorespro'
        begin
          # url = "http://data2.scorespro.com/exporter/?state=clientUpdate&usr=#{ApplicationYML['env']['SCORES_PRO']['username']}&pwd=#{ApplicationYML['env']['SCORES_PRO']['password']}&type=5&s=14&start=#{(Date.today).strftime("%F")}&end=#{(Date.today+ 30.days).strftime("%F")}"
          url = destination_url({index_matches: true})
          response = HTTParty.get(url)
        rescue Errno::ETIMEDOUT, Net::ReadTimeout
          retry
        end
        parsed_response = response.parsed_response['Export']['Sport']

        matches = []
        if parsed_response

          parsed_response["Matchday"].each do |match_day_data|
            date = match_day_data['date']


            if match_day_data["Match"].is_a?(Array)
              match_day_data["Match"].each do |m|
                m['start_time'] = date + " : " +m['startTime']
                matches << m
              end
            else
              m = match_day_data['Match']
              m['start_time'] = date + " : " + m['startTime']
              matches << m
            end
          end

          matches.group_by { |val| val["leagueCode"] }

        else
          p 'Invalid response from API'
          p parsed_response
          []
        end
      end

    end

end
