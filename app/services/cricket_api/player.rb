class CricketApi::Player < CricketApi::Base

  def initialize(options = {})
    self.api_id = options[:api_id]
    self.api_provider = options[:api_provider]
  end

  def fetch(options = {})
    begin
      @response = HTTParty.get("#{ API_URLS[options[:api_provider] || api_provider] }/cricket/profile?id=#{ options[:api_id] || api_id }")
    rescue Errno::ETIMEDOUT, Net::ReadTimeout
      retry
    end
    parsed_response = @response.parsed_response
    player = parsed_response['players']['player'] if parsed_response
  end

  def self.first_or_create_by(options)
    player_obj = ::Player.where(options[:conditions]).first_or_initialize
    if player_obj.blank? || options[:force_refresh]
      api_player_obj = new({ api_id: options[:api_player_id], api_provider: options[:api_provider] })
      api_player = api_player_obj.fetch
      if api_player

        player_obj.name = api_player['name']
        player_obj.api_player_id = api_player['id']

        if ['Allrounder', 'Batting allrounder', 'Bowling allrounder'].include?(api_player['playing_role'])
          player_obj.player_category = 'all_rounder'
        elsif ['Batsman', 'Wicketkeeper batsman', 'Top-order batsman', 'Middle-order batsman', 'Opening batsman'].include?(api_player['playing_role'])
          player_obj.player_category = 'batsman'
        elsif ['Bowler'].include?(api_player['playing_role'])
          player_obj.player_category = 'bowler'
        else
          player_obj.player_category = 'all_rounder'
        end

        # File.open("/tmp/#{ options[:api_player_id] }_goalserve.png", 'wb') do|f|
        #   f.write(Base64.decode64(api_player['image']))
        # end

        # debugger
        # player_obj.image = Pathname.new("/tmp/#{ options[:api_player_id] }_goalserve.png").open
        player_obj.api_provider = options[:api_provider]
        player_stats_obj = player_obj.career_stats.where(api_provider: options[:api_provider]).first_or_initialize
        if api_player['stats']
          # debugger
          nk_obj = Nokogiri::HTML(api_player_obj.instance_variable_get(:@response).body)
          player_stats_obj.batting = {}
          player_stats_obj.bowling = {}
          player_stats_obj.fielding = {}
          nk_obj.css('batting_fielding').css('type').each do |career_type|
            # debugger
            career_type_h = Hash.from_xml(career_type.to_xml)
            player_stats_obj.batting[career_type_h['type'].try(:[], 'name')] = career_type_h['type']
          end
          nk_obj.css('bowling').css('type').each do |career_type|
            career_type_h = Hash.from_xml(career_type.to_xml)
            player_stats_obj.bowling[career_type_h['type'].try(:[], 'name')] = career_type_h['type']
          end
          nk_obj.css('batting_fielding').css('type').each do |career_type|
            career_type_h = Hash.from_xml(career_type.to_xml)
            player_stats_obj.fielding[career_type_h['type'].try(:[], 'name')] = career_type_h['type']
          end
        end

        # debugger unless player_stats_obj.valid?
        player_obj.save
        player_stats_obj.save
        # debugger
      else
        player_obj.name = options[:api_player_name]
        player_obj.player_category = 'all_rounder'
        # player_obj.image = Pathname.new("/var/app/current/public/sil_thumb.jpg").open
        player_obj.save
        # debugger
      end
    end
  end

end
