class CricketApi::Tournament < CricketApi::Base

  def self.upcoming(options = {})
    if options[:api_provider] == 'goalserve'
      tournaments = CricketApi::Match.upcoming(options)
      tournaments_hash = Hash.new { |hash, key| hash[key] = Hash.new { |hash, key| hash[key] = [] } }

      tournaments.each do |tournament|
        tournaments_hash[tournament['id']]['name'] = tournament['name']
        tournaments_hash[tournament['id']]['series_file'] = tournament['series_file']
        tournaments_hash[tournament['id']]['squads_file'] = tournament['squads_file']
        tournaments_hash[tournament['id']]['matches'] << tournament
      end
      tournaments_hash

    # elsif options[:api_provider] == 'scorespro'
    #   begin
    #     url = "http://data2.scorespro.com/exporter/?state=clientUpdate&usr=#{ApplicationYML['env']['SCORES_PRO']['username']}&pwd=#{ApplicationYML['env']['SCORES_PRO']['password']}&type=5&s=14&start=#{(Date.today).strftime("%F")}&end=#{(Date.today+ 30.days).strftime("%F")}"
    #     response = HTTParty.get(url)
    #   rescue Errno::ETIMEDOUT, Net::ReadTimeout
    #     retry
    #   end
    #   parsed_response = response.parsed_response['Export']['Sport']

    #   matches = []
    #     parsed_response["Matchday"].each do |match_day_data|
    #       date = match_day_data['date']
    #       if match_day_data["Match"].is_a?(Array)
    #         match_day_data["Match"].each do |m|
    #           m['start_time'] = date + " : " +m['startTime']
    #           matches << m
    #         end
    #       else
    #         matches << match_day_data
    #       end
    #     end
    #   matches.group_by{|val|  val["leagueCode"]}

    end

  end

  def self.sync_all(options = {})
    CricketApi::Match.sync_all(options)
  end

  # Instance Methods
  def sync(options = {})
    CricketApi::Match.new.sync(options)
  end

end
