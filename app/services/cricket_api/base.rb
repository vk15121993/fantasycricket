class CricketApi::Base
  include HTTParty

  # Constants
  API_URLS = HashWithIndifferentAccess.new({
    goalserve: 'http://www.goalserve.com/getfeed/9a802c4e20304e05af8faa6a224296ac/'
  })

  # Attr
  attr_accessor :api_provider, :api_id

end
