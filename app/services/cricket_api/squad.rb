class CricketApi::Squad < CricketApi::Base

  attr_accessor :file_path

  def initialize(options = {})
    self.file_path = options[:file_path]
  end

  def fetch(options = {})
    begin
      response = HTTParty.get("#{ API_URLS[:goalserve] }/cricketfixtures#{ options[:file_path] || file_path }")
    rescue Errno::ETIMEDOUT, Net::ReadTimeout
      retry
    end
    parsed_response = response.parsed_response
    if parsed_response
      # debugger
      teams = parsed_response['squads']['category']['team']
    end
  end

  def sync(options = {})
    teams = fetch(options)
    if teams
      teams.each do |team|
        pt_obj = ::ParticipantTeam.where(api_team_id: team['id'], api_provider: options[:api_provider] || api_provider).first_or_initialize do |pt|
          pt.name = team['name']
        end
        # debugger unless pt_obj.valid?
        pt_obj.save

        # Syncing Players - START
        if team['player']
          # debugger if pt_obj.id == 53
          team['player'].each do |player|
            CricketApi::Player.first_or_create_by(
              conditions: {
                api_player_id: player['name'],
                api_provider: options[:api_provider] || api_provider,
                participant_team_id: pt_obj.id,
              },
              api_player_name: player['id'],
              api_player_id: player['name'],
              api_provider: options[:api_provider] || api_provider,
              participant_team_id: pt_obj.id,
              force_refresh: options[:force_refresh]
            )
          end

          pt_obj.update(current_squad: pt_obj.players.where(api_player_id: team['player'].map { |pl| pl['name'] }).pluck(:id))
          # debugger if pt_obj.name == 'Hampshire'
        end

      end
    end
  end

  def sync_scorespro_squad(options = {})
   # [{"class_name":"CricketApi::Squad","method_name":"sync_scorespro_squad","class_method":false,"instance_arguments":{},"method_arguments":{"api_provider":"scorespro","league_id":"145414","force_refresh":true}}

  end

end
