class Entity::Cricket::Season < Entity::Cricket::Base

  ATTRIBUTES = [:sid, :name, :competitions, :competitions_url]
  attr_accessor *ATTRIBUTES

  # Public class methods
  def self.all
    response = HTTParty.get(
      "#{ URLS[:seasons] }",
      query: {
        token: CREDENTIALS[:access_token]
      }
    )
  end

  # Public instance methods
  def initialize(options = {})
    @sid = options['sid']
    @name = options['name']
    @competitions_url = options['competitions_url']
  end

  def attributes
    attributes_pair = {}
    ATTRIBUTES.each do |attribute|
      attributes_pair[attribute] = send(attribute)
    end
    attributes_pair
  end

  def competitions
    response = HTTParty.get(
      "#{ URLS[:seasons] }/#{ sid }/competitions",
      query: {
        token: CREDENTIALS[:access_token]
      }
    )

    @competitions = []

    response.parsed_response['response']['items'].each do |competition_details|
      competition = Entity::Cricket::Competition.new(competition_details)
      @competitions << competition
    end

    @competitions
  end

end
