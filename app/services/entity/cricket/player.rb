class Entity::Cricket::Player < Entity::Cricket::Base

  ATTRIBUTES = [:player_id, :role, :role_str, :title, :short_name, :first_name, :last_name, :middle_name,
    :birthdate, :birthplace, :country, :primary_team, :thumb_url, :logo_url, :playing_role, :batting_style,
    :bowling_style, :fielding_position, :recent_match, :recent_appearance]

  attr_accessor *ATTRIBUTES

  alias_method :pid, :player_id
  alias_method :pid=, :player_id=

  PLAYING_ROLES = {
    'bat' => 'batsman',
    'bowl' => 'bowler',
    'all' => 'all_rounder',
    'wk' => 'wicket_keeper',
    'wkbat' => 'wicket_keeper_batsman'
  }

  def initialize(options = {})
    @player_id = options['player_id'] || options['pid']
    @role = options['role']
    @role_str = options['role_str']
    @title = options['title']
    @short_name = options['short_name']
    @first_name = options['first_name']
    @last_name = options['last_name']
    @middle_name = options['middle_name']
    @birthdate = options['birthdate']
    @birthplace = options['birthplace']
    @country = options['country']
    @primary_team = options['primary_team']
    @thumb_url = options['thumb_url']
    @logo_url = options['logo_url']
    @playing_role = PLAYING_ROLES[options['playing_role']]
    @batting_style = options['batting_style']
    @bowling_style = options['bowling_style']
    @fielding_position = options['fielding_position']
    @recent_match = options['recent_match']
    @recent_appearance = options['recent_appearance']
  end

end
