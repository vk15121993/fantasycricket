class Entity::Cricket::Team < Entity::Cricket::Base

  attr_accessor :team_id, :name, :short_name, :logo_url, :scores_full, :scores, :overs, :squads

  def initialize(options = {})
    @team_id = options['team_id']
    @name = options['name']
    @short_name = options['short_name']
    @logo_url = options['logo_url']
    @scores_full = options['scores_full']
    @scores = options['scores']
    @overs = options['overs']
  end

end
