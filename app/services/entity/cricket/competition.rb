class Entity::Cricket::Competition < Entity::Cricket::Base

  ATTRIBUTES = [:cid, :title, :abbr, :season, :datestart, :dateend,
    :total_matches, :total_rounds, :total_teams, :category, :match_format,
    :status, :country, :type, :matches_url, :teams_url, :standings_url, :rounds,
    :squads, :matches]

  attr_accessor *ATTRIBUTES

  def initialize(options = {})
    @cid = options['cid']
    @title = options['title']
    @abbr = options['abbr']
    @season = options['season']
    @datestart = options['datestart']
    @dateend = options['dateend']
    @total_matches = options['total_matches']
    @total_rounds = options['total_rounds']
    @total_teams = options['total_teams']
    @category = options['category']
    @match_format = options['match_format']
    @status = options['status']
    @country = options['country']
    @type = options['type']
    @matches_url = options['matches_url']
    @teams_url = options['teams_url']
    @standings_url = options['standings_url']
    @rounds = options['rounds']
  end

  def attributes
    attributes_pair = {}
    ATTRIBUTES.each do |attribute|
      attributes_pair[attribute] = send(attribute)
    end
    attributes_pair
  end

  def set_matches
    response = HTTParty.get(
      "#{ URLS[:competition] }/#{ cid }/matches",
      query: {
        token: CREDENTIALS[:access_token]
      }
    )

    @matches = response.parsed_response['response']['items'].inject([]) do |memo, el|
      memo << Entity::Cricket::Match.new(el)
    end
  end

  def set_squads
    response = HTTParty.get(
      "#{ URLS[:competition] }/#{ cid }/squads",
      query: {
        token: CREDENTIALS[:access_token]
      }
    )

    @squads = response.parsed_response['response']['squads'].inject([]) do |memo, el|
      memo << Entity::Cricket::Squad.new(el)
    end
  end

  def sync!(options = {})
    tournament_obj = ::Tournament.find_by(api_provider: API_PROVIDER, api_series_id: cid)
    unless tournament_obj.present?
      tournament_obj = ::Tournament.new(
        api_provider: API_PROVIDER,
        api_series_id: cid,
        name: title
      )
      tournament_obj.save!
    end

    if options[:with_squads] == true
      sync_squads!
    end
  end

  def sync_squads!
    set_squads

    squads.each do |squad|
      pt_obj = ::ParticipantTeam.find_by(api_provider: API_PROVIDER, api_team_id: squad.team_id)

      if pt_obj.present?
        pt_obj.name = squad.title
        pt_obj.save!
      else
        pt_obj = ::ParticipantTeam.new(api_provider: API_PROVIDER, api_team_id: squad.team_id)
        pt_obj.name = squad.title
        pt_obj.save!
      end

      team_lineup = []

      squad.players.each do |player|
        player_obj = ::Player.find_by(api_provider: API_PROVIDER, api_player_id: player.player_id,
          participant_team_id: pt_obj.id)
        if player_obj.present?
          team_lineup << player_obj.id.to_s
        else
          player_obj = ::Player.new(api_provider: API_PROVIDER, api_player_id: player.player_id,
            participant_team_id: pt_obj.id)
          player_obj.name = player.title
          player_obj.save!
          team_lineup << player_obj.id.to_s
        end
      end


      pt_obj.current_squad = (pt_obj.current_squad.to_a + team_lineup).uniq
      pt_obj.save!
    end
  end

  def self.get(cid)
    response = HTTParty.get(
      "#{ URLS[:competition] }/#{ cid }",
      query: {
        token: CREDENTIALS[:access_token]
      }
    )
    new(response.parsed_response['response'])
  end

end
