class Entity::Cricket::Base < Entity::Base

  # Constants
  API_PROVIDER = 'entity'
  URLS = {
    seasons: "#{ BASE_URL }/seasons",
    matches: "#{ BASE_URL }/matches",
    competition: "#{ BASE_URL }/competitions"
  }

end
