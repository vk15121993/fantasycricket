class Entity::Cricket::Match::Batsman

  attr_accessor :batsman_id, :role, :role_str, :runs, :balls_faced, :fours,
    :sixes, :how_out, :dismissal, :strike_rate, :bowler_id, :first_fielder_id,
    :second_fielder_id, :third_fielder_id

  def initialize(options = {})
    @batsman_id = options['batsman_id']
    @role = options['role']
    @role_str = options['role_str']
    @runs = options['runs']
    @balls_faced = options['balls_faced']
    @fours = options['fours']
    @sixes = options['sixes']
    @how_out = options['how_out']
    @dismissal = options['dismissal']
    @strike_rate = options['strike_rate']
    @bowler_id = options['bowler_id']
    @first_fielder_id = options['first_fielder_id']
    @second_fielder_id = options['second_fielder_id']
    @third_fielder_id = options['third_fielder_id']
  end
end
