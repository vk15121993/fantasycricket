class Entity::Cricket::Match::Venue

  attr_accessor :name, :location, :timezone

  def initialize(options = {})
    @name = options['name']
    @location = options['location']
    @timezone = options['timezone']
  end
end
