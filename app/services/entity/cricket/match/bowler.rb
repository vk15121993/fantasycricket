class Entity::Cricket::Match::Bowler

  attr_accessor :bowler_id, :overs, :maidens, :runs_conceded, :wickets, :noballs, :wides, :econ

  def initialize(options = {})
    @bowler_id = options['bowler_id']
    @overs = options['overs']
    @maidens = options['maidens']
    @runs_conceded = options['runs_conceded']
    @wickets = options['wickets']
    @noballs = options['noballs']
    @wides = options['wides']
    @econ = options['econ']
  end
end
