class NotificationSchedularService

  def self.schedule
    # notify_referal_credit_expiry
    send_admin_match_data # Sending Inplay to Admins
    user_ids = User.where('created_at > ?', Time.current - 5.days).pluck(:id)
    user_ids += [6506, 5910, 2071, 5289, 14648, 93688]
    todays_matches = Match.where('start_time > ? AND start_time < ?', Time.current + 2.hours, Time.current.end_of_day)
    selected_match = todays_matches.sample
    upcoming_series = Tournament.upcoming.where('start_time <= ?', (Time.current + 2.days).end_of_day).pluck(:name)

    if selected_match
      # schedule_time = selected_match.start_time - 90.minutes
      # threshold_time = Time.current.beginning_of_day + 20.hours + 30.minutes

      # if schedule_time > threshold_time
      #   schedule_time = threshold_time
      # end
      schedule_time = Time.current + 1.hour

      sms_list = (
        match_sms({ home_team_name: selected_match.try(:home_team).try(:name), away_team_name: selected_match.try(:away_team).try(:name) }) +
        series_sms({ series_name: upcoming_series.sample }) +
        generic_sms +
        prompt_to_play_sms +
        refer_and_earn_sms({ refer_win_money: 20 })
      )

      send_notification(schedule_time, user_ids, sms_list.sample )

    else
      schedule_time = Time.current.beginning_of_day + 10.hours
      sms_list = (
        series_sms({ series_name: upcoming_series.sample }) +
        generic_sms +
        prompt_to_play_sms +
        refer_and_earn_sms({ refer_win_money: 20 })
      )

      send_notification(schedule_time, user_ids, sms_list.sample )
    end
  end

  private

  def self.match_sms(options = {})
    home_team_name = options[:home_team_name]
    away_team_name = options[:away_team_name]
    url = options[:url]
    url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=match_sms_t1')
    url2 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=match_sms_t2')
    url3 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=match_sms_t3')
    url4 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=match_sms_t4')
    t1 = "Pad Up Dosto! Bus Kuch Ghanto mein hai #{ home_team_name } Vs #{ away_team_name } Match, Join League aur Jeeto Rs9000 tak. fantoss par Team Banao Abhi #{ url1 }"
    t2 = "#{ home_team_name } Vs #{ away_team_name }. Aaj Mauka Hai Rs23,500 tak JEETNE ka. Bus Kuch Ghante Baki! fantoss par apni Team Banao Abhi. #{ url2 }"
    t3 = "Bus Kuch Ghante Baki! #{ home_team_name } Vs #{ away_team_name } Match shuru hone wala hai. Mauka Hai Rs21,500 tak JEETNE ka. fantoss par apni Team Banao Abhi. #{ url3 }"
    t4 = "#{ home_team_name } Vs #{ away_team_name } Match is about to start. Join fantoss league. Hurry! League freezes an hour before the match. Play Now #{ url4 }"

    [t1, t2, t3, t4]
  end

  def self.series_sms(options = {})
    series_name = options[:series_name]
    return [] unless series_name
    url = options[:url]
    url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=series_sms_t1')
    url2 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=series_sms_t2')
    url3 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=series_sms_t3')
    url4 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=series_sms_t4')
    t1 = "BANO SELECTOR, JEETO SERIES! fantoss par aa gayi hai #{ series_name } KHELO JEETO KAMAO. Abhi Team Banao. #{ url1 }"
    t2 = "Kya Aap Jante hai ki fantoss par aa gayi #{ series_name }? Ab honge jeetne ke zyadaa mauke. Abhi Team Banao. KHELO JEETO KAMAO #{ url2 }"
    t3 = "fantoss par aa gayi hai #{ series_name }. KHELO JEETO KAMAO. Match Shuru hone wale hain. Abhi Team Banao. #{ url3 }"
    t4 = "Ab Aap Khel Sakte hai #{ series_name }, fantoss par. KHELO JEETO KAMAO. Match Shuru hone wale hain. Abhi Team Banao. #{ url4 }"

    [t1, t2, t3, t4]
  end

  def self.generic_sms(options = {})
    url = options[:url]
    url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=generic_sms_t1')
    url2 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=generic_sms_t2')
    url3 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=generic_sms_t3')
    url4 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=generic_sms_t4')
    url5 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=generic_sms_t5')
    url6 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=generic_sms_t6')
    url7 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=generic_sms_t7')
    t1 = "Bano MAN of the MATCH fantoss pe AMIT ne ab tak jeete hai Rs27500. Ab aapki Bari. Matches Shuru hone walae hain. Abhi Team Banao. Click #{ url1 }"
    t2 = "Iss Cricket Season fantoss pe AMIR ne ab tak jeete hai Rs27500. Ab aapki Bari. Matches Shuru hone walae hain. Abhi Team Banao. Click #{ url2 }"
    t3 = "Last Week fantoss par 650 Players ne Jeete Rs.4.27 lakhs. Ab aapki Bari. Matches Shuru hone walae hain. Abhi Team Banao. Click #{ url3 }"
    t4 = "Thank You. fantoss ne last week diye 1500 players ko Rs. 6.5 lakhs. Khushiya Sab ke liye. KHELO JEETO KAMAO. #{ url4 }"
    t5 = "Iss Cricket Season fantoss pe AMIR ne ab tak jeete hai Rs27500. Ab aapki Bari. Matches Shuru hone walae hain. Abhi Team Banao. Click #{ url5 }"
    t6 = "Last Week fantoss par 2305 Players ne Jeete Rs.5.27 lakhs. Ab aapki Bari. Matches Shuru hone walae hain. Abhi Team Banao. Click #{ url6 }"
    t7 = "Thank You fantoss ne last week diye 1500 players ko Rs. 6.5 lakhs. Khushiya Sab ke liye. KHELO JEETO KAMAO. #{ url7 }"

    [t1, t2, t3, t4, t5, t6, t7]
  end

  def self.education_sms(options = {})
    event_name = options[:event_name]
    url = options[:url]
    url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=education_sms_t1')
    url2 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=education_sms_t2')
    url3 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=education_sms_t3')
    url4 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=education_sms_t4')
    t1 = "CRICKET SERIES UPDATE - #{ event_name } Shuru hone wali hai! Abhi pata kare kon kon se bade khiladi khel rahe hai isse. #{ url1 }"
    t2 = "Akhir Kyu hai  #{ event_name }, Itna bada Cricket Tournament! Abhi pata kare kon kon se bade khiladi khel rahe hai isse. #{ url2 }"
    t3 = "Aa gayi ODI ke Sabse Badi League- #{ event_name }! Abhi pata kare kon kon se bade khiladi khel rahe hai isse. Click Kare #{ url3 }"
    t4 = "Kya Aap Jante hai #{ event_name }, pataa kare kyu hai yeh itna badaa tournament aur konse khildai khel rahe hai ismein. #{ url4 }"

    [t1, t2, t3, t4]
  end

  def self.prompt_to_play_sms(options = {})
    url = options[:url]
    url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=prompt_to_play_sms_t1')
    url2 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=prompt_to_play_sms_t2')
    url3 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=prompt_to_play_sms_t3')

    t1 = "fantoss Par Khelna hai bahut asaan, Sirf Choose karo 5 batsmen aur Jeeto Hazaro. Match Shuru hone wale hai. KHELO JEETO KAMAO, PLAY NOW. #{ url1 }"
    t2 = "fantoss pe Jeetne ka Mauka hai Zyada, Yahan har 2 mein se 1 Khiladi hota hai Vijeta. Der kis baat ki KHELO JEETO KAMAO. Abhi Team Banao #{ url2 }"
    t3 = "Thanks for registering on fantoss. Winning is now made easy. Simply choose 5 batsmen for the next match & Win Big. Play Now #{ url3 }"

    [t1, t2, t3]
  end

  def self.refer_and_earn_sms(options = {})
    refer_win_money = options[:refer_win_money]
    url = options[:url]
    url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=refer_and_earn_sms_t1')

    t1 = "Apka HAR Dost aapko dila sakta Rs#{ refer_win_money } fantoss par. Aaj Matches shuru hone wale hai. Abhi apne dosto ko invite karke aur team banaye #{ url1 }"
    [t1]
  end

  def self.referal_credit_expiry_sms(options = {})
    referal_credit_expiry = options[:referal_credit_expiry]
    url = options[:url]
    url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=referal_credit_expiry_sms_t1')

    t1 = "Kya Apney Dost sey miley fantoss credits use kiye? unhe expire hone sey pehley league banaye & jeetay asli cash. #{ url1 }"
    [t1]
  end

  def self.notify_referal_credit_expiry
    [10, 14].each do |x|
      # due_days = 15 - x
      users = User.joins(:wallet).where('wallets.id in (?) ', (ReferralCredit.non_expired.where('created_at <= ?', (DateTime.current - x.days)).pluck(:wallet_id)))
      users.each do |user|
        unless user.leagues.paid.present?
          sms = case x
            when 10
              url1 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=referral_reminder_1')
              "fantoss ne diye hai aapko Rs.#{user.wallet.referral_balance}, Match khelne ke liye. Iss Cash ko expire hone se pehle Match mein lagao. KHELO JEETO KAMAO #{ url1 }"
            when 14
              url2 = UrlShortnerService.shorten('https://fantoss.com?utm_source=backend_sms&utm_medium=referral_reminder_2')
              "fantoss Cash Expiry Alert ! Apke account ke Rs#{user.wallet.referral_balance}, kal expire hone wale hai. Isse Jaldi Match mein lagao. KHELO JEETO KAMAO #{ url2 }"
            else
              next
            end
          send_notification(DateTime.current, Array.wrap(user.id), sms )
        end
      end
    end

  end


  def self.send_admin_match_data
    admin_data_result = stats

    total_inplay = admin_data_result[:total_inplay]
    inplay_today = admin_data_result[:inplay_today]
    wallet_today = admin_data_result[:wallet_today]

    user_ids = [5910,2071,6506]
    text = "Date : #{DateTime.current} \nTotal Inplay : #{total_inplay} \nToday's Inplay : #{inplay_today} \nWallet Balance : #{wallet_today}"

    send_notification(DateTime.current, user_ids, text)

  end

  def self.stats
    m_ids = []
    Match.where('id > 1220').each do |m|
     m_ids << m.id if m.user_transactions.pluck(:transaction_category).uniq.count ==  1
    end
    total_inplay = UserTransaction.completed.where(match_id: m_ids, transaction_category: 'join_league').sum(:total_amount)
    matches_today = Match.where(:start_time => (DateTime.current.beginning_of_day)..(DateTime.current.end_of_day)).pluck('matches.id')
    inplay_today = UserTransaction.completed.where(match_id: matches_today, transaction_category: 'join_league').sum(:total_amount)
    wallet_today = Wallet.pluck(:main_balance).select { |c| c > 0 }.inject(&:+)

    { total_inplay: total_inplay, inplay_today: inplay_today, wallet_today: wallet_today }
  end

  def self.send_notification(schedule_time, recipient_ids, text )
    recipient_ids.each do |user_id|
      user = User.find_by(id: user_id)

      NotificationWorker.perform_at(
        schedule_time,
        {
          recipient_id: user_id,
          content_plain: text,
          channels: [:sms],
          mailer_options: {},
          sms_type: 'SMSPROMOPREM'
        }
      )
    end
  end
end
