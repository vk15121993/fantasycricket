class Msg91SmsService
  include HTTParty

  ROUTES = {
    transactional: '4',
    promotional: '1'
  }
  AUTHKEY = '213697ALGgLORw5aeb492d'

  def self.send(options = {})
    HTTParty.get(
      "http://api.msg91.com/api/sendhttp.php",
      query: {
        'sender' => 'FNTOSS',
        'route' => ROUTES[:transactional],
        'country' => '91',
        'mobiles' => options[:to],
        'message' => options[:message],
        'authkey' => AUTHKEY
      }
    )
  end

  def self.send_otp(options = {})
    response = HTTParty.post(
      'http://control.msg91.com/api/sendotp.php',
      query: {
        'authkey' => AUTHKEY,
        'sender' => 'FNTOSS',
        'mobile' => options[:to],
        'message' => 'Your verification code/OTP for http://FanTOSS.com is: ##OTP##'
      }
    )

    json_response = JSON.parse(response.parsed_response)
    json_response['type'] == 'success'
  end

  def self.verify_otp(options = {})
    response = HTTParty.post(
      'https://control.msg91.com/api/verifyRequestOTP.php',
      query: {
        'authkey' => AUTHKEY,
        'mobile' => options[:to],
        'otp' => options[:otp]
      }
    )

    json_response = JSON.parse(response.parsed_response)
    json_response['type'] == 'success'
  end
end
