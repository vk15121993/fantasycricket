class FacebookService

  attr_reader :graph

  def initialize(user)
    token = user.try(:authentications).try(:facebook).try(:token)
    begin
      @graph = Koala::Facebook::API.new(token) if token.present?
    rescue
      @graph = []
    end
  end

  def invitable_friends
    return [] if graph.blank?
    begin
      graph.get_connections("me", "invitable_friends")
    rescue
      []
    end
  end
end
