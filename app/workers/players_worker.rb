class PlayersWorker

  include Sidekiq::Worker

  def perform(id)
    player = ::Player.find(id)
    player_data = ScoreAPI.getPlayer(player.api_player_id)["query"]["results"]
    if player_data
      player_data = player_data["PlayerProfile"]
      carrer_detail = get_carrer_detail(player_data["CareerDetails"]["MatchStats"], player_data["CareerDetails"]["Role"])

      # unless player.photo_url.present?
      #   player.photo_url = player_data["PersonalDetails"]["PlayerLargeImgName"]
      # end
      # unless player.remote_image_url.present?
      #   player.remote_image_url = player_data["PersonalDetails"]["PlayerThumbImgName"]
      # end

      player.update({
        player_category: player_data["CareerDetails"]["Role"].downcase.gsub(' ' , '_'),
        date_of_birth: player_data["PersonalDetails"]["DateOfBirth"],
        odi_career: carrer_detail["ODI"],
        ipl_career: carrer_detail["IPL"],
        t20_career: carrer_detail["T20"],
        test_career: carrer_detail["Test"],
      })
      # player.update(:remote_image_url => player_data["PersonalDetails"]["PlayerThumbImgName"]) if (player.image.file.nil?)
    end

    # remote_image_url: player_data["PersonalDetails"]["PlayerLargeImgName"],
    # getPlayer api changed
    # change keys and add new fields
    # dump data to player new store
  end

  private
    def get_carrer_detail(data, role)
      details = Hash.new { |hash, key| hash[key] = {} }
      Array.wrap(data).each do |cd|
        details[cd["mtype"]] = {
          "Batting" => cd["Batting"].to_a.select { |d| d["value"] == "all" }.first,
          "Bowling" => cd["Bowling"].to_a.select { |d| d["value"] == "all" }.first,
          "Fielding" => cd["Fielding"].to_a.select { |d| d["value"] == "all" }.first,
        }
      end
      details
    end
end
