
class GenericWorker
  include Sidekiq::Worker

  def perform(options)
    options = HashWithIndifferentAccess.new options

    _class = options[:class_name].constantize
    method_name = options[:method_name]
    class_method = options[:class_method]
    instance_arguments = options[:instance_arguments]
    method_arguments = options[:method_arguments]

    if class_method
      _class.send(method_name, *[method_arguments])
    else
      _class.new(*instance_arguments).send(method_name, *[method_arguments])
    end

  end
end
