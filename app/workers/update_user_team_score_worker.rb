class UpdateUserTeamScoreWorker
  include Sidekiq::Worker

  def perform(match_id)
    match = ::Match.find(match_id)

    match.user_teams.each do |ut|
      ut.update_score_from_players_score
    end
  end
end
