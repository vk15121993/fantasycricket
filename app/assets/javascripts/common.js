$(function(){
	resize();

  var _slider = "";

  function make_slider(){
    if (_slider==""){
      _slider = $('.guide-slider').bxSlider();
    }
  }

  function destroy_slider(){
    if (_slider!=""){
      _slider.destroySlider();
    }
    _slider = "";
  }

  function howToPlayPopup(){
    $(".how-to-play-link").on('click', function(){
        $('body').addClass("how-to-play-active");
        $("#points_rules").hide();
        $('#refer_earn').hide();
        $('.guide-slider').show();
        $(window).trigger("lookup");
        make_slider();
      })
    $('.close-popup').click(function(){
      $('body').removeClass("how-to-play-active");
    })
  }
  howToPlayPopup();

  $(".points-link").on('click', function(){
      destroy_slider();
      $('body').addClass("how-to-play-active");
      $("#points_rules").show();
      $('.guide-slider').hide();
      $('#refer_earn').hide();
      $(window).trigger("lookup");
  });

  $(".refer_earn_btn").on('click', function(){
      destroy_slider();
      $("#points_rules").hide();
      $('.guide-slider').hide();
      $(window).trigger("lookup");
      $("#refer_earn").show();
      $('body').addClass("how-to-play-active");
  });



  function playWithFriends(){

    $(".p-tab").on('click', function(event){
      event.preventDefault();
      $(this).addClass("is-active").siblings().removeClass('is-active');
      var thisVal = $(this).data('play') - 1;
      var selAmount = $(".r-tab.is-active").data('rs');
      var winAmount = thisVal * selAmount / 10;
      var prizeAmount = $(".r-tab.is-active").data('prize');
      $("#earn-rs").html(winAmount);
      $("#prize-rs").html(prizeAmount);
      $("#winners-rs").html($(this).data('play')/2);
    })

    $(".r-tab").on('click', function(event){
      event.preventDefault();
      $(this).addClass("is-active").siblings().removeClass('is-active');
      var thisVal = $(this).data('rs');
      var prizeAmount = $(".r-tab.is-active").data('prize');
      var selAmount = $(".p-tab.is-active").data('play') - 1;
      var winAmount = thisVal * selAmount / 10;
      $("#earn-rs").html(winAmount);
      $("#prize-rs").html(prizeAmount);
      $("#winners-rs").html($(".p-tab.is-active").data('play')/2);
    })
  }
  playWithFriends();

})

//align footer at bottom
function resize() {
	var window_height = $(window).height();
	var header_height = $(".mdl-layout__header-row").height();
	$(".fantoss-body-content").css('min-height', window_height - 158);
}


$(window).resize(function() {
	resize();
})


