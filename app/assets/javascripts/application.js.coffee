#= require jquery_2
#= require jquery_ujs
# require turbolinks
# require jquery.turbolinks
# require turbolinks-compatibility

#= require material.js

#= require jquery.countdown.min
#= require jquery.bxslider.js
#= require common
#= require imagesloaded
#= require facebook

# require jquery.fitvids.js
# require google_analytics
# require humane
# require user/user_team_form
# require user/invite_friends
# require nprogress
# require nprogress-turbolinks
# require socket

#= require mixpanel

run_timer = ->
  $('[data-countdown]').each ->
    $this = $(this)
    finalDate = $(this).data('countdown')
    starting = $(this).data('starting')
    $this.countdown finalDate, (event) ->
      $this.html 'CLOSES IN ' + event.strftime('<span class="days"><span class="d-c">%D</span> DAY</span> <span class="hrs"><span class="h-c">%H</span> Hours</span> <span class="mins"><span class="m-c">%M</span> Minutes</span> <span class="sec"><span class="s-c">%S</span> Seconds</span>')
      if parseInt(event.strftime('%D')) < 1
        $(this).find('.days').hide()
      if parseInt(event.strftime('%H')) < 1
        if starting==true
          $this.html 'Match starts in '+ event.strftime('<span class="hrs"><span class="h-c">%H</span> Hours</span> <span class="mins"><span class="m-c">%M</span> Minutes</span> <span class="sec"><span class="s-c">%S</span> Seconds</span>')
        $(this).find('.hrs').hide()
      if parseInt(event.strftime('%M')) < 1
        $(this).find('.mins').hide()
      return
    return

right_drawer = ->
  $('#chat_right').click ->
    if $('.mdl-layout__drawer-right').hasClass('active')
      $('.mdl-layout__drawer-right').removeClass 'active'
    else
      $('.mdl-layout__drawer-right').addClass 'active'
    return
  $('.mdl-layout__obfuscator-right').click ->
    if $('.mdl-layout__drawer-right').hasClass('active')
      $('.mdl-layout__drawer-right').removeClass 'active'
    else
      $('.mdl-layout__drawer-right').addClass 'active'
    return


track_events = ->
  generate_callback = (a) ->
    ->
      if a.attr('href')
        if a.attr('target') == '_blank'
          window.open(a.attr('href'))
          return
        else
          window.location = a.attr('href')
      return
  $('a').click event, ->
    if $(this).attr('data-method') == 'put'
      return
    if $(this).attr('data-remote') == 'true'
      return
    if $(this).attr('data-no-turbolink')
      cb = generate_callback($(this))
      event.preventDefault()
    if $(this).attr('data-mixpanel')
      _href = $(this).attr('href')
      _text = $(this).text().trim()
      if _text.length == 0
        _text = 'Home'
      console.log("tracking " + _text + " href: " +  _href )
      if mixpanel
        mixpanel.track ( $(this).attr('data-mixpanel') ), { 'title': _text, 'href': _href }, cb
    if $(this).attr('data-no-turbolink')
      setTimeout cb, 500
    return

zopim_chat = ->
  $('[__jx__id], embed#__zopnetworkswf').remove()
  window.$zopim = null
  ((d, s) ->
    z =
    $zopim = (c) ->
      z._.push c
      return

    $ = z.s = d.createElement(s)
    e = d.body.getElementsByTagName(s)[0]

    z.set = (o) ->
      z.set._.push o
      return

    z._ = []
    z.set._ = []
    $.async = !0
    $.setAttribute 'charset', 'utf-8'
    $.src = '//v2.zopim.com/?3liVXoEZbDfgTfrrK7DTjlkEIJO3tvui'
    z.t = +new Date
    $.type = 'text/javascript'
    e.parentNode.insertBefore $, e
    return
  ) document, 'script'
  return

share_with_friends = ->
  $('.fb-share-btn').click ->
    url = $(this).data("url")
    FB.ui({method: 'send',link: url});
    return

popupCenter = (url, width, height, name) ->
  left = screen.width / 2 - (width / 2)
  top = screen.height / 2 - (height / 2)
  window.open url, name, 'menubar=no,toolbar=no,status=no,width=' + width + ',height=' + height + ',toolbar=no,left=' + left + ',top=' + top


inIframe = ->
  try
    return window.self != window.top
  catch e
    return true
  return



login_popup = ->
  $('a.play-with-fb').click (e) ->
    console.log(document.referrer )
    console.log(inIframe() )
    if (inIframe())
      url = $(this).attr('href') + '&display=popup'
      popupCenter url, $(this).attr('data-width'), $(this).attr('data-height'), 'authPopup'
      e.stopPropagation()
      false


loader_show = ->
  $('#pageLoader').show();

loader_hide = ->
  $('#pageLoader').hide();


$(document).on 'ready turbolinks:load', ->
  componentHandler.upgradeDom();
  run_timer();
  share_with_friends();
  track_events();
  login_popup();
  console.log('all_loaded');
  $("img").unveil();

$(document).on 'turbolinks:request-start', ->
  loader_show();
  return
$(document).on 'turbolinks:load', ->
  ga('send', 'pageview', window.location.pathname);
  loader_hide();
  return



