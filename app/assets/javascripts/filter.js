var Filter = function (selectors) {
  this.dropdown = $(selectors.dropdown);
  this.table = $(selectors.table);
  this.tableRows = this.table.find('tbody tr');
};

Filter.prototype.init = function () {
  this.bindEvents();
};

Filter.prototype.bindEvents = function () {
  var _this = this;

  this.dropdown.on('change', function() {
    _this.tableRows.show();
    if (this.value) {
      _this.tableRows.not('[data-filter-value=' + this.value + ']').hide();
    }
  });
};

$(function () {
  var selectors = {
    dropdown: '[data-behaviour=filter]',
    table: '[data-container=table]',
  };
  (new Filter(selectors)).init();
});
