var InviteFriends = function (form) {
  this.form = form;
  this.requestIdField = this.form.find("input[name='request_id']");
  this.requestToField = this.form.find("input[name='request_to']");
};

InviteFriends.prototype.init = function () {
  this.initializeFB();
  this.bindEvents();
};

InviteFriends.prototype.bindEvents = function () {
  var friend_ids = [],
      _this = this;
  this.form.on('submit', function (e) {
    e.preventDefault();
    friend_ids = $.map($("input[name='invitable_friend[]']:checked"), function(obj) {
      return obj.value
    })
    _this.sendRequest(friend_ids.join(','));
  });
};

InviteFriends.prototype.sendRequest = function (friend_ids) {
  var _this = this;
  FB.ui({
    method: 'apprequests',
    message: 'Welcome to Fangully.',
    to: friend_ids,
  }, function(response) {
    if (response && !response.error_message) {
      _this.requestIdField.prop('value', response.request);
      _this.requestToField.prop('value', response.to);
      _this.form.unbind('submit').submit();
    } else {
      alert('Error while posting.');
    }
  });
};

InviteFriends.prototype.initializeFB = function () {
  FB.init({
    appId:  this.form.data('appId'),
    xfbml:  true, cookie: true, status: true,
  });
};

$(function () {
  (new InviteFriends($('#invitable_friend_form'))).init();
});
