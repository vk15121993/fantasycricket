function UserTeamSelectionManager(options) {
  this.$form = $(options.formSelector);
  this.$batsmenPlayersContainer = this.$form.find(options.batsmenPlayersContainerSelector);
  this.$bowlersPlayersContainer = this.$form.find(options.bowlersPlayersContainerSelector);
  this.$allroundersPlayersContainer = this.$form.find(options.allroundersPlayersContainerSelector);
  this.$wicketkeepersPlayersContainer = this.$form.find(options.wicketkeepersPlayersContainerSelector);
  this.$captainPlayersContainer = this.$form.find(options.captainPlayersContainerSelector);
  this.$viceCaptainPlayersContainer = this.$form.find(options.viceCaptainPlayersContainerSelector);
  this.$tabPanes = this.$form.find(options.tabPanesSelector);
  this.$batsmenNavPill = this.$form.find(options.batsmenNavPillSelector);
  this.$bowlersNavPill = this.$form.find(options.bowlersNavPillSelector);
  this.$allroundersNavPill = this.$form.find(options.allroundersNavPillSelector);
  this.$wicketkeepersNavPill = this.$form.find(options.wicketkeepersNavPillSelector);
  this.$captainNavPill = this.$form.find(options.captainNavPillSelector);
  this.$viceCaptainNavPill = this.$form.find(options.viceCaptainNavPillSelector);
  this.$playersCheckboxes = this.$form.find(options.playersCheckboxesSelector);
  this.playersCheckboxesSelector = options.playersCheckboxesSelector;
  this.captainPlayersCheckboxesSelector = options.captainPlayersCheckboxesSelector;
  this.viceCaptainPlayersCheckboxesSelector = options.viceCaptainPlayersCheckboxesSelector;
  this.$batsmenCounterDisplay = this.$form.find(options.batsmenCounterDisplaySelector);
  this.$bowlersCounterDisplay = this.$form.find(options.bowlersCounterDisplaySelector);
  this.$allroundersCounterDisplay = this.$form.find(options.allroundersCounterDisplaySelector);
  this.$wicketkeepersCounterDisplay = this.$form.find(options.wicketkeepersCounterDisplaySelector);
  this.$captainCounterDisplay = this.$form.find(options.captainCounterDisplaySelector);
  this.$viceCaptainCounterDisplay = this.$form.find(options.viceCaptainCounterDisplaySelector);
  this.maxBatsmenCount = 4;
  this.maxBowlersCount = 4;
  this.maxAllroundersCount = 2;
  this.maxWicketkeepersCount = 1;
  this.maxCaptainCount = 1;
  this.maxViceCaptainCount = 1;
  this.$errorContainer = this.$form.find(options.errorContainerSelector);
  this.$captainBatsmenContainer = this.$captainPlayersContainer.find(options.captainBatsmenContainerSelector);
  this.$captainBowlersContainer = this.$captainPlayersContainer.find(options.captainBowlersContainerSelector);
  this.$captainAllroundersContainer = this.$captainPlayersContainer.find(options.captainAllroundersContainerSelector);
  this.$captainWicketkeepersContainer = this.$captainPlayersContainer.find(options.captainWicketkeepersContainerSelector);
  this.$viceCaptainBatsmenContainer = this.$viceCaptainPlayersContainer.find(options.viceCaptainBatsmenContainerSelector);
  this.$viceCaptainBowlersContainer = this.$viceCaptainPlayersContainer.find(options.viceCaptainBowlersContainerSelector);
  this.$viceCaptainAllroundersContainer = this.$viceCaptainPlayersContainer.find(options.viceCaptainAllroundersContainerSelector);
  this.$viceCaptainWicketkeepersContainer = this.$viceCaptainPlayersContainer.find(options.viceCaptainWicketkeepersContainerSelector);
}

var _proto_ = UserTeamSelectionManager.prototype

_proto_.init = function() {
  this.bindEvents();
}

_proto_.bindEvents = function() {
  this.bindPlayersCheckboxes();
  this.bindCaptainPlayersCheckboxes();
  this.bindViceCaptainPlayersCheckboxes();
  this.bindPlayersTab();
  this.bindForm();
}

_proto_.bindPlayersTab = function() {
  var _this = this;
  this.$batsmenNavPill.on('click', function(e) {
    _this.$tabPanes.hide();
    _this.$batsmenPlayersContainer.show();
  });
  this.$bowlersNavPill.on('click', function(e) {
    _this.$tabPanes.hide();
    _this.$bowlersPlayersContainer.show();
  });
  this.$allroundersNavPill.on('click', function(e) {
    _this.$tabPanes.hide();
    _this.$allroundersPlayersContainer.show();
  });
  this.$wicketkeepersNavPill.on('click', function(e) {
    _this.$tabPanes.hide();
    _this.$wicketkeepersPlayersContainer.show();
  });
  this.$captainNavPill.on('click', function(e) {
    _this.$tabPanes.hide();
    _this.$captainPlayersContainer.show();
  });
  this.$viceCaptainNavPill.on('click', function(e) {
    _this.$tabPanes.hide();
    _this.$viceCaptainPlayersContainer.show();
  });
}

_proto_.bindForm = function() {
  var _this = this;

  this.$form.on('submit', function(e) {
    if (_this.batsmenCount() < _this.maxBatsmenCount) {
      _this.populateErrorContainer("Select " + _this.maxBatsmenCount + " batsmen!");
      e.preventDefault();
      return false;
    }
    if (_this.bowlersCount() < _this.maxBowlersCount) {
      _this.populateErrorContainer("Select " + _this.maxBowlersCount + " bowlers!");
      e.preventDefault();
      return false;
    }
    if (_this.allroundersCount() < _this.maxAllroundersCount) {
      _this.populateErrorContainer("Select " + _this.maxAllroundersCount + " allrounders!");
      e.preventDefault();
      return false;
    }
    if (_this.wicketkeepersCount() < _this.maxWicketkeepersCount) {
      _this.populateErrorContainer("Select " + _this.maxWicketkeepersCount + " wicketkeepers!");
      e.preventDefault();
      return false;
    }
    if (_this.captainCount() < _this.maxCaptainCount) {
      _this.populateErrorContainer("Select " + _this.maxCaptainCount + " captain!");
      e.preventDefault();
      return false;
    }
    if (_this.viceCaptainCount() < _this.maxViceCaptainCount) {
      _this.populateErrorContainer("Select " + _this.maxViceCaptainCount + " vice-captain!");
      e.preventDefault();
      return false;
    }

    _this.hideErrorContainer();
  });
}

_proto_.bindPlayersCheckboxes = function() {
  var _this = this;

  this.$playersCheckboxes.on('change', function(e) {
    var $this = $(this);
    var $samePlayersInOtherTabs = _this.$form.find("input[data-player-id='" + $this.val() + "']").not($this);

    if (_this.batsmenCount() > _this.maxBatsmenCount) {
      _this.populateErrorContainer("Maximum batsmen selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('list-group-item-primary');
      return;
    }
    if (_this.bowlersCount() > _this.maxBowlersCount) {
      _this.populateErrorContainer("Maximum bowlers selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('list-group-item-primary');
      return;
    }
    if (_this.allroundersCount() > _this.maxAllroundersCount) {
      _this.populateErrorContainer("Maximum allrounders selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('list-group-item-primary');
      return;
    }
    if (_this.wicketkeepersCount() > _this.maxWicketkeepersCount) {
      _this.populateErrorContainer("Maximum wicketkeepers selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('list-group-item-primary');
      return;
    }

    if ($this.is(':checked')) {
      $this.closest('li').addClass('list-group-item-primary');
      $samePlayersInOtherTabs.prop('disabled', true);
      $samePlayersInOtherTabs.prop('readonly', true);
    } else {
      $this.closest('li').removeClass('list-group-item-primary');
      $samePlayersInOtherTabs.prop('disabled', false);
      $samePlayersInOtherTabs.prop('readonly', false);
    }

    _this.updateCaptainTab();
    _this.updateViceCaptainTab();
    _this.hideErrorContainer();
    _this.updatePlayersCount();

  });
}

_proto_.updatePlayersCount = function() {
  this.$batsmenCounterDisplay.text(this.batsmenCount());
  this.$bowlersCounterDisplay.text(this.bowlersCount());
  this.$allroundersCounterDisplay.text(this.allroundersCount());
  this.$wicketkeepersCounterDisplay.text(this.wicketkeepersCount());
  this.$captainCounterDisplay.text(this.captainCount());
  this.$viceCaptainCounterDisplay.text(this.viceCaptainCount());
}

_proto_.bindCaptainPlayersCheckboxes = function() {
  var _this = this;

  this.$form.on('change', this.captainPlayersCheckboxesSelector, function(e) {
    var $this = $(this);
    var $samePlayersInViceCaptainTab = _this.$viceCaptainPlayersContainer.find("input[data-player-id='" + $this.val() + "']");

    if (_this.captainCount() > _this.maxCaptainCount) {
      _this.populateErrorContainer("Maximum captain selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('captain');
      return;
    }
    if (_this.viceCaptainCount() > _this.maxViceCaptainCount) {
      _this.populateErrorContainer("Maximum vice-captain selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('captain');
      return;
    }

    if ($this.is(':checked')) {
      $this.closest('li').addClass('captain')
      $samePlayersInViceCaptainTab.closest('li').addClass('captain');
      $samePlayersInViceCaptainTab.prop('disabled', true);
      $samePlayersInViceCaptainTab.prop('readonly', true);
    } else {
      $this.closest('li').removeClass('captain');
      $samePlayersInViceCaptainTab.closest('li').removeClass('captain');
      $samePlayersInViceCaptainTab.prop('disabled', false);
      $samePlayersInViceCaptainTab.prop('readonly', false);
    }

    _this.hideErrorContainer();
    _this.$captainCounterDisplay.text(_this.captainCount());
    _this.$viceCaptainCounterDisplay.text(_this.viceCaptainCount());
  });
}

_proto_.bindViceCaptainPlayersCheckboxes = function() {
  var _this = this;

  this.$form.on('change', this.viceCaptainPlayersCheckboxesSelector, function(e) {
    var $this = $(this);
    var $samePlayersInCaptainTab = _this.$captainPlayersContainer.find("input[data-player-id='" + $this.val() + "']");

    if (_this.captainCount() > _this.maxCaptainCount) {
      _this.populateErrorContainer("Maximum captain selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('vice-captain');
      return;
    }
    if (_this.viceCaptainCount() > _this.maxViceCaptainCount) {
      _this.populateErrorContainer("Maximum vice-captain selected!");
      $this.prop('checked', false);
      $this.closest('li').removeClass('vice-captain');
      return;
    }

    if ($this.is(':checked')) {
      $this.closest('li').addClass('vice-captain');
      $samePlayersInCaptainTab.closest('li').addClass('vice-captain');
      $samePlayersInCaptainTab.prop('disabled', true);
      $samePlayersInCaptainTab.prop('readonly', true);
    } else {
      $this.closest('li').removeClass('vice-captain');
      $samePlayersInCaptainTab.closest('li').removeClass('vice-captain');
      $samePlayersInCaptainTab.prop('disabled', false);
      $samePlayersInCaptainTab.prop('readonly', false);
    }


    _this.hideErrorContainer();
    _this.$captainCounterDisplay.text(_this.captainCount());
    _this.$viceCaptainCounterDisplay.text(_this.viceCaptainCount());
  });
}

_proto_.updateCaptainTab = function() {
  var $selectedBatsmen = this.$form.find("input[data-player-type='batsmen']:checked");

  this.$captainBatsmenContainer.empty();
  this.$captainBatsmenContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Batsmen</strong></div></li>'));

  for (var i = 0; i < $selectedBatsmen.length; i++) {

    var $batsmanCheckbox = $($selectedBatsmen[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[captain_id]');
    $playerCheckbox.prop('id', 'user_team_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'captain');
    $playerCheckbox.attr('data-player-list-container', 'captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$captainBatsmenContainer.append($playerList);
  }

  var $selectedBowlers = this.$form.find("input[data-player-type='bowlers']:checked");

  this.$captainBowlersContainer.empty();
  this.$captainBowlersContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Bowlers</strong></div></li>'));

  for (var i = 0; i < $selectedBowlers.length; i++) {

    var $batsmanCheckbox = $($selectedBowlers[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[captain_id]');
    $playerCheckbox.prop('id', 'user_team_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'captain');
    $playerCheckbox.attr('data-player-list-container', 'captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$captainBowlersContainer.append($playerList);
  }

  var $selectedAllrounders = this.$form.find("input[data-player-type='allrounders']:checked");

  this.$captainAllroundersContainer.empty();
  this.$captainAllroundersContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Allrounders</strong></div></li>'));

  for (var i = 0; i < $selectedAllrounders.length; i++) {

    var $batsmanCheckbox = $($selectedAllrounders[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[captain_id]');
    $playerCheckbox.prop('id', 'user_team_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'captain');
    $playerCheckbox.attr('data-player-list-container', 'captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$captainAllroundersContainer.append($playerList);
  }

  var $selectedWicketkeepers = this.$form.find("input[data-player-type='wicketkeepers']:checked");

  this.$captainWicketkeepersContainer.empty();
  this.$captainWicketkeepersContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Wicketkeepers</strong></div></li>'));

  for (var i = 0; i < $selectedWicketkeepers.length; i++) {

    var $batsmanCheckbox = $($selectedWicketkeepers[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[captain_id]');
    $playerCheckbox.prop('id', 'user_team_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'captain');
    $playerCheckbox.attr('data-player-list-container', 'captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$captainWicketkeepersContainer.append($playerList);
  }
}

_proto_.updateViceCaptainTab = function() {
  var $selectedBatsmen = this.$form.find("input[data-player-type='batsmen']:checked");

  this.$viceCaptainBatsmenContainer.empty();
  this.$viceCaptainBatsmenContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Batsmen</strong></div></li>'));

  for (var i = 0; i < $selectedBatsmen.length; i++) {

    var $batsmanCheckbox = $($selectedBatsmen[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[vice_captain_id]');
    $playerCheckbox.prop('id', 'user_team_vice_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'vice-captain');
    $playerCheckbox.attr('data-player-list-container', 'vice-captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$viceCaptainBatsmenContainer.append($playerList);
  }

  var $selectedBowlers = this.$form.find("input[data-player-type='bowlers']:checked");

  this.$viceCaptainBowlersContainer.empty();
  this.$viceCaptainBowlersContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Bowlers</strong></div></li>'));

  for (var i = 0; i < $selectedBowlers.length; i++) {

    var $batsmanCheckbox = $($selectedBowlers[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[vice_captain_id]');
    $playerCheckbox.prop('id', 'user_team_vice_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'vice-captain');
    $playerCheckbox.attr('data-player-list-container', 'vice-captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$viceCaptainBowlersContainer.append($playerList);
  }

  var $selectedAllrounders = this.$form.find("input[data-player-type='allrounders']:checked");

  this.$viceCaptainAllroundersContainer.empty();
  this.$viceCaptainAllroundersContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Allrounders</strong></div></li>'));

  for (var i = 0; i < $selectedAllrounders.length; i++) {

    var $batsmanCheckbox = $($selectedAllrounders[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[vice_captain_id]');
    $playerCheckbox.prop('id', 'user_team_vice_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'vice-captain');
    $playerCheckbox.attr('data-player-list-container', 'vice-captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$viceCaptainAllroundersContainer.append($playerList);
  }

  var $selectedWicketkeepers = this.$form.find("input[data-player-type='wicketkeepers']:checked");

  this.$viceCaptainWicketkeepersContainer.empty();
  this.$viceCaptainWicketkeepersContainer.append($('<li class="list-group-item list-group-item-dark"><div class="list-as-heading"><strong>Wicketkeepers</strong></div></li>'));

  for (var i = 0; i < $selectedWicketkeepers.length; i++) {

    var $batsmanCheckbox = $($selectedWicketkeepers[i]);
    var $playerList = $('<li class="list-group-item text-secondary">');
    var $playerListLabel = $('<label>');
    var $playerName = $('<span>');
    var $playerCheckbox = $batsmanCheckbox.clone();

    $playerCheckbox.prop('checked', false);
    $playerCheckbox.prop('name', 'user_team[vice_captain_id]');
    $playerCheckbox.prop('id', 'user_team_vice_captain_id_' + $playerCheckbox.val());
    $playerCheckbox.attr('data-player-type', 'vice-captain');
    $playerCheckbox.attr('data-player-list-container', 'vice-captain');
    $playerListLabel.append($playerCheckbox);
    $playerName.text($playerCheckbox.data().playerName);
    $playerListLabel.append($playerName);
    $playerList.append($playerListLabel);

    this.$viceCaptainWicketkeepersContainer.append($playerList);
  }
}

_proto_.populateErrorContainer = function(error) {
  this.$errorContainer.text(error);
  this.$errorContainer.removeClass('d-none');
  window.scrollTo(0,0);
}

_proto_.hideErrorContainer = function() {
  this.$errorContainer.text('');
  this.$errorContainer.addClass('d-none');
}


_proto_.batsmenCount = function() {
  return this.$form.find("input[data-player-type='batsmen']:checked").length;
}

_proto_.bowlersCount = function() {
  return this.$form.find("input[data-player-type='bowlers']:checked").length;
}

_proto_.allroundersCount = function() {
  return this.$form.find("input[data-player-type='allrounders']:checked").length;
}

_proto_.wicketkeepersCount = function() {
  return this.$form.find("input[data-player-type='wicketkeepers']:checked").length;
}

_proto_.captainCount = function() {
  return this.$form.find("input[data-player-type='captain']:checked").length;
}

_proto_.viceCaptainCount = function() {
  return this.$form.find("input[data-player-type='vice-captain']:checked").length;
}



$(function() {
  var options = {
    formSelector: '#user-team-form',
    batsmenPlayersContainerSelector: '#batsmen',
    bowlersPlayersContainerSelector: '#bowlers',
    allroundersPlayersContainerSelector: '#allrounders',
    wicketkeepersPlayersContainerSelector: '#wicketkeepers',
    captainPlayersContainerSelector: '#captain',
    viceCaptainPlayersContainerSelector: '#vice-captain',
    tabPanesSelector: '.tab-pane',
    batsmenNavPillSelector: '#batsmen-tab',
    bowlersNavPillSelector: '#bowlers-tab',
    allroundersNavPillSelector: '#allrounders-tab',
    wicketkeepersNavPillSelector: '#wicketkeepers-tab',
    captainNavPillSelector: '#captain-tab',
    viceCaptainNavPillSelector: '#vice-captain-tab',
    playersCheckboxesSelector: "input[data-player-list-container='player']",
    captainPlayersCheckboxesSelector: "input[data-player-list-container='captain']",
    viceCaptainPlayersCheckboxesSelector: "input[data-player-list-container='vice-captain']",
    batsmenCounterDisplaySelector: '#batsmen-selection-counter',
    bowlersCounterDisplaySelector: '#bowlers-selection-counter',
    allroundersCounterDisplaySelector: '#allrounders-selection-counter',
    wicketkeepersCounterDisplaySelector: '#wicketkeepers-selection-counter',
    captainCounterDisplaySelector: '#captain-selection-counter',
    viceCaptainCounterDisplaySelector: '#vice-captain-selection-counter',
    errorContainerSelector: '#error-container',
    captainBatsmenContainerSelector: '#captain-batsmen',
    captainBowlersContainerSelector: '#captain-bowlers',
    captainAllroundersContainerSelector: '#captain-allrounders',
    captainWicketkeepersContainerSelector: '#captain-wicketkeepers',
    viceCaptainBatsmenContainerSelector: '#vice-captain-batsmen',
    viceCaptainBowlersContainerSelector: '#vice-captain-bowlers',
    viceCaptainAllroundersContainerSelector: '#vice-captain-allrounders',
    viceCaptainWicketkeepersContainerSelector: '#vice-captain-wicketkeepers'
  };

  var userTeamSelectionManagerObject = new UserTeamSelectionManager(options);
  userTeamSelectionManagerObject.init();
  userTeamSelectionManagerObject.updateCaptainTab();
  userTeamSelectionManagerObject.updateViceCaptainTab();
  userTeamSelectionManagerObject.updatePlayersCount();
});
