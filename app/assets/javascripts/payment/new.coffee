//= require payment/base

class window.Payment.New extends window.Payment
  @$form: $('.paytm-form')

  @bindEvents: ->
    @$form.submit()

$ ->
  Payment.New.bindEvents()
