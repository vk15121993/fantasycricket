//= require payment/base

class window.Payment.Success extends window.Payment
  @transactionId: window.transactionId
  @$loader: $('.loader')

  @bindEvents: ->
    @_createDynamicLeague()

  @_createDynamicLeague: ->
    _this = @
    $.ajax
      url: '/league'
      type: 'POST'
      data:
        transaction_id: _this.transactionId
      beforeSend: ->
        _this.$loader.removeClass('hide')
      success: ->
        console.log('already added to league')


$ ->
  Payment.Success.bindEvents()
