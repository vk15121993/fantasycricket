# == Schema Information
#
# Table name: users_coupons
#
#  id                  :integer          not null, primary key
#  user_id             :integer
#  coupon_id           :integer
#  league_id           :integer
#  user_transaction_id :integer
#  state               :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  amount              :float
#

class UsersCoupon < ActiveRecord::Base

  # Constants
  STATES = ['APPLIED', 'CONSUMED']

  # Associations
  belongs_to :user
  belongs_to :coupon
  belongs_to :league
  belongs_to :user_transaction

  # Attr_accessors
  attr_accessor :league_entry_fee, :match_id, :league_type

  # Validations
  validates :user, :coupon, presence: true
  # validates :user, uniqueness: { scope: :coupon }
  validates :state, inclusion: { in: STATES }
  validate  :coupon_applicability

  # Scopes
  scope :applied, -> { where(state: 'APPLIED') }
  scope :consumed, -> { where(state: 'CONSUMED') }
  scope :paytm_consumed, -> { consumed.joins(:coupon).where(coupons: { concession_type: 'PAYTM_CASHBACK' }) }

  # Public Methods
  def consume!(league_id, user_transaction_id)
    transaction do
      league = League.find_by(id: league_id)
      self.league_entry_fee = league.entry_fee
      self.match_id = league.match_id

      update!(
        state: 'CONSUMED',
        league_id: league_id,
        league_type: league.league_type,
        user_transaction_id: user_transaction_id,
        amount: worth(league_entry_fee)
      )
      coupon.update!(current_redeem_count: coupon.current_redeem_count.to_i + 1)
    end
  end

  def value_for(league_entry_fee)
    if coupon.concession_type == 'PAYTM_CASHBACK'
      return 0
    else
      return worth(league_entry_fee)
    end
  end

  def worth(league_entry_fee)
    coupon_value = 0
    if coupon.discount_type == 'FLAT'
      coupon_value = coupon.discount_value.to_f
    elsif coupon.discount_type == 'PERCENTAGE'
      coupon_value = coupon.discount_value/100.0 * league_entry_fee.to_f
    end

    if coupon.maximum_discount_value.present? && ( coupon.maximum_discount_value.to_i < coupon_value )
      coupon_value = coupon.maximum_discount_value
    end
    return coupon_value
  end

  private

  def coupon_applicability
    if coupon.start_date.present?
      if Time.current < coupon.start_date
        errors.add(:base, 'Coupon is not live yet!')
        return
      end
    end

    if coupon.end_date.present?
      if Time.current > coupon.end_date
        errors.add(:base, 'Coupon has been expired!')
        return
      end
    end

    if coupon.maximum_redeem_count.present?
      if coupon.current_redeem_count.to_i >= coupon.maximum_redeem_count.to_i
        errors.add(:base, 'Coupon has been redeemed to its max!')
        return
      end
    end

    if coupon.per_user_count.present?
      if user.users_coupons.consumed.where(coupon: coupon).count >= coupon.per_user_count
        errors.add(:base, "Coupon can be redeemed to max #{ coupon.per_user_count } times per user!")
        return
      end
    end

    if coupon.user_validation_eval_string.present?
      unless coupon.applicable_user_ids.include?(user.id)
        errors.add(:base, 'Coupon not valid for this user!')
        return
      end
    end

    if coupon.league_validation_eval_string.present?
      unless coupon.applicable_league_prices.include?(league_entry_fee.to_s)
        errors.add(:base, 'Coupon not valid for league of this entry fee!')
        return
      end
    end

    if coupon.match_validation_eval_string.present?
      unless coupon.applicable_match_ids.include?(match_id)
        errors.add(:base, 'Coupon not valid for this match!')
        return
      end
    end

    if coupon.league_type.present?
      unless coupon.applicable_league_types.include?(league_type)
        errors.add(:base, 'Coupon not valid for this league!')
        return
      end
    end

    if coupon.tournament.present?
      unless coupon.tournament.id == Match.find_by(id: match_id).try(:tournament_id)
        errors.add(:base, 'Coupon not valid for this tournament!')
        return
      end
    end

    if coupon.match.present?
      unless coupon.match.id == match_id
        errors.add(:base, 'Coupon not valid for this match!')
        return
      end
    end
  end
end
