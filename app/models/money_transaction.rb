# == Schema Information
#
# Table name: money_transactions
#
#  id                                 :integer          not null, primary key
#  request_details                    :json
#  response_details                   :json
#  last_verification_request_details  :json
#  last_verification_response_details :json
#  provider                           :string
#  amount                             :string
#  status                             :string
#  user_id                            :integer
#  created_at                         :datetime         not null
#  updated_at                         :datetime         not null
#  money_transaction_type             :string
#  details                            :json
#

class MoneyTransaction < ActiveRecord::Base

  # Constants
  MONEY_TRANSACTIONS_TYPES = ['WITHDRAW_TO_BANK', 'PAYTM_CASHBACK']

  # Associations
  belongs_to :user

  # Validations
  validates :provider, :amount, presence: true
  validates :money_transaction_type, inclusion: { in: MONEY_TRANSACTIONS_TYPES }
  validates :created_at, uniqueness: { scope: :user_id }

  # Callbacks
  after_save :update_user_wallet

  private

  def update_user_wallet
    if money_transaction_type == 'WITHDRAW_TO_BANK'
      if response_details && response_details_changed? && response_details_was == nil
        if status.blank?
          user.user_transactions.create!(
            params: { purpose: "NIL transaction status received from PAYTM; CHECK from PayTM dashboard: Money_Transaction_ID: #{ id }" },
            state: "completed",
            transaction_category: "withdraw_to_bank",
            credit_amount: 0,
            debit_amount: amount
          )
        end
        if status == 'SUCCESS' || status == 'PENDING'
          user.user_transactions.create!(
            params: { purpose: "Withdrawn to PayTM: Money_Transaction_ID: #{ id }" },
            state: "completed",
            transaction_category: "withdraw_to_bank",
            credit_amount: 0,
            debit_amount: amount
          )
        end
      end
    end
  end
end
