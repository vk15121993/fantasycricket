module RankingAllocation
  extend ActiveSupport::Concern

  def process_rankings(nullify_league = false)
    if nullify_league
      user_teams.update_all(rank: 0)
    else
      ordered_user_teams = user_teams.order(totalscore: :desc)
      user_team_scores = ordered_user_teams.pluck(:totalscore)
      ordered_user_teams.each do |user_team, index|
        rank = user_team_scores.index(user_team.totalscore) + 1
        user_team.update_columns(rank: rank)
      end
    end
  end

end
