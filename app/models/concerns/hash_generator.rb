module Concerns::HashGenerator
  extend ActiveSupport::Concern

  def generate_for(object)
    SecureRandom.uuid
    # case [object.class]
    # when [User]
    #   generate_for_user(object)
    # when [UserTeam]
    #   generate_for_user_team(object)
    # else
    #   Time.generate_for_current(object)
    # end
  end

  private

  def generate_for_user(object)
    Digest::MD5.hexdigest("#{ object.phone }#{ object.id }#{ object.email }#{ object.created_at || Time.current }")
  end

  def generate_for_user_team(object)
    Digest::MD5.hexdigest("#{ object.id }#{ object.league_id }#{ object.user_id }#{ object.created_at || Time.current }")
  end

  def Time.generate_for_current(object)
    Digest::MD5.hexdigest("#{ object.id }#{ object.created_at }#{ Time.current }")
  end

end
