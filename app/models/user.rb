# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  first_name             :string
#  last_name              :string
#  image_url              :string
#  email                  :string           default("")
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  confirmation_token     :string
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime
#  updated_at             :datetime
#  is_admin               :boolean
#  username               :string
#  credits                :integer
#  phone                  :string           not null
#  date_of_birth          :date
#  state                  :string
#  country                :string           default("IN")
#  meta_info              :hstore
#  referred_by_id         :integer
#  referral_code          :string
#  is_spam_referrer       :boolean          default(FALSE)
#  authentication_token   :string           default("")
#  wallet_balance         :string           default("")
#

class User < ActiveRecord::Base
  include Concerns::UserImagesConcern
  include HTTParty

  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :omniauthable,
         :timeoutable, :async , authentication_keys: [:login], omniauth_providers: [:google_oauth2, :facebook]

  # Constants
  MINIMUM_WITHDRAWL_AMOUNT = 20
  MAXIMUM_WITHDRAWL_AMOUNT = 9999
  MAXIMUM_ADD_TO_WALLET_AMOUNT = 4999

  SUPER_ADMIN_IDS = User.where(phone: ['7503578995', '8802053264', '8447696819', '7065150226']).pluck(:id)

  REG_LOGIN_DATA = {
    "registration_screen" =>'reg',
    "login1" => 0,
    "login2" => 0,
    "login3" => 0
  }
  AFFILIATE_DETAILS = {
    # databuddy: {
    #   code_string: 'databuddy',
    #   pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # },
    # u2mobi: {
    #   code_string: 'u2mobi',
    #   signup_pingback_url: "http://u2mob.in/p.ashx?a=66&e=88",
    #   paid_conversion_pingback_url: "http://u2mob.in/p.ashx?a=66&e=89"
    # },
    # adgebra: {
    #   code_string: 'adgebra',
    #   # pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # },
    # facebook: {
    #   code_string: 'facebook',
    #   # pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # },
    # colombiaonline: {
    #   code_string: 'colombiaonline',
    #   # pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # },
    # juicy_ads: {
    #   code_string: 'juicy_ads',
    #   # pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # },
    # taboola: {
    #   code_string: 'taboola',
    #   # pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # },
    # outbrain: {
    #   code_string: 'outbrain',
    #   # pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # },
    # whitespace: {
    #   code_string: 'whitespace',
    #   # pingback_url: 'https://databuddy.co/databuddy/api/v1/apps/callback/fantoss'
    # }
  }
  AFFILIATE_CODE_STRINGS = {
    # databuddy: 'databuddy',
    # u2mobi: 'u2mobi',
    # adgebra: 'adgebra',
    # facebook: 'facebook',
    # colombiaonline: 'colombiaonline',
    # juicy_ads: 'juicy_ads',
    # taboola: 'taboola',
    # outbrain: 'outbrain',
    # whitespace: 'whitespace'
  }

  # F_UIDS = [34205, 43673, 37267, 33664, 4977, 3837156  , 23286, 47262, 38299, 28992, 19063, 33379, 18736,
  # 44232, 43240, 14296, 35509, 36193, 44978, 49444, 50324, 50536, 36348, 34172, 33211, 22575, 53235,
  # 41763, 45212, 52591, 46422, 42767, 53884, 53753, 52779, 53912, 26701, 53789, 25575, 34174, 53809]
  F_UIDS = []
  # Removed users - 37829

  ## Associations
  has_one :wallet, dependent: :destroy

  has_many :user_teams
  has_many :matches, -> { distinct }, through: :user_teams
  has_many :leagues, through: :user_teams
  has_many :freeleagues, -> { free }, through: :user_teams, source: :league
  has_many :paidleagues, -> { paid }, through: :user_teams, source: :league
  has_many :wonleagues, -> { where("user_teams.has_won = true") }, through: :user_teams, source: :league
  has_many :users_coupons
  has_many :coupons, through: :users_coupons
  has_many :wallet_snaps
  has_many :wallet_transactions

  has_many :authentications, dependent: :destroy, validate: false, inverse_of: :user do
    def grouped_with_oauth
      includes(:oauth_cache).group_by {|a| a.provider }
    end

    def facebook
      find_by(provider: 'facebook')
    end

    def google_oauth2
      find_by(provider: 'google_oauth2')
    end
  end

  has_many :money_transactions, dependent: :destroy
  has_many :user_transactions, dependent: :destroy
  has_many :banked_credits, dependent: :destroy do
    def redeem!
      self.each { |c| c.redeem! }
    end
  end
  belongs_to :referred_by, class_name: User
  has_many :referrals, foreign_key: :referred_by_id, class_name: User

  ## Validations
  validates :phone, presence: true
  validates :username, uniqueness: { allow_nil: true }
  validates :username, presence: true, on: :update
  validates :username, format: {
    with: /\A(?=.*[a-z])[a-z\d]+\Z/i,
    message: 'must be aplha-numeric and contain at least 1 letter',
    allow_blank: true
  }, on: :update
  validates :username, length: {
    maximum: 18,
    allow_blank: true
  }, on: :update
  validates :terms_of_service, acceptance: { accept: "on" } , on: :create, allow_nil: false
  validates :phone, length: { is: 10 }
  validates :phone, uniqueness: {message: "Phone number has already been taken."}
  validates :phone, format: { with: /\A[6789]\d{9}\z/, message: "Enter paytm account mobile number to receive prize cashback." }
  # validates :first_name, :last_name, :phone, :country, :date_of_birth, presence: true
  # validates :state, presence: true
  # validate :must_be_above_18, if: -> { date_of_birth.present? }
  # validate :otp, presence: true
  # validates :state, exclusion: { in: %w(Assam Sikkim Odisha Tamil\ Nadu Nagaland), message: "of %{value} users are not allowed to play." }

  store_accessor :meta_info, :clickMomentId
  attr_accessor :referal_reward_value, :otp, :verification_id, :params


  ## Callbacks
  # before_create :generate_referral_code
  # before_create :generate_token
  before_create :build_wallet

  after_create :set_username
  # after_create :send_welcome_notifications
  after_create :populate_meta_info
  after_create :award_referral_credit_to_self
  # after_create :manage_affiliate_details
  after_create :award_referral_credit_to_parent

  # Scopes
  scope :affiliate_base_users_for, ->(affiliate) { where(referral_code: User::AFFILIATE_DETAILS[affiliate.to_sym][:code_string]) }
  scope :affiliate_referred_users_for, ->(affiliate) { where(referral_code: "#{ User::AFFILIATE_DETAILS[affiliate.to_sym][:code_string] }_referral") }
  scope :affiliate_users_for, ->(affiliate) { where(referral_code: [User::AFFILIATE_DETAILS[affiliate.to_sym][:code_string], "#{ User::AFFILIATE_DETAILS[affiliate.to_sym][:code_string] }_referral"]) }


  # Notify Admin
  def self.notify_admin(options = {})
    admins = User.where(id: [])

    admins.each do |admin|
      NotificationWorker.new.perform(
        {
          recipient: admin,
          content_plain: '',
          channels: [:email],
          mailer_options: {
            mailer: 'UserMailer',
            mail_method: 'general_email',
            mail_params: [
              admin,
              {
                intro_text: options[:intro_details] ||'Fraud User',
                more_details: options[:more_details] || 'Check meta data',
                subject: options[:subject] || 'Fraud User | fantoss'
              }
            ]
          }
        }
      )
    end
  end

  def display_name
    username.presence || first_name.presence || phone.presence || email.split('@')[0]
  end

  def total_won
    wonleagues.sum(:prize_money)
  end

  def fb_id
    authentications.facebook.proid
  end

  def wallet_data
    "₹ #{wallet.total_balance} ( ₹ #{wallet.main_balance.to_i} / ₹ #{wallet.referral_balance.to_i} )"
  end

  # Case insensitive email lookup.
  #
  # See Devise.config.case_insensitive_keys.
  # Devise does not automatically downcase email lookups.
  def self.find_by_email(email)
    find_by(email: email.downcase)
    # Use ILIKE if using PostgreSQL and Devise.config.case_insensitive_keys=[]
    #where('email ILIKE ?', email).first
  end

  # Override Devise to allow for Authentication or password.
  #
  # An invalid authentication is allowed for a new record since the record
  # needs to first be saved before the authentication.user_id can be set.
  def password_required?
    if authentications.empty?
      super || encrypted_password.blank?
    elsif new_record?
      false
    else
      super || encrypted_password.blank? && authentications.find{|a| a.valid?}.nil?
    end
  end

  # Merge attributes from Authentication if User attribute is blank.
  #
  # If User has fields that do not match the Authentication field name,
  # modify this method as needed.
  def reverse_merge_attributes_from_auth(auth)
    auth.oauth_data.each do |k, v|
      self[k] = v if self.respond_to?("#{k}=") && self[k].blank?
    end
  end

  # Do not require email confirmation to login or perform actions
  def confirmation_required?
    false
  end

  def send_welcome_notifications
    # UserMailer.delay.welcome_email(self.id) if self.email.present?

    NotificationWorker.new.perform(
      {
        recipient: self,
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/welcome_sms.txt',
          locals: { '@user': self }
        ),
        mailer_options: {
          mailer: 'UserMailer',
          mail_method: 'welcome_email',
          mail_params: [self.id]
        }
      }
    )
  end

  def full_name
    "#{first_name} #{last_name}".titleize.squish
  end

  def avatar_image
    image_url.presence || "robo_avatar/#{phone[-1]}.png"
  end

  def create_uid
    if referral_code.blank?
      generate_referral_code
      self.save
    end
    return self.referral_code
  end

  #Device's Method
  def password_required?
    !persisted? || !password.blank? || !password_confirmation.blank?
  end

  def is_eligible_for_promo(m)
    m.id==232 && credits.to_f==25
  end

  def exhaust_promo_credits(m)
    if m.id==232
      self.credits = 0
      self.save
    end
  end

  def award_referral_credit_to_referrer(referral_credit)
    if referred_by
      referred_by.user_transactions.create(
        params: { purpose: "Referral bonus credit for user: #{ username } #{ phone }" },
        state: "completed",
        transaction_category: "referral",
        credit_amount: referral_credit.to_i,
        debit_amount: 0
      )
      notify_about_first_league_to_referrer(referral_credit)
      notify_about_next_league_coupon
    end
  end

  def transfer_credits_to_wallet
    pending_credit = banked_credits.unredeemed.sum(:value)
    if !pending_credit.zero?
      user_transactions.create(
        params: { purpose: "Transfer old referrals to wallet_balance; V1 upgrade" },
        state: "completed",
        transaction_category: "add_to_wallet",
        credit_amount: pending_credit,
        debit_amount: 0
      )
      banked_credits.update_all(redeemed_on: Time.current)
    end
  end

  AFFILIATE_DETAILS.each do |affiliate, details|
    # +referral_code+ stores the type of referral system this user used while making account
    # Check whether a user is a affiliate base promotional user
    define_method "is_a_#{ affiliate }_base_user?" do
      referral_code == details[:code_string]
    end

    # Check whether a user is a affiliate referred user
    define_method "is_a_#{ affiliate }_referred_user?" do
      referral_code == "#{ details[:code_string] }_referral"
    end

    # Check whether a user is a affiliate tree user
    define_method "is_a_#{ affiliate }_user?" do
      send("is_a_#{ affiliate }_base_user?") || send("is_a_#{ affiliate }_referred_user?")
    end
  end

  def notify_about_reference_money_parent
    NotificationWorker.new.perform(
      {
        recipient: User.find_by(id: self.referred_by_id),
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/notify_about_reference_money_parent.txt',
          locals: { '@user': self }
        ),
        channels: [:sms],
        mailer_options: {}
      }
    )
  end

  def notify_about_referral_credit_to_self
    NotificationWorker.new.perform(
      {
        recipient: self,
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/notify_about_referral_credit_to_self.txt',
          locals: { '@user': self }
        ),
        channels: [:sms],
        mailer_options: {}
      }
    )
  end

  def notify_about_first_league_to_referrer(referral_credit)
    NotificationWorker.new.perform(
      {
        recipient: User.find_by(id: self.referred_by_id),
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/notify_about_first_league_to_referrer.txt',
          locals: { '@user': self, '@referral_credit': referral_credit }
        ),
        channels: [:sms],
        mailer_options: {}
      }
    )
  end

  def  notify_about_reference_money
    NotificationWorker.new.perform(
      {
        recipient: User.find_by(id: self.referred_by_id),
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/notify_about_reference_money.txt',
          locals: { '@user': self }
        ),
        channels: [:sms],
        mailer_options: {}
      }
    )
  end


  # def send_paytm_cashback(league_entry_fee)
  #   if referral_code == User::AFFILIATE_DETAILS[:paytm][:code_string] && created_at < Date.new(2017, 6, 18)
  #     MoneyTransferService.transfer_for(
  #       id,
  #       {
  #         amount: AFFILIATE_DETAILS[:paytm][:referral_amount].to_f / 100.0 * league_entry_fee.to_f,
  #         money_transaction_type: 'PAYTM_CASHBACK',
  #         metadata: "fantoss | PayTM cashback for Signup. Play Now.",
  #         details: { reason: 'PayTM Signup bonus.' }
  #       }
  #     )
  #   end
  # end

  def notify_about_next_league_coupon
    NotificationWorker.new.perform(
      {
        recipient: self,
        content_plain: ActionController::Base.new.render_to_string(
          template: 'sms_templates/notify_about_next_league_coupon.txt',
          locals: { '@user': self }
        ),
        channels: [:sms],
        mailer_options: {}
      }
    )
  end

  def populate_meta_info
      update(registration_info: User::REG_LOGIN_DATA )
  end

  def update_login_counter(screen)
    registration_info[screen] =  registration_info[screen].to_i + 1
    save if registration_info[screen].present?
  end

  def update_registration_counter(screen)
    if registration_info['registration_screen'].present?
      registration_info['registration_screen'] =  screen
      registration_info[screen] =  registration_info[screen].to_i + 1
      save
    end
  end

 private

  def manage_affiliate_details
    send("send_#{ referral_code }_pingback") if referral_code.present?

    AFFILIATE_DETAILS.each do |affiliate, details|
      if referred_by && referred_by.send("is_a_#{ affiliate }_user?")
        update_column(:referral_code, "#{ details[:code_string] }_referral")
        break
      end
    end
  end

  def send_databuddy_pingback
    if params[:databuddykey].present?
      update_column(:meta_info, { databuddykey: params[:databuddykey] } )
      url = "#{ User::AFFILIATE_DETAILS[:databuddy][:pingback_url] }?click_id=#{ params[:databuddykey] }"
      response = HTTParty.post(url)
    end
  end

  def send_u2mobi_pingback
    if params[:u2mobikey].present?
      click_id = params[:u2mobikey]
      update_column(:meta_info, { u2mobikey: click_id } )
      url = "#{ User::AFFILIATE_DETAILS[:u2mobi][:signup_pingback_url] }&r=#{click_id}"
      response = HTTParty.post(url)
    end
  end

  def send_facebook_pingback
    # NULL STATEMENT
  end

  def send_adgebra_pingback
    # NULL STATEMENT
  end

  def send_colombiaonline_pingback
    # NULL STATEMENT
  end

  def send_juicy_ads_pingback
    # NULL STATEMENT
  end

  def send_taboola_pingback
    # NULL STATEMENT
  end

  def send_outbrain_pingback
    # NULL STATEMENT
  end

  def send_whitespace_pingback
    # NULL STATEMENT
  end

  def generate_referral_code
    # return self.referral_code = self.phone
    # Doing NOTHING
  end

  def award_referral_credit_to_self
    #credit_amount = referred_by ? ApplicationYML['referral_credit'] : ApplicationYML['welcome_bonus']
    #credit_amount =  Rails.application.config.settings.welcome_bonus
    # unless User::AFFILIATE_CODE_STRINGS.values.include?(referral_code)
    if referred_by
      user_transactions.create(
        params: { purpose: "Welcome bonus | referred_by_id : #{ referred_by_id }" },
        state: "completed",
        transaction_category: "referral",
        credit_amount: Rails.application.config.settings.welcome_bonus,
        debit_amount: 0
      )
      # notify_about_referral_credit_to_self
    end
  end

  def award_referral_credit_to_parent
    if referred_by
      referred_by.user_transactions.create(
        params: { purpose: "Referral bonus credit for user: #{ username } #{ phone }" },
        state: "completed",
        transaction_category: "referral",
        credit_amount: Rails.application.config.settings.parent_referral_credit,
        debit_amount: 0
      )
      # notify_about_reference_money_parent
    end
  end

  def must_be_above_18
    if date_of_birth >= 18.years.ago.to_date
      errors.add(:date_of_birth, 'must be above 18 years.')
    end
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.phone || self.email
  end


  protected

  def generate_token
    self.authentication_token = loop do
      random_token = SecureRandom.urlsafe_base64(nil, false)
      break random_token unless User.exists?(authentication_token: random_token)
    end
  end

  def set_username
    update_column(:username, POPULAR_PLAYERS.sample + "#{ id }")
  end

end
