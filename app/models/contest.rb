# == Schema Information
#
# Table name: contests
#
#  id             :integer          not null, primary key
#  questions_hash :json
#  title          :string
#  created_at     :datetime
#  updated_at     :datetime
#  team_type      :string
#

class Contest < ActiveRecord::Base

  TEAM_TYPE = {
    top_11_players: "Top 11 Players",
    top_5_batsmen: "Top 5 Batsmen"
  }

  # TOP_11_CONTEST = find_by(team_type: 'top_11_players')
  TOP_5_CONTEST  = find_by(team_type: 'top_5_batsmen')

  has_many :user_teams
  has_many :leagues
  validates :title, :team_type, presence: true
  validates :title, :team_type, uniqueness: true

  # Instance Methods
  def top_11_players?
    team_type == "top_11_players"
  end

  def top_5_batsmen?
    team_type == "top_5_batsmen"
  end
end
