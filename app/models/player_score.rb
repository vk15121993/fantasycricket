# == Schema Information
#
# Table name: player_scores
#
#  id              :integer          not null, primary key
#  match_id        :integer
#  player_id       :integer
#  bat_runs_scored :integer          default(0)
#  bat_balls       :integer
#  bat_fours       :integer          default(0)
#  bat_sixes       :integer          default(0)
#  bat_sr          :decimal(, )
#  bowl_overs      :integer
#  bowl_maidens    :integer          default(0)
#  bowl_runs       :integer
#  bowl_wickets    :integer          default(0)
#  bowl_wides      :integer          default(0)
#  bowl_noballs    :integer          default(0)
#  bowl_er         :decimal(, )
#  field_catches   :integer
#  field_stumpings :integer
#  field_runouts   :integer
#  created_at      :datetime
#  updated_at      :datetime
#  bat_score       :integer          default(0)
#  bowl_score      :integer          default(0)
#  field_score     :integer          default(0)
#  total           :integer          default(0)
#  bat_status      :string
#  bat_singles     :integer          default(0)
#  bat_duck        :integer          default(0)
#  bat_hundreds    :integer          default(0)
#  bat_fifties     :integer          default(0)
#  inning_no       :integer          default(1)
#

class PlayerScore < ActiveRecord::Base

  belongs_to :player
  belongs_to :match
  # has_and_belongs_to_many :user_teams
  # validates :name, presence: true

  before_save :update_player_scores


private
  def update_player_scores
    set_bat_score
    set_bowl_score
    # self.total = bowl_score + bat_score + field_score
    self.total = bat_score + bowl_score
  end

  def set_bat_score
    bat_points = Rails.application.config.points.batting

    if (bat_runs_scored.zero? && (bat_balls.to_i > 0) && (bat_status != 'Batting') && (bat_status != 'not out'))
      self.bat_duck = 1
      self.bat_score = bat_points.out_for_duck.to_i
    else
      self.bat_duck = 0
      self.bat_singles = bat_runs_scored # - (bat_fours * 4 + bat_sixes * 6)
      self.bat_score = (( bat_singles * bat_points.single ) + ( bat_fours * bat_points.four ) + ( bat_sixes * bat_points.six ))

      self.bat_double_hundreds, remainders = bat_runs_scored.divmod(200)
      self.bat_hundreds, remainders = remainders.divmod(100)
      self.bat_fifties, remainders = remainders.divmod(50)

      self.bat_score += bat_double_hundreds * bat_points.double_century
      self.bat_score += bat_hundreds * bat_points.century
      self.bat_score += bat_fifties * bat_points.half_century
    end

  end

  def set_bowl_score
    bowl_points = Rails.application.config.points.bowling
    wickets_haul_points = 0

    if bowl_wickets.to_i == 4
      wickets_haul_points = bowl_points.four_wicket_haul
    elsif bowl_wickets.to_i == 5
      wickets_haul_points = bowl_points.five_wicket_haul
    end

    self.bowl_score = (
      bowl_wickets * bowl_points.wicket_taken +
      bowl_maidens * bowl_points.maiden_over +
      wickets_haul_points
    )
  end

end




# d > a > t > BATTING
#   {
# i: "3478",
# a: "3",
# c: "not out",
# b: "48",
# r: "14",
# sr: "29.1666667",
# six: "0",
# four: "0"
# },
# i - player_api_id, b - bowls, r - runs scored, sr - strike rate, six - six, four - four

# d > o > t > Bowling
#   {
# i: "4235",
# a: "3",
# o: "4",
# mo: "0",
# r: "11",
# w: "0",
# nb: "0",
# wd: "0",
# sr: "2.75"
# },
# i - player_api_id, o - over, mo - maiden over, r - runs given, w - wickets, nb: noball, wd: wise ball, sr - bowling strike rate
# Fielding - no data mapped yet

