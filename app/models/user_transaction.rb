# == Schema Information
#
# Table name: user_transactions
#
#  id                       :integer          not null, primary key
#  params                   :hstore
#  payment_gateway_response :hstore
#  user_id                  :integer
#  token                    :string
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#  league_id                :integer
#  user_team_id             :integer
#  match_id                 :integer
#  state                    :string           default("created")
#  transaction_category     :string           default("")
#  total_amount             :integer          default(0)
#  balance_amount           :integer          default(0)
#

class UserTransaction < ActiveRecord::Base
  PAYMENT_GATEWAYS = { paytm: 1, mobikwik: 2 }
  TRANSACTION_TYPE = %w(join_league add_to_wallet withdraw_to_bank match_win referral referral_expiry refund tie_refund deduction coupon_refund)

  belongs_to :user
  belongs_to :league
  belongs_to :user_team
  belongs_to :match
  has_many   :banked_credits, as: :referring_to , dependent: :destroy
  has_many   :users_coupons
  has_many   :coupons, through: :users_coupons
  has_many   :wallet_transactions

  scope :completed, -> { where(state: 'completed') }
  scope :winnings, -> { where(transaction_category: 'match_win') }
  scope :ties, -> { where(transaction_category: 'tie_refund') }
  scope :wallet_in, -> { where(transaction_category: 'add_to_wallet') }
  scope :wallet_out, -> { where(transaction_category: 'withdraw_to_bank') }
  scope :referral, -> { where(transaction_category: 'referral') }
  scope :join_league, -> { where(transaction_category: 'join_league') }
  scope :refund, -> { where(transaction_category: 'refund') }
  scope :deduction, -> { where(transaction_category: 'deduction') }
  scope :c_refund, -> { where(transaction_category: 'coupon_refund') }

  validates :user, :user_id, presence: true
  validates :transaction_category, inclusion: { in: TRANSACTION_TYPE, allow_blank: true }
  validate  :single_user_transaction_every_x_time, on: :create
  validate  :single_txn_category_per_user, on: :create
  validate  :last_user_transanction_wallet_transaction_creation, on: :create

  after_save :notify_affiliator

  def last_user_transanction_wallet_transaction_creation
    last_user_transanction = user.user_transactions.last

    if last_user_transanction && last_user_transanction.wallet_transactions.count < 0
      errors.add(:base, "Invalid state! Please talk to our customer care.")
    end
  end

  def single_user_transaction_every_x_time
    last_user_transanction = user.user_transactions.last
    if !(['coupon_refund', 'match_win', 'tie_refund', 'referral', 'referral_expiry'].include? transaction_category )
      if last_user_transanction && last_user_transanction.try(:created_at) && last_user_transanction.created_at > (Time.current - 10.seconds)
        errors.add(:base, "That's way too fast!!!")
      end
    else
      user.wallet.reload
    end
  end

  def single_txn_category_per_user
    if ['match_win', 'tie_refund', 'join_league'].include?(transaction_category) && self.class.where(user_team: user_team, transaction_category: transaction_category).count >= 1
      errors.add(:base, "Cannot have multiple user_transactions for #{transaction_category} for single user_team")
    end
  end

  before_create :populate_token
  after_save :update_banked_credits
  # after_save :populate_league_match_and_user_team

  # Alias Methods
  alias_attribute :credit_amount, :balance_amount
  alias_attribute :debit_amount, :total_amount

  def notify_admin_about_failed_payment
    AdminMailer.notify_admin_about_failed_payment(self).deliver_now
  end

  def initiated?
    state=="initiated"
  end

  def failed?
    state=="failed"
  end

  def completed?
    state=="completed"
  end

  def set_state(state='initiated')
    update(state: state)
  end

  def notify_admin_about_successful_payment
    AdminMailer.notify_admin_about_successful_payment(self).deliver_now
  end


  def notify_affiliator
    if (transaction_category == 'add_to_wallet') && (state == 'completed')
      return if user.user_transactions.where(transaction_category: 'add_to_wallet', state: 'completed').count > 1
      if( (user.meta_info['u2mobikey'].present?) rescue false)
        click_id = user.meta_info['u2mobikey']
        url = "#{ User::AFFILIATE_DETAILS[:u2mobi][:paid_conversion_pingback_url] }&r=#{click_id}"
        response = HTTParty.post(url)
      end
    end
  end

  # def notify_affiliator
  #   if (transaction_category == 'add_to_wallet') && (state == 'completed')
  #     return if user.user_transactions.where(transaction_category: 'add_to_wallet'), state: 'completed').count >= 1
  #     if user.meta_info.present?
  #       click_id = user.meta_info.keys.first
  #       update_column(:meta_info, { u2mobikey: click_id } )
  #       url = "#{ User::AFFILIATE_DETAILS[:u2mobi][:paid_conversion_pingback_url] }&r=#{click_id}"
  #     end
  #   end
  # end

  private

  def populate_token
    self.token = loop do
      random_token = rand(36**8).to_s(36)
      break random_token unless UserTransaction.exists?(token: random_token)
    end
  end

  def update_banked_credits
    if state_changed? && state == 'completed'
      # debugger
      if credit_amount  > 0
        banked_credits.create!(
          value: credit_amount,
          reason: transaction_category,
          user_id: user_id,
          transaction_type: 'credit'
        )
      end
      if debit_amount  > 0
        # Added transaction Category 'referral expiry in transaction category'in referral.
        if (transaction_category == 'join_league' && user.wallet.reload.total_balance >= debit_amount) ||
          (transaction_category != 'join_league' && user.wallet.reload.main_balance >= debit_amount) ||
          (transaction_category == 'referral_expiry')
          banked_credits.create!(
            value: debit_amount,
            reason: transaction_category,
            user_id: user_id,
            transaction_type: 'debit'
          )
        else
          raise 'Insufficient balance'
        end
      end

      # For FREE Leagues
      if credit_amount == 0 && debit_amount == 0
        if user_team
          # user_team.update_column(:league_id, self.league.id)
          user_team.update!(league: league)
          League.reset_counters(league.id, :user_teams)
        end
      end

    end
  end

end
