module LoadAdminConcern

  private

    def authenticate_admin!
      usrs_ids = User.where(phone: ['7503578995']).pluck(:id)
      redirect_to '/', alert: 'Not authorized.' unless current_user && (current_user.is_admin? || usrs_ids.include?(current_user.id))
    end

end
