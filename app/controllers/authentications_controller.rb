# == Schema Information
#
# Table name: authentications
#
#  id            :integer          not null, primary key
#  user_id       :integer
#  provider      :string           not null
#  proid         :string           not null
#  token         :string
#  refresh_token :string
#  secret        :string
#  expires_at    :datetime
#  username      :string
#  image_url     :string
#  created_at    :datetime
#  updated_at    :datetime
#

class AuthenticationsController < ApplicationController
  load_and_authorize_resource :user
  load_and_authorize_resource only: :destroy

  def index
    @failed = params[:failed]
    @return_to = user_authentications_path
    @authentications = @user.authentications.grouped_with_oauth
  end

  def destroy
    @authentication.destroy
    redirect_to user_authentications_path current_user
  end
end
