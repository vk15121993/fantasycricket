# All Administrate controllers inherit from this `Admin::ApplicationController`,
# making it the ideal place to put authentication logic or other
# before_filters.
#
# If you want to add pagination or other controller-level concerns,
# you're free to overwrite the RESTful controller actions.
module Admin
  class ApplicationController < Administrate::ApplicationController
    include LoadAdminConcern

    before_filter :authenticate_admin!
    before_filter :authenticate_super_admin!
    before_filter :default_params

    http_basic_authenticate_with name: "fm_admin", password: "fm_adam1n"


    def default_params
      params[:order] ||= "created_at"
      params[:direction] ||= "desc"
    end

    private

    def authenticate_super_admin!
      # Finance User: Narender ##### User.find_by(phone: '9813794466')
      # fin_user_id = 34792 is also admin now

      usrs_ids = User::SUPER_ADMIN_IDS

      super_admin_ids = usrs_ids
      if controller_name == 'players' || controller_name == 'user_transactions'
        if action_name == 'destroy'
          redirect_to admin_matches_paths
        else
          return true
        end

      elsif ['new', 'edit', 'create', 'update', 'destroy'].include?(action_name) && !super_admin_ids.include?(current_user.id)
        redirect_to admin_matches_path
      end
    end

  end

end
