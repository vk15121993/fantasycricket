module Admin
  class DataController < Admin::ApplicationController
    before_action :set_default_params, except: [:matches, :upcoming_series, :stats, :league_odd_even]
    before_action :set_date_params, only: [:league_odd_even]

    def stats
      @stats = NotificationSchedularService.stats
    end

    def money_transactions
      get_money_transactions_total_requests
      get_money_transactions_success_amount
      get_money_transactions_total_success_requests
      get_user_transactions_added_to_wallet

      days = (
        @money_transactions_total_requests.keys +
        @money_transactions_success_amount.keys +
        @money_transactions_total_success_requests.keys +
        @user_transactions_added_to_wallet.keys
      ).uniq

      @money_transactions = {}
      @total_requests = @success_requests = @success_amount = @credited_amount = 0

      days.each do |day|
        @money_transactions[day] = {
          success_amount: @money_transactions_success_amount[day] || 0,
          success_requests: @money_transactions_total_success_requests[day] || 0,
          total_requests: @money_transactions_total_requests[day] || 0,
          credited_amount: @user_transactions_added_to_wallet[day] || 0
        }
        @total_requests += @money_transactions_total_requests[day] || 0
        @success_requests += @money_transactions_total_success_requests[day] || 0
        @success_amount += @money_transactions_success_amount[day] || 0
        @credited_amount += @user_transactions_added_to_wallet[day] || 0
      end

      @total_records = days.count.to_f

    end

    def new_users
      @new_total_users = append_general_conditions_for(User.all).distinct.count
      @new_unreferred_users = append_general_conditions_for(User.where(referred_by: nil)).distinct.count
      @new_users_who_played_any_league = append_general_conditions_for(User.joins(:leagues)).distinct.count
      @new_users_who_played_paid_league = append_general_conditions_for(User.joins(:leagues).where('leagues.prize_money > 0')).distinct.count
      @new_unreferred_users_who_played_any_league = append_general_conditions_for(User.joins(:leagues).where(referred_by: nil)).distinct.count
      @new_unreferred_users_who_played_paid_league = append_general_conditions_for(User.joins(:leagues).where(referred_by: nil).where('leagues.prize_money > 0')).distinct.count

      @banked_credits = append_general_conditions_for(
        BankedCredit.select('DATE(created_at)').
          where(reason: 'referral', transaction_type: 'credit')
      ).sum(:value)

      days = (
        @new_total_users.keys + @new_unreferred_users.keys + @banked_credits.keys +
        @new_users_who_played_any_league.keys + @new_users_who_played_paid_league.keys
      ).uniq

      @new_users_data = {}
      @unreferred_users = @referred_users = @total_users = @referred_money_credited = 0
      @users_with_any_league = @users_with_paid_league = @users_with_only_free_league = @users_with_no_league = 0
      @unreferred_users_with_any_league = @unreferred_users_with_paid_league = @unreferred_users_with_only_free_league = @unreferred_users_with_no_league = 0
      @referred_users_with_any_league = @referred_users_with_paid_league = @referred_users_with_only_free_league = @referred_users_with_no_league = 0

      days.each do |day|
        users_with_any_league = @new_users_who_played_any_league[day] || 0
        users_with_paid_league = @new_users_who_played_paid_league[day] || 0
        users_with_only_free_league = (@new_users_who_played_any_league[day] || 0) - (@new_users_who_played_paid_league[day] || 0)
        users_with_no_league = (@new_total_users[day] || 0) - (@new_users_who_played_any_league[day] || 0)

        unreferred_users_with_any_league = @new_unreferred_users_who_played_any_league[day] || 0
        unreferred_users_with_paid_league = @new_unreferred_users_who_played_paid_league[day] || 0
        unreferred_users_with_only_free_league = (@new_unreferred_users_who_played_any_league[day] || 0) - (@new_unreferred_users_who_played_paid_league[day] || 0)
        unreferred_users_with_no_league = (@new_unreferred_users[day] || 0) - (@new_unreferred_users_who_played_any_league[day] || 0)

        referred_users_with_any_league = users_with_any_league - unreferred_users_with_any_league
        referred_users_with_paid_league = users_with_paid_league - unreferred_users_with_paid_league
        referred_users_with_only_free_league = users_with_only_free_league - unreferred_users_with_only_free_league
        referred_users_with_no_league = users_with_no_league - unreferred_users_with_no_league

        @new_users_data[day] = {
          unreferred_users: @new_unreferred_users[day] || 0,
          referred_users: (@new_total_users[day] || 0) - (@new_unreferred_users[day] || 0),
          total_users: @new_total_users[day] || 0,
          referred_money_credited: @banked_credits[day] || 0,
          users_with_any_league: users_with_any_league,
          users_with_paid_league: users_with_paid_league,
          users_with_only_free_league: users_with_only_free_league,
          users_with_no_league: users_with_no_league,
          unreferred_users_with_any_league: unreferred_users_with_any_league,
          unreferred_users_with_paid_league: unreferred_users_with_paid_league,
          unreferred_users_with_only_free_league: unreferred_users_with_only_free_league,
          unreferred_users_with_no_league: unreferred_users_with_no_league,
          referred_users_with_any_league: referred_users_with_any_league,
          referred_users_with_paid_league: referred_users_with_paid_league,
          referred_users_with_only_free_league: referred_users_with_only_free_league,
          referred_users_with_no_league: referred_users_with_no_league
        }
        @unreferred_users += @new_unreferred_users[day] || 0
        @referred_users += (@new_total_users[day] || 0) - (@new_unreferred_users[day] || 0)
        @total_users += @new_total_users[day] || 0
        @referred_money_credited += @banked_credits[day] || 0

        @users_with_any_league += users_with_any_league
        @users_with_paid_league += users_with_paid_league
        @users_with_only_free_league += users_with_only_free_league
        @users_with_no_league += users_with_no_league

        @unreferred_users_with_any_league += unreferred_users_with_any_league
        @unreferred_users_with_paid_league += unreferred_users_with_paid_league
        @unreferred_users_with_only_free_league += unreferred_users_with_only_free_league
        @unreferred_users_with_no_league += unreferred_users_with_no_league

        @referred_users_with_any_league += referred_users_with_any_league
        @referred_users_with_paid_league += referred_users_with_paid_league
        @referred_users_with_only_free_league += referred_users_with_only_free_league
        @referred_users_with_no_league += referred_users_with_no_league
      end

      @total_records = days.count.to_f
    end

    def leagues
      @all_leagues = append_general_conditions_for(League.all).count
      @leagues_count, @users_count = {}, {}

      # All Leagues
      # --------------------------
      leagues_of_all_entry_fees = append_general_conditions_for(League.all)
      @leagues_count["all"] = leagues_of_all_entry_fees.count

      object = append_general_conditions_for(
        User.joins(:leagues).where(
          leagues: { id: append_where_by_condition_for(League.all) }
        ),
        'leagues'
      )

      @users_count["all"] = object.select('users.id').distinct.count
      # --------------------------

      # Only Free Leagues
      # --------------------------
      leagues_of_free_entry_fees = append_general_conditions_for(League.free)
      @leagues_count["free"] = leagues_of_free_entry_fees.count

      object = append_general_conditions_for(
        User.joins(:leagues).where(
          leagues: { id: append_where_by_condition_for(League.free) }
        ),
        'leagues'
      )

      @users_count["free"] = object.select('users.id').distinct.count
      # --------------------------

      # Only Paid Leagues
      # --------------------------
      leagues_of_paid_entry_fees = append_general_conditions_for(League.paid)
      @leagues_count["paid"] = leagues_of_paid_entry_fees.count

      object = append_general_conditions_for(
        User.joins(:leagues).where(
          leagues: { id: append_where_by_condition_for(League.paid) }
        ),
        'leagues'
      )

      @users_count["paid"] = object.select('users.id').distinct.count
      # --------------------------

      # Categorial Leagues
      # --------------------------
      League::ENTRY_FEE_ENUM.each do |l_entry_fee|
        leagues_of_x_entry_fees = append_general_conditions_for(League.where(entry_fee: l_entry_fee))
        @leagues_count["#{l_entry_fee}"] = leagues_of_x_entry_fees.count

        object = append_general_conditions_for(
          User.joins(:leagues).where(
            leagues: {
              id: append_where_by_condition_for(League.where(entry_fee: l_entry_fee))
            }
          ),
          'leagues'
        )

        @users_count["#{l_entry_fee}"] = object.select('users.id').distinct.count
      end
      # --------------------------

      days = @all_leagues.keys.uniq

      @leagues_data = {}
      @total_leagues = 0
      @total_leagues_count, @total_users_count = Hash.new(0), Hash.new(0)

      days.each do |day|
        @leagues_data[day] = {
          total_leagues: @all_leagues[day] || 0,
          total_leagues_count: {},
          total_users_count: {}
        }
        @total_leagues += @all_leagues[day] || 0

        (League::ENTRY_FEE_ENUM + ['all', 'free', 'paid']).each do |l_entry_fee|
          @leagues_data[day][:total_leagues_count]["#{l_entry_fee}"] = @leagues_count["#{l_entry_fee}"][day] || 0
          @leagues_data[day][:total_users_count]["#{l_entry_fee}"] = @users_count["#{l_entry_fee}"][day] || 0
        end

      end

      League::ENTRY_FEE_ENUM.each do |l_entry_fee|
        object = append_where_by_condition_for(
          User.joins(:leagues).where(
            leagues: {
              id: League.where(entry_fee: l_entry_fee)
            }
          ),
          'leagues'
        )
        @total_leagues_count["#{l_entry_fee}"] = append_where_by_condition_for(League.where(entry_fee: l_entry_fee)).count
        @total_users_count["#{l_entry_fee}"] = object.select('users.id').distinct.count
      end

      ['all', 'free', 'paid'].each do |scope_type|
        object = append_where_by_condition_for(
          User.joins(:leagues).where(
            leagues: {
              id: League.send(scope_type)
            }
          ),
          'leagues'
        )
        @total_leagues_count["#{scope_type}"] = append_where_by_condition_for(League.send(scope_type)).count
        @total_users_count["#{scope_type}"] = object.select('users.id').distinct.count
      end

      @total_records = days.count.to_f

    end

    def matches
      params[:start_date] = params[:start_date].presence || (Time.current.to_date).to_s
      params[:end_date] = params[:end_date].presence || (Time.current.to_date + 1.day).to_s
      params[:order_by] = params[:order_by].presence || 'ASC'

      @matches = append_order_by_condition_for(
        Match.where('start_time > ?', Date.strptime(params[:start_date], "%Y-%m-%d").in_time_zone('Mumbai')).
          where('start_time < ?', Date.strptime(params[:end_date], "%Y-%m-%d").in_time_zone('Mumbai'))
      )

      @match_stats = {}
      @total_match_stats = {}
      @total_match_total_stats = Hash.new(0)
      @matches_count = @matches.count

      League::PAID_LEAGUE_ENUM.each do |l_entry_fee|
        @total_match_stats[l_entry_fee] = Hash.new(0)
      end

      @matches.each do |m|
        match_stats_hash = all_leagues_stats_for(m)
        match_total_stats = Hash.new(0)

        match_stats_hash.each do |l_entry_fee, match_stats_per_l_entry_fee|
          match_stats_per_l_entry_fee.each do |key, value|
            match_total_stats[key] += value
            @total_match_total_stats[key] += value
            @total_match_stats[l_entry_fee][key] += value
          end
        end

        @match_stats["#{ m.title }-#{ m.api_match_id }-#{ m.api_provider }-#{m.id} "] = {
          match_details: m,
          leagues_details: match_stats_hash,
          match_total_stats: match_total_stats
        }
      end

      if @match_stats.present?
        @secondary_headers = @match_stats.first[1][:leagues_details].first[1].keys
      end

    end

    def upcoming_series
      if params[:api_provider] == 'goalserve'
        @tournaments = CricketApi::Tournament.upcoming(api_provider: params[:api_provider] || :goalserve)
      elsif params[:api_provider] == 'scorespro'
        @tournaments = ScoresPro::Tournament.upcoming(api_provider: params[:api_provider] || :scorespro)
      end
    end

    def sync
      api_provider = params[:api_provider]
      api_series_ids = params[:api_series_ids]
      api_match_ids = params[:api_match_ids]
      # debugger

      if api_series_ids
        GenericWorker.perform_at(
          Time.current + 2.seconds,
          {
            class_name: 'CricketApi::Tournament',
            method_name: :sync,
            class_method: false,
            instance_arguments: {},
            method_arguments: {
              api_provider: params[:api_provider] || :goalserve,
              api_series_ids: (api_series_ids.present? ? [api_series_ids].flatten : []),
              api_match_ids: (api_match_ids.present? ? [api_match_ids].flatten : [])
            }
          }
        )
        # CricketApi::Tournament.new.sync(
        #   api_provider: params[:api_provider] || :goalserve,
        #   api_series_ids: [api_series_ids].flatten,
        #   api_match_ids: [api_match_ids].flatten
        # )
      end

      redirect_to upcoming_series_admin_data_path, notice: 'Matche(s) Synced Successfully'
    end

    def sync_squad
      GenericWorker.perform_at(
          Time.current + 2.seconds,
          {
            class_name: 'CricketApi::Squad',
            method_name: :sync,
            class_method: false,
            instance_arguments: {},
            method_arguments: {
              api_provider: params[:api_provider],
              file_path: params[:api_file_path],
              force_refresh: true
            }
          }
        )

      redirect_to upcoming_series_admin_data_path, notice: 'Squad(s) Synced Successfully'
    end

    def sync_scorespro_squad
      GenericWorker.perform_at(
        DateTime.current,
        {
          class_name: 'ScoresPro::Squad',
          method_name: :sync,
          class_method: true,
          instance_arguments: {},
          method_arguments: {
            api_provider: params[:api_provider],
            league_id: params[:league_id],
            force_refresh: true
          }
        }
      )
      redirect_to upcoming_series_admin_data_path(api_provider: 'scorespro'), notice: 'Squad(s) Synced Successfully'

    end

    def sync_match
      options = {}
      options[:series_id] = params['api_series_ids']
      options[:data] = params[:data]
      options[:api_provider] = 'scorespro'

      result = ScoresPro::Match.populate_match(options)

      # REPLACE ABOVE LINE WITH GENERIC WORKER

      unless result
        redirect_to upcoming_series_admin_data_path(api_provider: 'scorespro'), notice: 'Please Sync Squad first'
        return
      end
      redirect_to upcoming_series_admin_data_path(api_provider: 'scorespro') , notice: 'Match(s) Synced Successfully'
    end


    User::AFFILIATE_DETAILS.keys.each do |affiliate|
      define_method "#{ affiliate }_users" do
        @new_base_users = append_general_conditions_for(User.send(:affiliate_base_users_for, affiliate)).distinct.count
        @new_referred_users = append_general_conditions_for(User.send(:affiliate_referred_users_for, affiliate)).distinct.count

        @new_users_who_played_any_league = append_general_conditions_for(User.send(:affiliate_base_users_for, affiliate).joins(:leagues)).distinct.count
        @new_users_who_played_paid_league = append_general_conditions_for(User.send(:affiliate_base_users_for, affiliate).joins(:leagues).where('leagues.prize_money > 0')).distinct.count
        @new_referred_users_who_played_any_league = append_general_conditions_for(User.send(:affiliate_referred_users_for, affiliate).joins(:leagues).where(referred_by: nil)).distinct.count
        @new_referred_users_who_played_paid_league = append_general_conditions_for(User.send(:affiliate_referred_users_for, affiliate).joins(:leagues).where(referred_by: nil).where('leagues.prize_money > 0')).distinct.count

        days = (
          @new_base_users.keys + @new_referred_users.keys +
          @new_users_who_played_any_league.keys + @new_users_who_played_paid_league.keys
        ).uniq

        @new_users_data = {}
        @referred_users = @referred_users = @base_users = @referred_money_credited = 0
        @users_with_any_league = @users_with_paid_league = @users_with_only_free_league = @users_with_no_league = 0
        @referred_users_with_any_league = @referred_users_with_paid_league = @referred_users_with_only_free_league = @referred_users_with_no_league = 0

        days.each do |day|
          users_with_any_league = @new_users_who_played_any_league[day] || 0
          users_with_paid_league = @new_users_who_played_paid_league[day] || 0
          users_with_only_free_league = (@new_users_who_played_any_league[day] || 0) - (@new_users_who_played_paid_league[day] || 0)
          users_with_no_league = (@new_base_users[day] || 0) - (@new_users_who_played_any_league[day] || 0)

          referred_users_with_any_league = @new_referred_users_who_played_any_league[day] || 0
          referred_users_with_paid_league = @new_referred_users_who_played_paid_league[day] || 0
          referred_users_with_only_free_league = (@new_referred_users_who_played_any_league[day] || 0) - (@new_referred_users_who_played_paid_league[day] || 0)
          referred_users_with_no_league = (@new_referred_users[day] || 0) - (@new_referred_users_who_played_any_league[day] || 0)

          @new_users_data[day] = {
            referred_users: @new_referred_users[day] || 0,
            base_users: @new_base_users[day] || 0,
            users_with_any_league: users_with_any_league,
            users_with_paid_league: users_with_paid_league,
            users_with_only_free_league: users_with_only_free_league,
            users_with_no_league: users_with_no_league,
            referred_users_with_any_league: referred_users_with_any_league,
            referred_users_with_paid_league: referred_users_with_paid_league,
            referred_users_with_only_free_league: referred_users_with_only_free_league,
            referred_users_with_no_league: referred_users_with_no_league
          }
          @referred_users += @new_referred_users[day] || 0
          @base_users += @new_base_users[day] || 0

          @users_with_any_league += users_with_any_league
          @users_with_paid_league += users_with_paid_league
          @users_with_only_free_league += users_with_only_free_league
          @users_with_no_league += users_with_no_league

          @referred_users_with_any_league += referred_users_with_any_league
          @referred_users_with_paid_league += referred_users_with_paid_league
          @referred_users_with_only_free_league += referred_users_with_only_free_league
          @referred_users_with_no_league += referred_users_with_no_league
        end

        @total_records = days.count.to_f

        render 'affiliate_users'

      end
    end

    def league_odd_even
      @league_type = params[:val]
      if @league_type == 'even'
        @entry_fee_size = League::ENTRY_FEE_ENUM
      else
        @entry_fee_size = League::ODD_ENTRY_FEE_ENUM
      end
      @league_count = League.where(league_type:  @league_type).where('leagues.created_at > ? AND leagues.created_at < ?',@start_date, @end_date).group('date(created_at)').group(:entry_fee).count
      @league_count_total = League.where(league_type:  @league_type).where('leagues.created_at > ? AND leagues.created_at < ?',@start_date, @end_date).group(:entry_fee).count
      @date_array = (@start_date..@end_date)
      @date_array = sort_by_date(params[:order_by], @date_array)
      @unique_user_count = User.joins(:user_teams).joins(:leagues).where(leagues: { league_type: @league_type }).where('user_teams.created_at > ? AND user_teams.created_at < ?',@start_date, @end_date).group('date(leagues.created_at)').group(:entry_fee).distinct('user_teams.user_id').count
      @unique_user_total = User.joins(:user_teams).joins(:leagues).where(leagues: { league_type: @league_type }).where('user_teams.created_at > ? AND user_teams.created_at < ?',@start_date, @end_date).group(:entry_fee).distinct('user_teams.user_id').count
    end

    private

    def all_leagues_stats_for(m)
      leagues_and_teams = {}
      League::PAID_LEAGUE_ENUM.each do |l_entry_fee|
        leagues_of_x_entry_fees = m.leagues.where(entry_fee: l_entry_fee)
        # private_leagues_of_x_entry_fees = leagues_of_x_entry_fees.where(is_private: true)

        leagues_and_teams["#{l_entry_fee}"] = {
          leagues_count: leagues_of_x_entry_fees.count,
          # private_leagues_count: private_leagues_of_x_entry_fees.count,
          user_teams_count: 0,
          amount_paid: 0,
          referral_money_used: 0,
          coupon_money_used: 0
        }

        # League::LEAGUE_SIZE.each do |l_size|
        #   debugger
        #   leagues_and_teams["#{l_entry_fee}"]["leagues_count_for_size_#{l_size}"] = leagues_of_x_entry_fees.where(limit: l_size).count
        #   # leagues_and_teams["#{l_entry_fee}"]["private_leagues_count_for_size_#{l_size}"] = private_leagues_of_x_entry_fees.where(limit: l_size).count
        # end

        leagues_per_size_count = leagues_of_x_entry_fees.group(:limit).count

        League::LEAGUE_SIZE.each do |l_size|
          leagues_and_teams["#{l_entry_fee}"]["leagues_count_for_size_#{l_size}"] = leagues_per_size_count[l_size.to_i].to_i
        end

        l_user_teams = UserTeam.where(league: leagues_of_x_entry_fees)
        l_user_teams_count = l_user_teams.count
        leagues_and_teams["#{l_entry_fee}"][:user_teams_count] = l_user_teams_count
        leagues_and_teams["#{l_entry_fee}"][:amount_paid] = l_user_teams_count * l_entry_fee.to_f
        # leagues_and_teams["#{l_entry_fee}"][:winners_count] = l_user_teams.where(has_won: true).count
        # leagues_and_teams["#{l_entry_fee}"][:tied_count] = l_user_teams.where(has_tied: true).count
        coupon_money_used = UsersCoupon.consumed.where(league: leagues_of_x_entry_fees).sum(:amount)
        leagues_and_teams["#{l_entry_fee}"][:coupon_money_used] = coupon_money_used

        referral_money_used = l_user_teams.sum(:referral_wallet_balance_used)
        leagues_and_teams["#{l_entry_fee}"][:referral_money_used] = referral_money_used
      end
      leagues_and_teams
    end

    def get_money_transactions_total_requests
      @money_transactions_total_requests = append_general_conditions_for(MoneyTransaction.all).count
    end

    def get_money_transactions_success_amount
      @money_transactions_success_amount = append_general_conditions_for(MoneyTransaction.where(status: 'SUCCESS')).
        sum('CAST (amount AS FLOAT)')
    end

    def get_money_transactions_total_success_requests
      @money_transactions_total_success_requests = append_general_conditions_for(MoneyTransaction.where(status: 'SUCCESS')).
        count
    end

    def get_user_transactions_added_to_wallet
      @user_transactions_added_to_wallet = append_general_conditions_for(
        UserTransaction.where(transaction_category: 'add_to_wallet', state: 'completed').
          where.not(params: nil, payment_gateway_response: nil)
      ).sum(:balance_amount)
    end

    def append_general_conditions_for(object, table_name = object.table.name)
      object = append_where_by_condition_for(object, table_name)
      object = append_group_by_condition_for(object, table_name)
      object = append_order_by_condition_for(object, table_name)
    end

    def append_order_by_condition_for(object, table_name = object.table.name)
      object.order("DATE(#{ table_name }.created_at) #{ params[:order_by] }")
    end

    def append_group_by_condition_for(object, table_name = object.table.name)
      object.group("DATE(#{ table_name }.created_at)")
    end

    def append_where_by_condition_for(object, table_name = object.table.name)
      object.where("#{ table_name }.created_at > ?", params[:start_date]).
      where("#{ table_name }.created_at < ?", params[:end_date])
    end

    def set_default_params
      params[:start_date] = params[:start_date].presence || (Time.current - 3.month)
      params[:end_date] = params[:end_date].presence || (Time.current)
      params[:order_by] = params[:order_by].presence || 'ASC'
    end

    def set_date_params
      if params[:start_date] == nil
        params[:start_date] = 3.month.ago
        @start_date = Date.new(params[:start_date].year, params[:start_date].month, params[:start_date].day)
      else
        @start_date = Date.parse(params[:start_date])
      end
      if params[:end_date] == nil
        params[:end_date] = Date.today.end_of_day
        @end_date = Date.new(params[:end_date].year, params[:end_date].month, params[:end_date].day)
      else
        @end_date = Date.parse(params[:end_date]).end_of_day
      end
      params[:order_by] = params[:order_by].presence || 'ASC'
      params[:val] = params[:val].presence || 'even'
    end


    def sort_by_date(order, date_array)
      if order == 'ASC'
        date_array.sort
      elsif order == 'DESC'
        date_array.sort {|a,b| b <=> a}
      else
        raise 'Invalid direction. Specify either ASC or DESC.'
      end
    end

  end
end
