class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  skip_before_action :system_down

  def google_oauth2
    omniauth_details = request.env["omniauth.auth"]
    login_page = request.env['omniauth.params']['login_variation'] rescue ""
    p "====================================================================="
    p login_page
    p "====================================================================="
    # You need to implement the method below in your model (e.g. app/models/user.rb)
    @user = user_auth(omniauth_details['uid'], omniauth_details['provider'])
    # google_login_redirect_url = Rails.env.development? ? 'https://fantoss.surge.sh' : 'https://fantoss.com'
    if Rails.env.development?
      google_login_redirect_url = 'https://fantoss.surge.sh'
    elsif Rails.env.staging?
      google_login_redirect_url = 'https://lite.fantoss.com'
    else
      google_login_redirect_url = 'https://fantoss.com'
    end
    # puts JSON.pretty_generate omniauth_details

    if @user.try(:persisted?)
      # TODO: UPDATE USER LOGIN DATA
      sign_in @user
      @user.update_login_counter(login_page)
      redirect_to(
        "#{ google_login_redirect_url }/home" +
        root_path(
          current_user.as_json(only: [:id, :authentication_token, :phone])
        )
      )
    else
      redirect_to(
        "#{ google_login_redirect_url }/login" +
          root_path(
            proid: omniauth_details['uid'],
            provider: omniauth_details['provider'],
            token: omniauth_details['credentials']['token'],
            image_url: omniauth_details['info']['image'],
            first_name: omniauth_details['info']['first_name'],
            last_name: omniauth_details['info']['last_name'],
            email: omniauth_details['info']['email']
          )
      )
    end
  end

  private

  def user_auth(uid, provider)
    auth =  Authentication.find_by(proid: uid, provider: provider)
    @user =  auth.try :user
  end

end
