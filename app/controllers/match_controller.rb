class MatchController < ApplicationController
  # load_and_authorize_resource
  skip_authorization_check
  skip_before_action :authenticate_user!, only: [:leaderboard, :show]
  skip_before_action :authenticate_user_from_token!, only: [:load_player_card]
  before_filter :get_match, except: [:error]
  before_filter :get_user_teams, except: [:error, :load_player_card]
  skip_before_action :authenticate_user_from_token!, only: [:load_player_card]

  def index
  end

  def show
  end

  def leaderboard    
    render template: 'match/show'
  end

  def error
    redirect_to root_path if flash.empty?
  end

  def load_player_card
    @user_team = UserTeam.find(params[:user_team_id])
    @block = "#player_card_#{params[:index]}" 
    respond_to do |format|
      format.js
    end
  end
private
  def get_match
    @match = Match.includes(:leagues).find(params[:id])
    @top_5_contest = Contest.find_by(team_type: 'top_5_batsmen')
    @page_title = "#{@match.team_names} - #{@match.tournament.name} - #{@match.title} live match leaderboard"
    prepare_meta_tags(title: @page_title, og: {title: @page_title})
  end
  
  def get_user_teams
    @user_teams = []
    @user_teams = @match.user_teams.where(user_id: current_user.id) if current_user.present?
  end

end
