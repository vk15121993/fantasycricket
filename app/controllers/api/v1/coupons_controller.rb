class Api::V1::CouponsController < ApplicationController
  before_action :find_coupon, only: :apply
  before_action :find_users_coupon, only: :apply

  def apply
    if @users_coupon.save
      render json: {
        responseCode: '200',
        responseMessage: @users_coupon.coupon.coupon_message.presence || 'Coupon applied successfully.',
        users_coupon: @users_coupon.as_json.merge(value: @users_coupon.value_for(params[:entry_fee]))
      }.to_json
    else
      render json: {
        responseCode: '422',
        responseMessage: "Invalid Coupon",
        users_coupon: @users_coupon,
        errors: @users_coupon.errors
      }.to_json
    end
  end

  private

  def find_coupon
    @coupon = Coupon.find_by(code: params[:code])
    unless @coupon
      render json: {
        responseCode: '404',
        responseMessage: "Coupon not found"
      }.to_json
    end
  end

  def find_users_coupon
    users_coupon_params = {
      user: current_user,
      coupon: @coupon,
      state: 'APPLIED'
    }

    @users_coupon = UsersCoupon.find_or_initialize_by(users_coupon_params)
    @users_coupon.league_entry_fee = params[:entry_fee]
    @users_coupon.match_id = params[:match_id]
    @users_coupon.league_type = params[:l_type]

  end

end
