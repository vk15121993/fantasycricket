class Api::V1::MoneyTransactionsController < ApplicationController
  before_action :avoid_fraud_users, only: [:create, :update]
  before_action :find_money_transaction, only: :update
  before_action :load_amount, only: :create
  before_action :ensure_wallet_balance, only: :create
  before_action :ensure_minimum_amount, only: :create
  before_action :ensure_maximum_amount, only: :create
  before_action :ensure_request_count, only: :create

  def index
    render json: current_user.money_transactions
  end

  def offline
    render json: {
      responseCode: '200',
      responseMessage: 'We are upgrading our wallet. Automatic withdraw will be available soon. Please try in 2-3 days. Thankx.'
    }.to_json
  end

  def create
    if current_user.failed_attempts == 0
      current_user.update(failed_attempts: 1)
      money_transaction = MoneyTransferService.transfer_for(current_user.id, { amount: @amount })
      if money_transaction
        render json: {
          responseCode: '200',
          responseMessage: "Your transfer for Rs #{ @amount } has been initiated. You can see it's status from dashboard.",
          money_transaction: money_transaction
        }.to_json
      else
        render json: {
          responseCode: '422',
          responseMessage: "Our servers are busy right now. Please try after 10 minutes. Also, please ensure there's a gap of 5 minutes between last league join and withdrawl request.",
        }.to_json
      end

    else
      render json: {
        responseCode: '422',
        responseMessage: "Something went wrong. Contact our support for further info.",
      }.to_json
    end
  end

  def update
    MoneyTransferService.check_transaction_status_for(@money_transaction)
    render json: {
      responseCode: '200',
      responseMessage: 'Status updated.',
      money_transaction: @money_transaction.reload
    }.to_json
  end

  private

  def ensure_request_count
    last_money_transaction = current_user.money_transactions.last
    if last_money_transaction && last_money_transaction.created_at > (Time.current - 3.days)
      render json: {
        responseCode: '422',
        responseMessage: "You can request for transfer only once every 3 days! Your last withdrawal request was on #{ last_money_transaction.created_at.strftime('%B %d, %Y %I:%M %p') }"
      }.to_json
    end
  end

  def find_money_transaction
    @money_transaction = current_user.money_transactions.find_by(id: params[:id])
    render(json: { responseCode: '404', responseMessage: 'Not Found' }.to_json) unless @money_transaction
  end

  def ensure_minimum_amount
    if @amount < User::MINIMUM_WITHDRAWL_AMOUNT
      render json: {
        responseCode: '422',
        responseMessage: "Wallet must have a minimum amount of Rs #{ User::MINIMUM_WITHDRAWL_AMOUNT } for withdrawal!"
      }.to_json
    end
  end

  def ensure_maximum_amount
    if @amount > User::MAXIMUM_WITHDRAWL_AMOUNT
      render json: {
        responseCode: '422',
        responseMessage: "You can withdraw maxmimum upto Rs #{ User::MAXIMUM_WITHDRAWL_AMOUNT } at a time!"
      }.to_json
    end
  end

  def ensure_wallet_balance
    if current_user.wallet.reload.main_balance.to_f < @amount
      render json: {
        responseCode: '422',
        responseMessage: "Oops! The amount exceeds your wallet's main balance!"
      }.to_json
    end
  end

  def load_amount
    # ## Temp message until PayTM API down.
    if Rails.env.development? || Rails.env.staging?
      render json: {
        responseCode: '422',
        responseMessage: "Oops! This is #{ Rails.env } environment!"
      }.to_json and return
    end

    # render json: {
    #   responseCode: '422',
    #   responseMessage: "PayTM withdrawl will be up very soon. Thanks for your patience."
    # }.to_json and return

    # responseMessage: "Oops! We are having some technical issue regarding PayTM payouts. " +
    #   "We will let you know when it will be rectified. Till then, you can always use your wallet to play more and win more!"

    if params[:amount]
      @amount = params[:amount].to_i
    else
      @amount = current_user.wallet.reload.main_balance.to_f
    end
  end
end
