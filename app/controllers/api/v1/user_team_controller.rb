class Api::V1::UserTeamController < ApplicationController
include ApiResourceLoaderConcern

  before_filter :load_match, except: [:index, :edit, :getleaderboards]
  before_filter :load_contest, except: [:index, :edit, :getleaderboards]
  before_filter :load_user_team, only: [:show , :update]
  before_filter :ensure_editability, only: [:update]
  before_filter :load_players, only: [:create,  :update]
  before_filter :find_match, only: [:getleaderboards]
  before_filter :avoid_fraud_users, only: [:create_team, :edit, :update]

  def index
    @match = Match.find_by(id: params[:match_id])
    @contest_id = Contest.find_by(team_type: 'top_5_batsmen').try(:id)
    return render json: {responseCode: 500 , responseMessage: "No match present."} unless @match
    @user_teams = @match.user_teams.where(user_id: current_user.id).includes(:players)
  end

  def getleaderboards
    user_teams_per_page = 50
    if @match
      where_cond=''
      if params[:league_type]!= "null"
      where_cond ="leagues.entry_fee = #{params[:league_type]} "
      end
      all_leagues = @match.user_teams.select('user_teams.*,RANK() OVER (ORDER BY totalscore desc)').order(totalscore: :desc, created_at: :desc).joins('INNER JOIN leagues ON user_teams.league_id = leagues.id').where(where_cond)
      total_lbs_count=all_leagues.size
      user_teams = all_leagues.offset(params[:offset]).limit(15)
      own_teams = all_leagues.select{|item| item.user_id == current_user.id }

      user_teams_data = user_teams.map.with_index do |ut, index|
        ut.attributes.merge(user_details: ut.user.as_json(only: [:username, :image_url]), position: index + 1)
      end

      own_teams_data = own_teams.map.with_index do |ut, index|
        ut.attributes.merge(user_details: ut.user.as_json(only: [:username, :image_url]), position: index + 1)
      end

      if current_user
        current_user_teams_data = user_teams_data.select { |ut| ut['user_id'] == current_user.id }
      else
        current_user_teams_data = []
      end
      render json: {responseCode: 200 ,
                    responseMessage: "Leaderboards result",
                    user_teams: user_teams_data,
                    current_user_teams: current_user_teams_data,
                    total_lbs_count: total_lbs_count,
                    start: params[:start],
                    own_teams: own_teams_data
                  }
    else
      render json: {responseCode: 404 , responseMessage: "Match not present"}
    end

  end

  def create_team
    last_user_team = current_user.user_teams.last
    if last_user_team && last_user_team.created_at > (Time.current - 10.seconds)
      return render json: { responseCode: 400 , responseMessage: "Please wait a little more. We are finding players for you." }
    end

  	@user_team = @match.user_teams.build(user_team_permitted_params)
    if @user_team.save
      @user_team
    else
 	   return render json: {responseCode: 400 , responseMessage: "Bad request" , errors: @user_team.errors.full_messages}
    end
  end

  def edit
    @user_team = UserTeam.find_by(id: params[:id], user_id: current_user.id)
    unless @user_team
      return render json: {responseCode: 204 , responseMessage: "No user team found."}
    end
    unless @user_team.allow_edit?
      return render json: {responseCode: 405, responseMessage: "You can not edit this team.", match_id: @match, contest_id: @contest}
    end
  end

  def update
    if @user_team.update(user_team_permitted_params)
      @user_team.reload
    else
      return render json: {responseCode: 405, responseMessage: "Bad request.",error: @user_team.errors.full_messages , match_id: @match, contest_id: @contest}
    end
  end

  private
  def user_team_permitted_params
    _params = params.require(:user_team)
      .permit(:contest_id ,:match_id, :captain_id, :captain_id1 ,player_ids: [])
      .merge({ contest_id: @contest.id, user_id: current_user.id })
    _params.merge!({ ut_token: params[:ut_token] }) if params[:ut_token].present?
    _params
  end

  def find_match
    @match = Match.find_by_id(params[:match_id])
  end
end
