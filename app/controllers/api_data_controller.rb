class ApiDataController < ApplicationController
  skip_authorization_check
  skip_before_action :authenticate_user!

  before_action :set_api_response, only: :sync_to_db
  before_action :set_live_score_response, only: :refresh_player_score

  def sync_to_db
    ::ApiDataSyncService.new(@api_response).call
    flash[:notice] = "Data is synced with db."
    redirect_to :back
  end

  def refresh_player_score
    ::ApiLiveScoreService.new(@live_score_response).call
    flash[:notice] = "Score refreshed."
    redirect_to :back
  end

  private
    def set_api_response
      @api_response = ScoreAPI.getSeries(params[:series_id])["query"]["results"] if params[:series_id].present?
      unless @api_response
        redirect_to root_path
      end
    end

    def set_live_score_response
      @live_score_response = ScoreAPI.getLiveScore(params[:match_id])["query"]["results"] if params[:match_id].present?
      unless @live_score_response
        redirect_to root_path
      end
    end
end
