class OperaController < ApplicationController
  include OperaHelper
  skip_before_action :authenticate_user!, only: [:index]

  http_basic_authenticate_with name: "opera", password: "opera@fantoss"

  def index
    @month = params[:month] || Time.current.month
    @year = params[:year] || Time.current.year
    @val = params[:val]
    # @initiated_date =  Date.parse("2017-01-01")
    # @end_date = Date.today

    @total_m = Hash.new { |hash, key| hash[key] = {} }
    @total_d = Hash.new { |hash, key| hash[key] = {} }

    if @val.present? && @val == 'total'
      (1..12).each do |month|
        first_day_of_month = Date.parse("#{ @year }-#{ month }-01")

        users = User.affiliate_base_users_for('opera').
          where('created_at >= ?', first_day_of_month).
          where('created_at < ?', first_day_of_month.next_month)

        @total_m[month.to_s][:users] = users.count

        League::ALL_ENTRY_FEE_ENUM.each do |entry_fee|
          user_teams = UserTeam.joins(:user).joins(:league).where(leagues: { entry_fee: entry_fee }).
            where(users: { referral_code: 'opera' }).
            # where(has_won: true).
            where.not(has_tied: true).
            where('user_teams.created_at >= ?', first_day_of_month).
            where('user_teams.created_at < ?', first_day_of_month.next_month)

          @total_m[month.to_s][entry_fee.to_s] = user_teams.count
        end
      end

    else

      (1..(Date.parse("#{ @year }-#{ @month }-01")).end_of_month.day).each do |day|
        day_of_month = Date.parse("#{ @year }-#{ @month }-#{ day }")

        users = User.affiliate_base_users_for('opera').
          where('created_at >= ?', day_of_month).
          where('created_at < ?', day_of_month.next_day)

        @total_d[day.to_s][:users] = users.count

        League::ALL_ENTRY_FEE_ENUM.each do |entry_fee|
          user_teams = UserTeam.joins(:user).joins(:league).where(leagues: { entry_fee: entry_fee }).
            where(users: { referral_code: 'opera' }).
            # where(has_won: true).
            where.not(has_tied: true).
            where('user_teams.created_at >= ?', day_of_month).
            where('user_teams.created_at < ?', day_of_month.next_day)

          @total_d[day.to_s][entry_fee.to_s] = user_teams.count
        end

      end

    end


    # debugger

    # @months = (@initiated_date..@end_date).map{ |m| m.strftime('%Y%m') }.uniq.map{ |m| m }
    # if @val.nil? || @val != "total"
    #   if @val.present? && @val != "total"
    #     @start_date = Date.parse(@val+"01")
    #     @end_date = (@start_date +days_in_month(Date::MONTHNAMES.index(params[:month].to_s), Time.now.year)-1)
    #   else
    #     @today = Date.today
    #     @start_date = Date.parse "#{@today.year}-#{@today.month}-01"
    #     @start_date.to_s
    #     @end_date = Date.today
    #   end
    #   @users_registered_query = User.group("DATE(created_at), referral_code").
    #               having('DATE(created_at) > ?', Date.parse("2017-01-01")).
    #               having('referral_code = ?', 'opera')
    #   @users_registered = @users_registered_query.count
    #   @user_team = UserTeam.joins(:league).joins(:user).
    #                 group(
    #                   "DATE(user_teams.created_at)", "entry_fee", 'league_id',
    #                   'has_won', 'has_tied', 'users.referral_code'
    #                 ).
    #                 having(has_won: true).
    #                 having(has_tied: false).
    #                 # having('users.referral_code = ?', 'opera').
    #                 count(:league_id)
    #   @total_opera_transaction = @user_team




    #   @load = "first"
    # else
    #   @load = "on_month"
    #   @users_registered_query = User.group("DATE_TRUNC('month', created_at), referral_code").
    #                 having("DATE_TRUNC('month', created_at) > ?", Date.parse("2017-01-01")).
    #                 having('referral_code = ?', 'optimise').select("DATE_TRUNC('month', created_at)")
    #   @no_user_for_month = @users_registered_query.count
    #   @league_played_month = UserTeam.joins(:league).joins(:user).
    #                 group(
    #                   "DATE_TRUNC('month',user_teams.created_at)", "entry_fee",
    #                   'has_won', 'has_tied', 'league_id', 'users.referral_code'
    #                 ).
    #                 having(has_won: true).
    #                 having(has_tied: true).
    #                 # having('users.referral_code = ?', 'opera').
    #                 count(:league_id)
    #   @total_opera_transaction = @league_played_month
    #   # debugger
    # end
    # @opera_total_transaction = total_transaction(@total_opera_transaction, @no_user_for_month, @load , @start_date, @end_date)
  end

  private

  def days_in_month(month, year = Time.now.year)
    return 29 if month == 2 && Date.gregorian_leap?(year)
    Time::COMMON_YEAR_DAYS_IN_MONTH[month]
  end


end
