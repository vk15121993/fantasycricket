class ApplicationController < ActionController::Base
  # if ENV['BASIC_AUTH']
  #   user, pass = ENV['BASIC_AUTH'].split(':')
  #   http_basic_authenticate_with name: user, password: pass
  # end
  # before_filter :set_access

  before_action :prepare_meta_tags, if: "request.get?"
  # before_action :check_verification


  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # Disabling as we are logging users from facebook
  # protect_from_forgery with: :exception

  # Devise, require authenticate by default
  prepend_before_action :authenticate_user_from_token! ,  unless: -> { request.format.html? }
  prepend_before_action :authenticate_user!,  unless: -> { request.format.json? }
  # before_action :system_down, unless: -> { request.format.json? }

  # CanCan, check authorization unless authorizing with devise
  # check_authorization unless: :skip_check_authorization?

  # Special handling for ajax requests.
  # Must appear before other rescue_from statements.
  rescue_from Exception, with: :handle_uncaught_exception

  include CommonHelper
  include ErrorReportingConcern
  include AuthorizationErrorsConcern

  # FRAUD_UIDS = [43081, 44458, 31253, 33190, 28745, 12251, 33323, 42449, 37037, 20661, 45862, 43604, 26701, 38371,
  #   42517, 46834, 43906, 44100, 46975, 45831, 25721, 31159, 43924, 38233, 46479, 37741, 42803, 23286, 28320, 28258,
  #   28750, 26284, 45355, 16033, 42391, 38299, 38133, 45435, 35996, 38717, 36780, 21238, 46527, 18736, 32649, 26858,
  #   46302, 47963, 41962, 40694, 22575, 16395, 17303, 36971, 26474, 18777, 33664, 35528, 23484, 8811, 18599, 32283,
  #   35391, 40675, 38438, 40020, 31164, 40727, 48020, 33187, 45790, 47069, 25720, 42202, 29612, 17449, 35670, 45386,
  #   36575, 42767, 1647, 42915, 46215, 27547, 34578, 18789, 34442, 48116, 39778, 42213, 42555, 18412, 41313, 19612,
  #   24161, 35436, 37581, 48175, 17482, 36339, 36381, 46517, 16851, 18586, 48060, 16390, 45666, 35587, 36754, 46185,
  #   47831, 38318, 37304, 36348, 20830, 26501, 46822, 46088, 46252, 46853, 30647, 38228, 18063, 30639, 19531, 42488,
  #   47645, 39916, 19063, 45976, 33509, 17429, 47688, 47819, 40922, 43311, 48504, 38689, 36022, 48709, 46265, 35362,
  #   47771, 34567, 38637, 46958, 9320, 36919, 23997, 41886, 20508, 40749, 47545, 27415, 35225, 44264, 23414, 35787,
  #   47223, 21626, 32095, 23771, 18165, 8489, 34433, 46444, 44472, 30072, 35307, 4977, 31143, 48642, 29980, 24257,
  #   24937, 48163, 16521, 10221, 46077, 46319, 18336, 47842, 35479, 49492, 48318, 7929, 45042, 46996, 17568, 26176,
  #   35942, 16764, 37059, 33401, 33989, 45633, 45830, 39646, 37054, 14694, 34172, 22655, 29722, 49574, 34505, 48499,
  #   8521, 49314, 35410, 34847, 37599, 38813, 43673, 40550, 16389, 38247, 24028, 48130, 43084, 48682, 21425, 41198,
  #   43267, 48035, 41221, 15145, 49279, 24148, 20391, 46532, 11053, 8728, 48474, 48822, 33459, 22166, 4835, 47471,
  #   23290, 49012, 35506, 49167, 26396, 35480, 49710, 48994, 42132, 11056, 48064, 43489, 45764, 38239, 37279, 49069,
  #   23128, 43077, 47364, 27370, 36177, 28142, 43390, 46019, 47697, 21474, 46595, 48556, 35434, 36314, 10157, 49444,
  #   16365, 43124, 39837, 49209, 24058, 44961, 35442, 34884, 46422, 47976, 21584, 32929, 47262, 29965, 34591, 27670,
  #   7161, 49480, 36590, 48810, 46765, 36110, 37213, 18877, 50971, 33388, 44539, 50683, 49156, 14333, 41197, 49335,
  #   23534, 8601, 45680, 36612, 46123, 42630, 43127, 38845, 29662, 37099, 28992, 34509, 16007, 21337, 45702, 32028,
  #   36430, 25200, 32553, 45642, 41204, 35722, 43970, 15415, 36262, 45583, 35057, 33774, 45923, 38033, 44463, 26619,
  #   12253, 21152, 20892, 16992, 44460, 37783, 41103, 8964, 38399, 33379, 32707, 31219, 45975, 22429, 28211, 34746,
  #   45212, 44354, 40970, 28592, 17099, 24291, 25429, 29977, 13246, 44822, 37140, 47529, 34242, 35911, 47131, 47853,
  #   46838, 44317, 45064, 35973, 24173, 22946, 16790, 44727, 9607, 14296, 45917, 46281, 48149, 24953, 8826, 42119,
  #   46868, 28919, 48022, 45086, 24516, 24050, 40947, 49084, 26834, 46371, 37808, 48162, 48132, 48439, 48411, 48152,
  #   48326, 16418, 45948, 42526, 20375, 29089, 32859, 36304, 36691, 48136, 48784, 42870, 47041, 35509, 44882, 50536,
  #   44146, 33036, 34412, 15947, 12189, 35664, 19578, 52942, 30963, 43907, 28373, 37829, 36626, 43422, 37762, 30921,
  #   42744, 49432, 47086, 36351, 34445, 43710, 44224, 34343, 21480, 43240, 21107, 34195, 36135, 49175, 28958, 37603,
  #   46538, 26023, 36947, 34735, 34776, 31576, 49472, 47222, 29528, 32339, 19225, 23029, 46151, 48057, 49575, 17606,
  #   34305, 34817, 46635, 50324, 49463, 44566, 7329, 17640, 44978, 50476, 32964, 23680, 38654, 33211, 43332, 31579,
  #   45048, 43668, 47690, 46668, 17801, 43688, 37048, 47847, 41690, 49225, 28990, 37367, 34850, 33208, 49313, 32336,
  #   49265, 11105, 31595, 31001, 45944, 22626, 7595, 45149, 50819, 34144, 50366, 29175, 50876, 46184, 50029,
  #   28058, 35727, 41907, 20673, 39972, 34812, 46614, 20456, 52041, 36126, 50130, 45551, 37657, 50345, 48974, 13807,
  #   36931, 45507, 15970, 48901, 43220, 27453, 16085, 36531, 37957, 45027, 27961, 33306, 50105, 51923, 41201, 50678,
  #   16833, 51808, 44519, 49199, 19846, 53072, 26905, 45594, 28363, 16255, 42409, 48812, 41969, 53235, 46961, 46696,
  #   37855, 42697, 50861, 43632, 29463, 48260, 50963, 46150, 49194, 49889, 48738, 48987, 40920, 45322, 49157, 37517,
  #   52851, 49348, 41763, 41206, 49262, 27573, 45869, 7616, 30298, 37261, 37771, 38877, 20030, 51873, 27482,
  #   39734, 49666, 46782, 50349, 25151, 52591, 49291, 51668, 42609, 52889, 52867, 47194, 18823, 54033, 28976, 52779,
  #   50771, 34376, 19397, 42966, 52200, 34860, 9537, 35720, 28427, 39448, 45018, 28624, 31940, 34174, 18480, 27353,
  #   53305, 50177, 51440, 27844, 8192, 49499, 53884, 53892, 46385, 32394, 47128, 19093, 53753, 15168, 53551, 41224,
  #   51848, 3202, 38204, 53789, 53912]

  FRAUD_UIDS = [59592, 202810, 205462, 106982, 203855, 203856, 205252, 205457, 17099, 202434, 205639, 203885, 204697, 205264,
                203239, 204376, 202564, 203979, 203983, 202588, 203966, 204573, 203379, 203857, 202546, 205036, 205057]



  def avoid_fraud_users
    if current_user && (User::F_UIDS.include?(current_user.id) || MetaInfo.blocked_users.select(:identifier).distinct.pluck(:identifier).include?(current_user.id))
      return render json: { responseCode: 403 , responseMessage: "We are reviewing your account information. Till then you cannot do major actions on our website, like creating teams and joining leagues. For further information, please contact our support." }
    end
  end

  def system_down
    usrs_ids = User.where(phone: ['7503578995']).pluck(:id)
    if current_user && !(current_user.is_admin? || usrs_ids.include?(current_user.id))
      redirect_to('https://fantoss.com')
    end
  end

  def check_verification
    redirect_to verify_path if  current_user.present? && (current_user.is_verified == false)
  end
  def prepare_meta_tags(options={})
    site_name   = "fantoss.com"
    title       = "Daily fantasy cricket leagues for free and paytm cash. Made for everyone."
    description = "Play top 5 batsman fantasy cricket league game made by India's top sports startup. Challenge hundreds of members competing in 2, 4 and 8 leagues to win cash daily. Payments powered by Paytm."
    image       = options[:image] || "https://fantoss.com/meta_assets/og_image.jpg"
    current_url = request.original_url

    static_keywords = %w[live\ cricket\ news fantasy\ cricket live\ cricket\ score world\ cup\ t20\ 2016 indian\ cricket\ team icc\ world\ cup IPL\ 2016 virat\ kohli paytm fantasy\ league\ ipl ipl\ fantasy fantasy\ cricket\ dream11 fantasy\ cricket fantasy\ cricket\ game daily\ fantasy\ cricket]
    # options[:keywords] = static_keywords + options[:keywords] if options[:keywords].present?

    defaults = {
      site:        site_name,
      title:       title,
      image:       image,
      description: description,
      keywords:    static_keywords,
      twitter: {
        site_name: site_name,
        site: '@fantoss.com',
        card: 'summary',
        description: description,
        image: image
      },
      og: {
        url: current_url,
        site_name: site_name,
        title: title,
        image: image,
        description: description,
        type: 'website'
      }
    }

    options.reverse_merge!(defaults)

    set_meta_tags options
  end


  protected

  def authenticate_user_from_token!
    # return render json: { responseCode: 400 , responseMessage: "Under Maintenance" }

    logger.info "=====#{request.headers}========#{request.headers[:TOKEN]}===========#{params}====="
    user_token = request.headers[:TOKEN].presence
    user       = user_token && User.find_by_authentication_token(user_token)
    if user
      if ApplicationController::FRAUD_UIDS.include? user.id
        return render json: { responseCode: 401 , responseMessage: "Fraudulent transactions reported from Paytm." }
      end
      sign_in user
    else
      return render json: { responseCode: 401 , responseMessage: "Unauthorized access" }
    end
  end

  def set_access
    response.headers["Access-Control-Allow-Origin"] = "*"
    headers["Access-Control-Allow-Methods"] = "*"#%w{GET POST PUT DELETE OPTIONS}.join(",")
    headers["Access-Control-Allow-Headers"] = "*"#%w{Origin Accept accept Content-Type X-Requested-With X-CSRF-Token TOKEN}.join(",")
    head(:ok) if request.request_method == "OPTIONS"
  end


  # def skip_check_authorization?
  #   devise_controller? || is_a?(RailsAdmin::ApplicationController)
  # end

  # Reset response so redirect or render can be called again.
  # This is an undocumented hack but it works.
  def reset_response
    self.instance_variable_set(:@_response_body, nil)
  end

  # Respond to uncaught exceptions with friendly error message during ajax requets
  def handle_uncaught_exception(exception)
    if request.format == :js
      report_error(exception)
      flash.now[:error] = Rails.env.development? ? exception.message : I18n.t('errors.unknown')
      render 'layouts/uncaught_error.js'
    else
      raise
    end
  end
end
