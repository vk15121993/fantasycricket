
require "em-websocket"
require 'active_record'
require 'httparty'
require 'json'
# Local
# ActiveRecord::Base.establish_connection(
#   :adapter => "postgresql",
#   :encoding => "unicode",
#   :host => "localhost",
#   :database => "fantoss_development",
#   :username => "postgres",
#   :password => "postgres",
#   :pool => 5
#   )

# Defining Classes

#Event Machine
EM.run {
  @clients = {}
  p "I am in EVENT MACHINE"
  EM::WebSocket.start(:host => "0.0.0.0", :port => 8080) do |ws|
    ws.onopen{ |handshake|
      @clients[SecureRandom.urlsafe_base64.to_s] = ws
    }
    ws.onclose { puts "Connection closed" }
      ws.onmessage{ |msg|
        msg = JSON.parse(msg)
           @clients.each do |k,v|
              v.send msg["body"]
           end
      }
  end
}
