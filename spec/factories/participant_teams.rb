# == Schema Information
#
# Table name: participant_teams
#
#  id            :integer          not null, primary key
#  name          :string
#  created_at    :datetime
#  updated_at    :datetime
#  captain_id    :integer
#  keeper_id     :integer
#  api_team_id   :string
#  current_squad :text             default([]), is an Array
#  short_name    :string           default("")
#

FactoryGirl.define do
  factory :participant_team, :class => 'ParticipantTeams' do
    name "MyString"
    tournament_id 1
  end

end
