# == Schema Information
#
# Table name: matches
#
#  id            :integer          not null, primary key
#  home_team_id  :integer
#  away_team_id  :integer
#  created_at    :datetime
#  updated_at    :datetime
#  start_time    :datetime
#  end_time      :datetime
#  tournament_id :integer
#  is_live       :boolean          default(FALSE)
#  title         :string
#  api_match_id  :string
#  mtype         :string
#  team_names    :string           default("")
#  ms            :string           default("Match is yet to start")
#

require 'rails_helper'

RSpec.describe Match, :type => :model do
  pending "add some examples to (or delete) #{__FILE__}"
end
